package com.aramex.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.aramex.config.LocatorsRepository;

public class CashierCheckerPage {

	@FindBy(xpath=LocatorsRepository.AppRejPaymntbtn)
	private WebElement AppRejPaymntbtn;
	
	@FindBy(xpath=LocatorsRepository.chkActionDrpdwn)
	private WebElement chkActionDrpdwn;
	
	@FindBy(xpath=LocatorsRepository.approveByChkr)
	private WebElement approveByChkr;
	
	@FindBy(xpath=LocatorsRepository.rejectByChkr)
	private WebElement rejectByChkr;
	
	@FindBy(xpath=LocatorsRepository.submitbtn)
	private WebElement submitbtn;
	
	@FindBy(xpath=LocatorsRepository.Caschkrchkbox)
	private WebElement Caschkrchkbox;
	
	@FindBy(xpath=LocatorsRepository.viewTotal)
	private WebElement viewTotal;
	
	@FindBy(xpath=LocatorsRepository.updateRepaymnt)
	private WebElement updateRepaymnt;
	
	@FindBy(xpath=LocatorsRepository.dropdownOpen)
	private WebElement dropdownOpen;
	
	@FindBy(xpath=LocatorsRepository.cancelReapymnt)
	private WebElement cancelReapymnt;
	
	@FindBy(xpath=LocatorsRepository.genrtDCRBnkTrnsf)
	private WebElement genrtDCRBnkTrnsf;
	
	@FindBy(xpath=LocatorsRepository.rejectbckt)
	private WebElement rejectbckt;
	
	@FindBy(xpath=LocatorsRepository.paymentType)
	private WebElement paymentType;
	
	@FindBy(xpath=LocatorsRepository.cardType)
	private WebElement cardType;
	
	@FindBy(xpath=LocatorsRepository.cashType)
	private WebElement cashType;
	
	@FindBy(xpath=LocatorsRepository.bankTrasfrType)
	private WebElement bankTrasfrType;

	public WebElement getAppRejPaymntbtn() {
		return AppRejPaymntbtn;
	}

	public WebElement getChkActionDrpdwn() {
		return chkActionDrpdwn;
	}

	public WebElement getApproveByChkr() {
		return approveByChkr;
	}

	public WebElement getRejectByChkr() {
		return rejectByChkr;
	}

	public WebElement getSubmitbtn() {
		return submitbtn;
	}

	public WebElement getCaschkrchkbox() {
		return Caschkrchkbox;
	}

	public WebElement getViewTotal() {
		return viewTotal;
	}

	public WebElement getUpdateRepaymnt() {
		return updateRepaymnt;
	}

	public WebElement getDropdownOpen() {
		return dropdownOpen;
	}

	public WebElement getCancelReapymnt() {
		return cancelReapymnt;
	}

	public WebElement getGenrtDCRBnkTrnsf() {
		return genrtDCRBnkTrnsf;
	}

	public WebElement getRejectbckt() {
		return rejectbckt;
	}

	public WebElement getPaymentType() {
		return paymentType;
	}

	public WebElement getCardType() {
		return cardType;
	}

	public WebElement getCashType() {
		return cashType;
	}

	public WebElement getBankTrasfrType() {
		return bankTrasfrType;
	}
}
