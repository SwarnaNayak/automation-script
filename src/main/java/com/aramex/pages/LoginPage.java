package com.aramex.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.aramex.config.LocatorsRepository;

public class LoginPage {

	@FindBy(id=LocatorsRepository.userName)
	private WebElement usernameby; 
	
	@FindBy(id=LocatorsRepository.passWord)
	private WebElement passwordby; 
	
	@FindBy(xpath=LocatorsRepository.Login)
	private WebElement login; 
	
	@FindBy(xpath=LocatorsRepository.aramexLogo)
	private WebElement armexlogoimg;
	
	@FindBy(xpath=LocatorsRepository.navigateToCahsierMkkr)
	private WebElement navigateTocashierMakr;

	@FindBy(xpath=LocatorsRepository.changetoCashierChkr)
	private WebElement changetoCashierChkr;
	
	@FindBy(xpath=LocatorsRepository.navigateToCahsierChkr)
	private WebElement navigateToCahsierChkr;
	
	@FindBy(xpath=LocatorsRepository.navigateToFinanceChkr)
	private WebElement navigateToFinanceChkr;
	
	public WebElement getNavigateToFinanceChkr() {
		return navigateToFinanceChkr;
	}

	public WebElement getChangetoCashierChkr() {
		return changetoCashierChkr;
	}

	public WebElement getNavigateToCahsierChkr() {
		return navigateToCahsierChkr;
	}

	public WebElement getUsernameby() {
		return usernameby;
	}

	public WebElement getPasswordby() {
		return passwordby;
	}

	public WebElement getLogin() {
		return login;
	}

	public WebElement getArmexlogoimg() {
		return armexlogoimg;
	}

	public WebElement getNavigateTocashierMakr() {
		return navigateTocashierMakr;
	}
	
	
}
