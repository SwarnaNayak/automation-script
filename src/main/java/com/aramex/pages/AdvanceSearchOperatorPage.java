package com.aramex.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.aramex.config.LocatorsRepository;

public class AdvanceSearchOperatorPage {

	@FindBy(xpath=LocatorsRepository.operatornotEqualsTo)
	private WebElement operatornotEqualsTo;
	
	@FindBy(xpath=LocatorsRepository.operatorgreaterthanEqualsTo)
	private WebElement operatorgreaterthanEqualsTo;
	
	@FindBy(xpath=LocatorsRepository.operatornotlessthanEqualsTo)
	private WebElement operatornotlessthanEqualsTo;
	
	@FindBy(xpath=LocatorsRepository.operatorlessthan)
	private WebElement operatorlessthan;
	
	public WebElement getOperatorLike() {
		return operatorLike;
	}

	@FindBy(xpath=LocatorsRepository.operatorLike)
	private WebElement operatorLike;
	
	@FindBy(xpath=LocatorsRepository.operatorgreaterthan)
	private WebElement operatorgreaterthan;
	
	@FindBy(xpath=LocatorsRepository.operatorin)
	private WebElement operatorin;
	
	@FindBy(xpath=LocatorsRepository.operatorrange)
	private WebElement operatorrange;

	@FindBy(xpath=LocatorsRepository.addsymbol)
	private WebElement addsymbol;
	
	@FindBy(xpath=LocatorsRepository.rangeinput1)
	private WebElement rangeinput1;
	
	@FindBy(xpath=LocatorsRepository.rangeinpuut2)
	private WebElement rangeinpuut2;
	
	@FindBy(xpath=LocatorsRepository.datevaluefrom)
	private WebElement datevaluefrom;
	
	@FindBy(xpath=LocatorsRepository.datevalueTo)
	private WebElement datevalueTo;
	
	public WebElement getDatevaluefrom() {
		return datevaluefrom;
	}

	public WebElement getDatevalueTo() {
		return datevalueTo;
	}

	public WebElement getMonthSelectr() {
		return monthSelectr;
	}

	public WebElement getFromMonth() {
		return fromMonth;
	}

	public WebElement getFromDate() {
		return fromDate;
	}

	public WebElement getToMonth() {
		return toMonth;
	}

	public WebElement getToDate() {
		return toDate;
	}

	@FindBy(xpath=LocatorsRepository.monthSelectr)
	private WebElement monthSelectr;
	
	@FindBy(xpath=LocatorsRepository.fromMonth)
	private WebElement fromMonth;
	
	@FindBy(xpath=LocatorsRepository.fromDate)
	private WebElement fromDate;
	
	@FindBy(xpath=LocatorsRepository.toMonth)
	private WebElement toMonth;
	
	@FindBy(xpath=LocatorsRepository.toDate)
	private WebElement toDate;

	public WebElement getRangeinput1() {
		return rangeinput1;
	}

	public WebElement getRangeinpuut2() {
		return rangeinpuut2;
	}

	public WebElement getAddsymbol() {
		return addsymbol;
	}

	public WebElement getOperatornotEqualsTo() {
		return operatornotEqualsTo;
	}

	public WebElement getOperatorgreaterthanEqualsTo() {
		return operatorgreaterthanEqualsTo;
	}

	public WebElement getOperatornotlessthanEqualsTo() {
		return operatornotlessthanEqualsTo;
	}

	public WebElement getOperatorlessthan() {
		return operatorlessthan;
	}

	public WebElement getOperatorgreaterthan() {
		return operatorgreaterthan;
	}

	public WebElement getOperatorin() {
		return operatorin;
	}

	public WebElement getOperatorrange() {
		return operatorrange;
	}
	
	
}
