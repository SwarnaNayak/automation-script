package com.aramex.pages;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import com.aramex.base.TestBase;
import com.aramex.config.LocatorsRepository;
import com.aramex.util.TestUtil;
import com.aramex.util.WebDriverLib;

public class ShipmentDetailViewPage extends TestBase{

	TestUtil testutil =  new TestUtil();
	
	@FindBy(xpath=LocatorsRepository.shipmntselctarrow)
	private WebElement shipmntselctarrow;
	
	@FindBy(xpath=LocatorsRepository.selectPayByCash)
	private WebElement selectPayByCash;
	
	@FindBy(xpath=LocatorsRepository.selectPayByCard)
	private WebElement selectPayByCard;
	
	@FindBy(xpath=LocatorsRepository.selectPayBYBnkTrsnf)
	private WebElement selectPayBYBnkTrsnf;
	
	public WebElement getCardLast4Digit() {
		return cardLast4Digit;
	}

	public WebElement getAuthCode() {
		return authCode;
	}

	public void setSelectPayBYBnkTrsnf(WebElement selectPayBYBnkTrsnf) {
		this.selectPayBYBnkTrsnf = selectPayBYBnkTrsnf;
	}

	@FindBy(xpath=LocatorsRepository.clkRaise)
	private WebElement clkRaise;
	
	@FindBy(xpath=LocatorsRepository.transactioninput)
	private WebElement transactioninput;
	
	@FindBy(xpath=LocatorsRepository.comments)
	private WebElement comments;
	
	@FindBy(xpath=LocatorsRepository.paybtn)
	private WebElement paybtn;
	
	@FindBy(xpath=LocatorsRepository.clkonOK)
	private WebElement clkonOK;
	
	@FindBy(xpath=LocatorsRepository.clkonCancel)
	private WebElement clkonCancel;
	
	@FindBy(xpath=LocatorsRepository.clkonlisting)
	private WebElement clkonlisting;
	
	@FindBy(xpath=LocatorsRepository.cardLast4Digit)
	private WebElement cardLast4Digit;
	
	@FindBy(xpath=LocatorsRepository.authCode)
	private WebElement authCode;
	
	@FindBy(xpath=LocatorsRepository.bnkaccntNo)
	private WebElement bnkaccntNo;
	
	@FindBy(xpath=LocatorsRepository.tranfrReff)
	private WebElement tranfrReff;
	
	public WebElement getBnkaccntNo() {
		return bnkaccntNo;
	}

	public WebElement getTranfrReff() {
		return tranfrReff;
	}

	public WebElement getClkonlisting() {
		return clkonlisting;
	}

	public TestUtil getTestutil() {
		return testutil;
	}

	public WebElement getShipmntselctarrow() {
		return shipmntselctarrow;
	}

	public WebElement getSelectPayByCash() {
		return selectPayByCash;
	}

	public WebElement getSelectPayByCard() {
		return selectPayByCard;
	}

	public WebElement getSelectPayBYBnkTrsnf() {
		return selectPayBYBnkTrsnf;
	}
	public WebElement getClkRaise() {
		return clkRaise;
	}

	public WebElement getTransactioninput() {
		return transactioninput;
	}
	public WebElement getComments() {
		return comments;
	}

	public WebElement getPaybtn() {
		return paybtn;
	}

	public WebElement getClkonOK() {
		return clkonOK;
	}


	public WebElement getClkonCancel() {
		return clkonCancel;
	}

	public void clickon_search_Shipment(String shipmentno) throws InterruptedException, IOException {
		  CashierMakerConsumerHomePage homePage=PageFactory.initElements(TestBase.driver, CashierMakerConsumerHomePage.class);
		  ShipmentsBasicSearchPage shipmntbasicsrch=PageFactory.initElements(TestBase.driver, ShipmentsBasicSearchPage.class);
		  Actions act =  new Actions(driver);
		  WebDriverLib wbLib=new WebDriverLib();
		  
		  wbLib.waitForPageTobeLoad();
		  Thread.sleep(5000);
		  shipmntbasicsrch.getClickondropdownToNavigateShipment().click();
		  shipmntbasicsrch.getClickonshipmentmodule().click();
		  Thread.sleep(9000);
		  //act.moveToElement(homePage.getFilterBy()).click().perform();
		  homePage.getFilterBy().click();
		  shipmntbasicsrch. getSelectShipmentNumber().click();
		  homePage.getBasicSearchInputField().click();
		  homePage.getBasicSearchInputField().sendKeys(shipmentno);
		  homePage.getClickonSearchoption().click();
		 
		  Thread.sleep(8000);       
		  boolean ShipmentNo = driver.getPageSource().contains(shipmentno);
		  if(ShipmentNo==true)
		  {
			  Assert.assertTrue(true);
			 
			  testutil.captureScreen(driver, "ShipmentNo searchpassed");
			  driver.findElement(By.xpath("//td[text()='"+shipmentno+"']")).click();
			  Thread.sleep(9000);
			  testutil.captureScreen(driver, " ' "+shipmentno+" ' detailview");
		  }
		  else {
			  testutil.captureScreen(driver, "ShipmentNo searchfailed");
			  Assert.assertTrue(false);
		  }

	}
}
