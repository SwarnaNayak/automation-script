package com.aramex.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.aramex.config.LocatorsRepository;

public class ServiceRequestPage {

	@FindBy(xpath=LocatorsRepository.ClickMYRequestbucket)
	private WebElement ClickMYRequestbucket;

	public WebElement getClickMYRequestbucket() {
		return ClickMYRequestbucket;
	}
	
	@FindBy(xpath=LocatorsRepository.clickonServiceRQ)
	private WebElement clickonServiceRQ;
	
	@FindBy(xpath=LocatorsRepository.ClickAllTransactions)
	private WebElement ClickAllTransactions;
	
	public WebElement getClickAllTransactions() {
		return ClickAllTransactions;
	}

	@FindBy(xpath=LocatorsRepository.clickondropdownToNavigateServiceRq)
	private WebElement clickondropdownToNavigateServiceRq;
	
	public WebElement getClickondropdownToNavigateServiceRq() {
		return clickondropdownToNavigateServiceRq;
	}

	public WebElement getclickonServiceRQ() {
		return clickonServiceRQ;
	}
	@FindBy(xpath=LocatorsRepository.ClickApprovedCashbuck)
	private WebElement ClickApprovedCashbuck;
	
	@FindBy(xpath=LocatorsRepository.ClickApprovedCardbuck)
	private WebElement ClickApprovedCardbuck;
	
	@FindBy(xpath=LocatorsRepository.ClickApprovedByBnkbuck)
	private WebElement ClickApprovedByBnkbuck;
	
	public WebElement getClickApprovedCardbuck() {
		return ClickApprovedCardbuck;
	}

	public WebElement getClickApprovedByBnkbuck() {
		return ClickApprovedByBnkbuck;
	}

	public WebElement getClickApprovedCashbuck() {
		return ClickApprovedCashbuck;
	}

	@FindBy(xpath=LocatorsRepository.selectCustNoSAP)
	private WebElement selectCustNoSAP;
	
	@FindBy(xpath=LocatorsRepository.selectReqRef)
	private WebElement selectReqRef;
	
	@FindBy(xpath=LocatorsRepository.selectRepayAmnt)
	private WebElement selectRepayAmnt;
	
	@FindBy(xpath=LocatorsRepository.selectTransCurr)
	private WebElement selectTransCurr;
	
	@FindBy(xpath=LocatorsRepository.selectShipVal)
	private WebElement selectShipVal;
	
	@FindBy(xpath=LocatorsRepository.selectAmntToPaid)
	private WebElement selectAmntToPaid;
	
	@FindBy(xpath=LocatorsRepository.selectTotlAmntpaid)
	private WebElement selectTotlAmntpaid;
	
	@FindBy(xpath=LocatorsRepository.selectPayType)
	private WebElement selectPayType;
	
	@FindBy(xpath=LocatorsRepository.selectRaisedBy)
	private WebElement selectRaisedBy;
	
	@FindBy(xpath=LocatorsRepository.selectAppRejBy)
	private WebElement selectAppRejBy;
	
	@FindBy(xpath=LocatorsRepository.selectAssignTo)
	private WebElement selectAssignTo;

	@FindBy(xpath=LocatorsRepository.selectTranscDt)
	private WebElement selectTranscDt;
	
	public WebElement getSelectTranscDt() {
		return selectTranscDt;
	}

	public WebElement getSelectCustNoSAP() {
		return selectCustNoSAP;
	}

	public WebElement getSelectReqRef() {
		return selectReqRef;
	}

	public WebElement getSelectRepayAmnt() {
		return selectRepayAmnt;
	}

	public WebElement getSelectTransCurr() {
		return selectTransCurr;
	}

	public WebElement getSelectShipVal() {
		return selectShipVal;
	}

	public WebElement getSelectAmntToPaid() {
		return selectAmntToPaid;
	}

	public WebElement getSelectTotlAmntpaid() {
		return selectTotlAmntpaid;
	}

	public WebElement getSelectPayType() {
		return selectPayType;
	}

	public WebElement getSelectRaisedBy() {
		return selectRaisedBy;
	}

	public WebElement getSelectAppRejBy() {
		return selectAppRejBy;
	}

	public WebElement getSelectAssignTo() {
		return selectAssignTo;
	}
	

}
