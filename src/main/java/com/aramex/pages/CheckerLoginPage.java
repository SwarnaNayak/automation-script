package com.aramex.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.aramex.base.BasePage;
import com.aramex.config.LocatorsRepository;

public class CheckerLoginPage extends BasePage {
	
	@FindBy(id=LocatorsRepository.userName)
	WebElement usernameby; 
	
	@FindBy(id=LocatorsRepository.passWord)
	WebElement passwordby; 
	
	@FindBy(xpath=LocatorsRepository.Login)
	WebElement login; 
	
	
	/*
	 * public LoginPage() {
	 * 
	 * PageFactory.initElements(driver, this); }
	 */
	 
	public CheckerLoginPage(WebDriver driver) {
		super(driver);
		PageFactory.initElements(driver, this);
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
		}}
	
	/*
	 * public LoginPage(WebDriver driver) { super(driver);
	 * PageFactory.initElements(driver, this); try { Thread.sleep(3000); } catch
	 * (InterruptedException e) { } }
	 */
	
	public boolean setUserName(String username){		
		usernameby.sendKeys(username);
		return false;		
	}
	
	public boolean setPassword(String password){		
		passwordby.sendKeys(password);
			return false;				
	}
		

	
	
}
