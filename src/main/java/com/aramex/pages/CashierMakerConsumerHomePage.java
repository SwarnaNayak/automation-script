package com.aramex.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.aramex.config.LocatorsRepository;

public class CashierMakerConsumerHomePage {

	
	@FindBy(xpath=LocatorsRepository.filterBy)
	private WebElement filterBy; 
	
	@FindBy(xpath=LocatorsRepository.selectMobileNumber)
	private WebElement selectMobileNumber;
	
	@FindBy(xpath=LocatorsRepository.selectConsumerName)
	private WebElement selectConsumerName;
	
	@FindBy(xpath=LocatorsRepository.basicSearchInputField)
	private WebElement basicSearchInputField;
	
	@FindBy(xpath=LocatorsRepository.clickonSearchoption)
	private WebElement clickonSearchoption;

	@FindBy(xpath=LocatorsRepository.clearfilter)
	private WebElement clearfilter;
	
	@FindBy(xpath=LocatorsRepository.selectEmail)
	private WebElement selectEmail;
	
	@FindBy(xpath=LocatorsRepository.selectLatestBatch)
	private WebElement selectLatestBatch;
	
	@FindBy(xpath=LocatorsRepository.selectAvailableLimit)
	private WebElement selectAvailableLimit;
	
	@FindBy(xpath=LocatorsRepository.selectLimitCurrency)
	private WebElement selectLimitCurrency;
	
	@FindBy(xpath=LocatorsRepository.selectConsumerStatus)
	private WebElement selectConsumerStatus;
	
	@FindBy(xpath=LocatorsRepository.selectCreditScore)
	private WebElement selectCreditScore;
	
	@FindBy(xpath=LocatorsRepository.selectPropensityScore)
	private WebElement selectPropensityScore;
	
	@FindBy(xpath=LocatorsRepository.selectSource)
	private WebElement selectSource;
	
	@FindBy(xpath=LocatorsRepository.selectConsumerNumber)
	private WebElement selectConsumerNumber;
	
	@FindBy(xpath=LocatorsRepository.selectLatestCampaign)
	private WebElement selectLatestCampaign;
	
	@FindBy(xpath=LocatorsRepository.selectLatestOrderDate)
	private WebElement selectLatestOrderDate;
	
	@FindBy(xpath=LocatorsRepository.selectCampaignTitle)
	private WebElement selectCampaignTitle;
	
	@FindBy(xpath=LocatorsRepository.selectCampaignStatus)
	private WebElement selectCampaignStatus;
	
	@FindBy(xpath=LocatorsRepository.selectLimit)
	private WebElement selectLimit;
	
	@FindBy(xpath=LocatorsRepository.advanceSearchButton)
	private WebElement advanceSearchButton;
	
	@FindBy(xpath=LocatorsRepository.variabledropdown)
	private WebElement variabledropdown;
	
	@FindBy(xpath=LocatorsRepository.Operatordropdown)
	private WebElement Operatordropdown;
	
	@FindBy(xpath=LocatorsRepository.Valueinputfield)
	private WebElement Valueinputfield;
	
	@FindBy(xpath=LocatorsRepository.operatorequalsTo)
	private WebElement operatorequalsTo;
	
	@FindBy(xpath=LocatorsRepository.operatornotEqualsTo)
	private WebElement operatornotEqualsTo;
	
	@FindBy(xpath=LocatorsRepository.applyfilter)
	private WebElement applyfilter;
	
	@FindBy(xpath=LocatorsRepository.addrow)
	private WebElement addrow;
	
	@FindBy(xpath=LocatorsRepository.caseSensitivechkbox)
	private WebElement caseSensitivechkbox;
	
	public WebElement getCaseSensitivechkbox() {
		return caseSensitivechkbox;
	}

	public WebElement getDeleteRow() {
		return deleteRow;
	}

	@FindBy(xpath=LocatorsRepository.deleteRow)
	private WebElement deleteRow;
	
	public WebElement getAddrow() {
		return addrow;
	}

	public WebElement getAdvanceSearchButton() {
		return advanceSearchButton;
	}

	public WebElement getVariabledropdown() {
		return variabledropdown;
	}

	public WebElement getOperatordropdown() {
		return Operatordropdown;
	}

	public WebElement getValueinputfield() {
		return Valueinputfield;
	}

	public WebElement getOperatorequalsTo() {
		return operatorequalsTo;
	}

	public WebElement getOperatornotEqualsTo() {
		return operatornotEqualsTo;
	}

	public WebElement getApplyfilter() {
		return applyfilter;
	}

	public WebElement getSelectEmail() {
		return selectEmail;
	}

	public WebElement getSelectLatestBatch() {
		return selectLatestBatch;
	}

	public WebElement getSelectAvailableLimit() {
		return selectAvailableLimit;
	}

	public WebElement getSelectLimitCurrency() {
		return selectLimitCurrency;
	}

	public WebElement getSelectConsumerStatus() {
		return selectConsumerStatus;
	}

	public WebElement getSelectCreditScore() {
		return selectCreditScore;
	}

	public WebElement getSelectPropensityScore() {
		return selectPropensityScore;
	}

	public WebElement getSelectSource() {
		return selectSource;
	}

	public WebElement getSelectConsumerNumber() {
		return selectConsumerNumber;
	}

	public WebElement getSelectLatestCampaign() {
		return selectLatestCampaign;
	}

	public WebElement getSelectLatestOrderDate() {
		return selectLatestOrderDate;
	}

	public WebElement getSelectCampaignTitle() {
		return selectCampaignTitle;
	}

	public WebElement getSelectCampaignStatus() {
		return selectCampaignStatus;
	}

	public WebElement getSelectLimit() {
		return selectLimit;
	}

	public WebElement getFilterBy() {
		return filterBy;
	}

	public WebElement getClearfilter() {
		return clearfilter;
	}

	public WebElement getSelectMobileNumber() {
		return selectMobileNumber;
	}

	public WebElement getSelectConsumerName() {
		return selectConsumerName;
	}

	public WebElement getBasicSearchInputField() {
		return basicSearchInputField;
	}

	public WebElement getClickonSearchoption() {
		return clickonSearchoption;
	}
}
