package com.aramex.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.aramex.config.LocatorsRepository;

public class FinanceCheckerPage {

	@FindBy(xpath=LocatorsRepository.selectSlno)
	private WebElement selectSlno;
	
	@FindBy(xpath=LocatorsRepository.selectDocumentno)
	private WebElement selectDocumentno;
	
	@FindBy(xpath=LocatorsRepository.selectSAPDocno)
	private WebElement selectSAPDocno;
	
	@FindBy(xpath=LocatorsRepository.selectShipmntID)
	private WebElement selectShipmntID;
	
	@FindBy(xpath=LocatorsRepository.selectConsumerNoSAP)
	private WebElement selectConsumerNoSAP;
	
	@FindBy(xpath=LocatorsRepository.selectEtailerAPNo)
	private WebElement selectEtailerAPNo;
	
	@FindBy(xpath=LocatorsRepository.selectDocType)
	private WebElement selectDocType;
	
	@FindBy(xpath=LocatorsRepository.selectTransDt)
	private WebElement selectTransDt;
	
	@FindBy(xpath=LocatorsRepository.selectTransType)
	private WebElement selectTransType;
	
	@FindBy(xpath=LocatorsRepository.selectTransAmnt)
	private WebElement selectTransAmnt;
	
	@FindBy(xpath=LocatorsRepository.selectTransStatus)
	private WebElement selectTransStatus;
	
	@FindBy(xpath=LocatorsRepository.selectTaxType)
	private WebElement selectTaxType;
	
	@FindBy(xpath=LocatorsRepository.selectTaxAmnt)
	private WebElement selectTaxAmnt;
	
	@FindBy(xpath=LocatorsRepository.selectTaxStatus)
	private WebElement selectTaxStatus;
	
	@FindBy(xpath=LocatorsRepository.selectConsumerNm)
	private WebElement selectConsumerNm;
	
	@FindBy(xpath=LocatorsRepository.selectConsMobNo)
	private WebElement selectConsMobNo;
	
	@FindBy(xpath=LocatorsRepository.selectEtailerNM)
	private WebElement selectEtailerNM;
	
	@FindBy(xpath=LocatorsRepository.selectEtailerID)
	private WebElement selectEtailerID;

	@FindBy(xpath=LocatorsRepository.TransDtfromMonth)
	private WebElement TransDtfromMonth;
	
	@FindBy(xpath=LocatorsRepository.TransDtfromDate)
	private WebElement TransDtfromDate;
	
	@FindBy(xpath=LocatorsRepository.TransDttoMonth)
	private WebElement TransDttoMonth;
	
	@FindBy(xpath=LocatorsRepository.TransDttoDate)
	private WebElement TransDttoDate;
	
	public WebElement getTransDtfromMonth() {
		return TransDtfromMonth;
	}

	public WebElement getTransDtfromDate() {
		return TransDtfromDate;
	}

	public WebElement getTransDttoMonth() {
		return TransDttoMonth;
	}

	public WebElement getTransDttoDate() {
		return TransDttoDate;
	}

	public WebElement getSelectSlno() {
		return selectSlno;
	}

	public WebElement getSelectDocumentno() {
		return selectDocumentno;
	}

	public WebElement getSelectSAPDocno() {
		return selectSAPDocno;
	}

	public WebElement getSelectShipmntID() {
		return selectShipmntID;
	}

	public WebElement getSelectConsumerNoSAP() {
		return selectConsumerNoSAP;
	}

	public WebElement getSelectEtailerAPNo() {
		return selectEtailerAPNo;
	}

	public WebElement getSelectDocType() {
		return selectDocType;
	}

	public WebElement getSelectTransDt() {
		return selectTransDt;
	}

	public WebElement getSelectTransType() {
		return selectTransType;
	}

	public WebElement getSelectTransAmnt() {
		return selectTransAmnt;
	}

	public WebElement getSelectTransStatus() {
		return selectTransStatus;
	}

	public WebElement getSelectTaxType() {
		return selectTaxType;
	}

	public WebElement getSelectTaxAmnt() {
		return selectTaxAmnt;
	}

	public WebElement getSelectTaxStatus() {
		return selectTaxStatus;
	}

	public WebElement getSelectConsumerNm() {
		return selectConsumerNm;
	}

	public WebElement getSelectConsMobNo() {
		return selectConsMobNo;
	}

	public WebElement getSelectEtailerNM() {
		return selectEtailerNM;
	}

	public WebElement getSelectEtailerID() {
		return selectEtailerID;
	}
	
	
}
