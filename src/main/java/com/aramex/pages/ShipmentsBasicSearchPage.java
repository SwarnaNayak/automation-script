package com.aramex.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import com.aramex.config.LocatorsRepository;

public class ShipmentsBasicSearchPage{
		
	public WebElement getSelectCustometName() {
		return selectCustometName;
	}

	public WebElement getSelectEtailer() {
		return selectEtailer;
	}

	public WebElement getSelectShipmentNumber() {
		return selectShipmentNumber;
	}

	public WebElement getSelectOrderNumber() {
		return selectOrderNumber;
	}

	public WebElement getSelectorigin() {
		return selectorigin;
	}

	public WebElement getSelectDestination() {
		return selectDestination;
	}

	public WebElement getSelectShipmentvalue() {
		return selectShipmentvalue;
	}

	public WebElement getSelectRemainingAmount() {
		return selectRemainingAmount;
	}

	public WebElement getSelectCurrencyCode() {
		return selectCurrencyCode;
	}

	public WebElement getSelectDuties() {
		return selectDuties;
	}

	public WebElement getSelectCharges() {
		return selectCharges;
	}

	public WebElement getSelectstatus() {
		return selectstatus;
	}

	public WebElement getSelectLoanStatus() {
		return selectLoanStatus;
	}

	public WebElement getSelectLoanProduct() {
		return selectLoanProduct;
	}

	public WebElement getSelectPickupDate() {
		return selectPickupDate;
	}

	public WebElement getSelectexptdtofdelvry() {
		return selectexptdtofdelvry;
	}
	
	@FindBy(xpath=LocatorsRepository.clickondropdownToNavigateShipment)
	private WebElement clickondropdownToNavigateShipment;
	
	@FindBy(xpath=LocatorsRepository.clickonshipmentmodule)
	private WebElement clickonshipmentmodule;

	public WebElement getClickondropdownToNavigateShipment() {
		return clickondropdownToNavigateShipment;
	}

	public WebElement getClickonshipmentmodule() {
		return clickonshipmentmodule;
	}

	@FindBy(xpath=LocatorsRepository.selectCustometName)
	private WebElement selectCustometName;
	
	@FindBy(xpath=LocatorsRepository.selectEtailer)
	private WebElement selectEtailer;
	
	@FindBy(xpath=LocatorsRepository.selectShipmentNumber)
	private WebElement selectShipmentNumber;
	
	@FindBy(xpath=LocatorsRepository.selectOrderNumber)
	private WebElement selectOrderNumber;
	
	@FindBy(xpath=LocatorsRepository.selectorigin)
	private WebElement selectorigin;
	
	@FindBy(xpath=LocatorsRepository.selectDestination)
	private WebElement selectDestination;
	
	@FindBy(xpath=LocatorsRepository.selectShipmentvalue)
	private WebElement selectShipmentvalue;
	
	@FindBy(xpath=LocatorsRepository.selectRemainingAmount)
	private WebElement selectRemainingAmount;
	
	@FindBy(xpath=LocatorsRepository.selectCurrencyCode)
	private WebElement selectCurrencyCode;
	
	@FindBy(xpath=LocatorsRepository.selectDuties)
	private WebElement selectDuties;
	
	@FindBy(xpath=LocatorsRepository.selectCharges)
	private WebElement selectCharges;
	
	@FindBy(xpath=LocatorsRepository.selectstatus)
	private WebElement selectstatus;
	
	@FindBy(xpath=LocatorsRepository.selectLoanStatus)
	private WebElement selectLoanStatus;
	
	@FindBy(xpath=LocatorsRepository.selectLoanProduct)
	private WebElement selectLoanProduct;
	
	@FindBy(xpath=LocatorsRepository.selectPickupDate)
	private WebElement selectPickupDate;
	
	@FindBy(xpath=LocatorsRepository.selectexptdtofdelvry)
	private WebElement selectexptdtofdelvry;
	
}
