package com.aramex.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.aramex.base.BasePage;
import com.aramex.base.TestBase;
import com.aramex.config.LocatorsRepository;

public class LoginToApp extends BasePage{

	@FindBy(id=LocatorsRepository.userName)
	WebElement usernameby; 
	
	@FindBy(id=LocatorsRepository.passWord)
	WebElement passwordby; 
	
	@FindBy(xpath=LocatorsRepository.Login)
	WebElement login; 
	
	
	public LoginToApp(WebDriver driver) {
		super(driver);
		PageFactory.initElements(driver, this);
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
		}}
	
}
