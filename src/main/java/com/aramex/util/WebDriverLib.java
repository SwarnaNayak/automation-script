package com.aramex.util;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.aramex.base.TestBase;

public class WebDriverLib extends TestBase{

	Logger LOGGER = Logger.getLogger(WebDriverLib.class.getName());
	public void waitForPageTobeLoad(){
		TestBase.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
	}
	
	public  void waitForXpathPresent(String xpath){
		WebDriverWait wait=new WebDriverWait(TestBase.driver, 20);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpath)));
	}
	
	public void waitForNamePresent(String name){
		WebDriverWait wait=new WebDriverWait(TestBase.driver, 20);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.name(name)));
	}
	
	public  void waitForTextPresent(String textToBeVerified ){
		WebDriverWait wait=new WebDriverWait(TestBase.driver, 20);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[contains(text(),'" + textToBeVerified + "')]")));
	}
	
	public void waitforvisibilityOfElementLocated(String xpath) {
	WebDriverWait wait = new WebDriverWait(driver,30);
	   wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpath)));
	}

	public  void waitForElementToClickble(String xpath){
		WebDriverWait wait=new WebDriverWait(TestBase.driver, 20);
		 wait.until(ExpectedConditions.elementToBeClickable(By.xpath(xpath)));
	}
	
	
	
	public List<WebElement> getRowData(String rowXpath){
		List<WebElement> lst=TestBase.driver.findElements(By.xpath(rowXpath));
		return lst;
	}
	
	public boolean acceptAlert(){
		boolean flag=false;
		try{
		Alert alt=TestBase.driver.switchTo().alert();
		LOGGER.info("The message dispalyed on the popup is -->>" +alt.getText());
		alt.accept();
		flag=true;
		}
		catch(NoAlertPresentException t){
			LOGGER.info("Alert is Not Present");
			
		}
		return flag;
	}
	
	public boolean dismissAlert(){
		boolean flag=false;
		try{
		Alert alt=TestBase.driver.switchTo().alert();
		LOGGER.info(alt.getText());
		alt.dismiss();
		flag=true;
		}
		catch(NoAlertPresentException t){
			LOGGER.info("Alert is Not Present");
			
		}
		return flag;
	}
	
	public boolean verifyText(String xpathOfActText, String expectedText){
		boolean flag=false;
		String actText=TestBase.driver.findElement(By.xpath(xpathOfActText)).getText();
		if(actText.equals(expectedText)){
			LOGGER.info(expectedText+" is Displayed in UI");
			flag=true;
		}
		else{
			LOGGER.info(expectedText+" is Not Displayed in UI");
		}
		return flag;
	}
	
	public  boolean verifyTextPresentInUI(String expectedText ){
		boolean flag=false;
		String entirePageSource=TestBase.driver.getPageSource();
		if(entirePageSource.contains(expectedText)){
			LOGGER.info(expectedText+" is Displayed in UI");
			flag=true;
		}
		else{
			LOGGER.info(expectedText+" is Not Displayed in UI");
		}
		return flag;
	}
	
	
}
