package com.aramex.util;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.io.FileHandler;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.aramex.base.TestBase;

public class TestUtil extends TestBase {

	public static long PAGE_LOAD_TIME=20;
	public static long IMPLICIT_WAIT=10;
		
	 public void captureScreen(WebDriver driver, String tname) throws IOException {
		 TakesScreenshot ts = (TakesScreenshot) driver;
		 File source = ts.getScreenshotAs(OutputType.FILE);
		 File target = new File(System.getProperty("user.dir") + "/ScreenShotDemo/" + tname + ".png");
		 FileUtils.copyFile(source, target);
		 System.out.println("Screenshot taken");
		 }
	 
	 
	 public static void takeScreenshotAtEndOfTest() throws IOException {
			File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
			//String currentDir = System.getProperty("user.dir");
			File DestFile = new File("C:\\Users\\Kuliza-367\\eclipse-workspace\\TestAutomationAramex\\screenshots\\" + System.currentTimeMillis() + ".png");
			FileHandler.copy(scrFile, DestFile);
		}
	
}
