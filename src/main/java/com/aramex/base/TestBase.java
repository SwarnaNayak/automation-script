package com.aramex.base;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import com.aramex.util.TestUtil;
import com.aramex.util.WebDriverLib;
import com.aramex.util.WebEventListener;


public class TestBase {
	
	public static WebDriver driver;
	public static Properties prop;
	public static EventFiringWebDriver e_driver;
	public static WebEventListener eventListener;
	static Logger log = Logger.getLogger("devpinoyLogger");
	static WebDriverLib wbLib=new WebDriverLib();
	
	
	public TestBase() {
		
		try {
			prop = new Properties();
			FileInputStream ip = new FileInputStream(System.getProperty("user.dir")+ "/src/main/java/com/aramex"
					+ "/config/config.properties");
			prop.load(ip);
			log.info("Configuration file is loaded successfully...!");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
						
	}
	
	
	public static void initialization() throws InterruptedException{
		
		String browserName = prop.getProperty("browser");
		
		if(browserName.equals("chrome")){
			
			System.out.println("user directory"+System.getProperty("user.dir"));
	    	System. setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + "\\Executables\\chromedriver.exe");	
	    	driver = new ChromeDriver();
			
		}
		else if(browserName.equals("FF")){
			 System. setProperty("webdriver.gecko.driver", System.getProperty("user.dir") + "\\Executables\\geckodriver.exe");
		  		driver = new FirefoxDriver();
		}
		
		
		e_driver = new EventFiringWebDriver(driver);
		// Now create object of EventListerHandler to register it with EventFiringWebDriver
		eventListener = new WebEventListener();
		e_driver.register(eventListener);
		driver = e_driver;
		
		driver.manage().window().maximize();
		log.info("************************Maximizing the window**************************");
		driver.manage().deleteAllCookies();
		driver.manage().timeouts().pageLoadTimeout(TestUtil.PAGE_LOAD_TIME, TimeUnit.SECONDS);
		driver.manage().timeouts().implicitlyWait(TestUtil.IMPLICIT_WAIT, TimeUnit.SECONDS);
		
		driver.get(prop.getProperty("LMSURL"));
		
		driver.manage().timeouts().pageLoadTimeout(TestUtil.PAGE_LOAD_TIME, TimeUnit.SECONDS);
		Thread.sleep(5000);
		log.info("************************Passing the URL into the browswer is done**************************");
		//ExtentTestManager.getTest().log(LogStatus.INFO, "Open Browser and navigate to Leadsportal");
		
		//driver.get(prop.getProperty("LMSURL"));
		
	}

	
	
}
