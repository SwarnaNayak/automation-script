package com.aramex.config;

import org.openqa.selenium.By;

public class LocatorsRepository {
	
		
	//Login Page Locators
	public static final String userName = "email";
	public static final String passWord = "password";
	public static final String Login = "//button[@class='ant-btn ant-btn-primary']";
	public static final String aramexLogo = "//img[@id='logo']";
	public static final String navigateToCahsierMkkr = "//span[contains(text(),'Cashier Maker')]";
	public static final String changetoCashierChkr = "//span[contains(text(),'Cashier Maker')]/preceding-sibling::i";
	public static final String navigateToCahsierChkr = "//span[contains(text(),'Cashier Checker')]";
	public static final String navigateToFinanceChkr = "//span[contains(text(),'Finance Checker')]";
	
	//Landing Page Locators
	public static final String clickOnRole = "//i[@class='anticon anticon-caret-right']";
	
	//Cashier Maker Home Page Locators
	
	public static final String filterBy = "//div[@class='filterSelect ant-select ant-select-enabled']";
	
	public static final String clearfilter = "//button[@id='clearFilter']";
	public static final String selectMobileNumber = "//li[contains(text(),'Mobile Number')]";
	public static final String selectConsumerName = "//li[contains(text(),'Consumer Name')]";
	public static final String selectEmail = "//li[contains(text(),'Email')]";
	public static final String selectLatestBatch = "//li[contains(text(),'Latest Batch')]";
	public static final String selectLimit = "//li[contains(text(),'Limit')]";
	public static final String selectAvailableLimit = "//li[contains(text(),'Available Limit')]";
	public static final String selectLimitCurrency = "//li[contains(text(),'Limit Currency')]";
	public static final String selectConsumerStatus = "//li[contains(text(),'Consumer Status')]";
	public static final String selectCreditScore = "//li[contains(text(),'Credit Score')]";
	public static final String selectPropensityScore = "//li[contains(text(),'Propensity Score')]";
	public static final String selectSource = "//li[contains(text(),'Source')]";
	public static final String selectConsumerNumber = "//li[contains(text(),'Consumer Number')]";
	public static final String selectLatestCampaign = "//li[contains(text(),'Latest Campaign')]";
	public static final String selectLatestOrderDate = "//li[contains(text(),'Latest Order Date')]";
	public static final String selectCampaignTitle = "//li[contains(text(),'Campaign Title')]";
	public static final String selectCampaignStatus = "//li[contains(text(),'Campaign Status')]";
	public static final String basicSearchInputField = "//input[contains(@placeholder,'Search')]";
	public static final String clickonSearchoption = "//button[contains(@class,'ant-btn ant-input-search-button ant-btn-primary')]";
	public static final String getfirstcolumndata = "//td[contains(text(),'')][1]";
	public static final String campaignTitleField = "//span[contains(text(),'CAMPAIGN STATUS')]";
			
	public static final String advanceSearchButton = "//button[@id='advanceSearch2']";
	public static final String variabledropdown = "//label[contains(text(),'Variable')]/../following-sibling::div";
	public static final String Operatordropdown = "//label[contains(text(),'Operator')]/../following-sibling::div";
	public static final String Valueinputfield = "//span[@class='ant-form-item-children']//input[@class='ant-input']";
	
	public static final String operatorequalsTo = "//li[text()='=']";
	public static final String operatornotEqualsTo = "//li[text()='!=']";
	public static final String operatorgreaterthanEqualsTo = "//li[contains(text(),'>=')]";
	public static final String operatornotlessthanEqualsTo = "//li[text()='<=']";
	public static final String operatorlessthan = "//li[text()='<']";
	public static final String operatorgreaterthan = "//li[text()='>']";
	public static final String operatorin = "//li[text()='in']";
	public static final String operatorrange = "//li[text()='range']";
	public static final String operatorLike= "//li[text()='like']";

	public static final String applyfilter = "//button[@id='applyFilter']";
	public static final String addrow = "//button[@id='addARow']";
	public static final String deleteRow = "//tr[1]//td[5]//div[1]";
	public static final String caseSensitivechkbox = "//label[contains(text(),'Case-sensitive?')]/../following-sibling::div//span";
	public static final String addsymbol = "//label[contains(text(),'Value')]/../following-sibling::div//i";
	public static final String rangeinput1 = "//label[contains(text(),'Value (From-To)')]/../following-sibling::div//input[1]";
	public static final String rangeinpuut2 = "//label[contains(text(),'Value (From-To)')]/../following-sibling::div//input[2]";

	//Shipments Basic Search page locators
	public static final String clickondropdownToNavigateShipment = "//span[contains(text(),'Consumers')]/preceding-sibling::i";
	public static final String clickonshipmentmodule = "//li[contains(text(),'Shipments')]";
	public static final String selectCustometName = "//li[contains(text(),'Customer Name')]";
	public static final String selectEtailer = "//li[contains(text(),'Etailer')]";
	public static final String selectShipmentNumber = "//li[contains(text(),'Shipment Number')]";
	public static final String selectOrderNumber = "//li[contains(text(),'Order Number')]";
	public static final String selectorigin = "//li[contains(text(),'Origin')]";
	public static final String selectDestination = "//li[contains(text(),'Destination')]";
	public static final String selectShipmentvalue = "//li[contains(text(),'Shipment Value')]";
	public static final String selectRemainingAmount = "//li[contains(text(),'Remaining Amount')]";
	public static final String selectCurrencyCode = "//li[contains(text(),'Currency Code')]";
	public static final String selectDuties= "//li[contains(text(),'Duties')]";
	public static final String selectCharges = "//li[contains(text(),'Charges')]";
	public static final String selectstatus = "//li[contains(text(),'Status')]";
	public static final String selectLoanStatus = "//li[contains(text(),'Loan Status')]";
	public static final String selectLoanProduct = "//li[contains(text(),'Loan Product')]";
	public static final String selectPickupDate= "//li[contains(text(),'Pickup Date')]";
	public static final String selectexptdtofdelvry = "//li[contains(text(),'Expected Date of Delivery')]";
	public static final String datevaluefrom = "//span[@class='ant-form-item-children']//span[1]//div[1]//input[1]";
	public static final String datevalueTo = "//span[@class='ant-form-item-children']//span[2]//div[1]//input[1]";
	public static final String monthSelectr = "//a[@class='ant-calendar-month-select']";
	public static final String fromMonth = "//a[contains(text(),'Apr')]";
	public static final String fromDate = "//td[@title='April 3, 2020']";
	public static final String toMonth = "//a[contains(text(),'Jun')]";
	public static final String toDate = "//td[@title='June 10, 2020']";
	
	//Shipment Detail View Page
	public static final String shipmntselctarrow = "//div//span[text()='Raise']//ancestor::div[2]//span[@class='ant-select-arrow']";
	public static final String selectPayByCash = "//li[contains(text(),'Pay by Cash')]";
	public static final String selectPayByCard = "//li[contains(text(),'Pay by Card')]";
	public static final String selectPayBYBnkTrsnf = "//li[contains(text(),'Pay by Bank Transfer')]";
	public static final String clkRaise = "//div//span[text()='Raise']";
	public static final String transactioninput = "//span[text()='Transaction Amount']//ancestor::div[3]//input";
	//public static final String transactiondt = "//span[text()='Raise']";
	public static final String comments = "//span[text()='Comments']//ancestor::div[3]//input";
	public static final String paybtn = "//span[text()='Pay']";
	public static final String clkonOK = "//span[text()='OK']";
	public static final String clkonCancel = "//span[text()='Cancel']";
	public static final String clkonlisting= "//a[contains(text(),'Listing')]";
	public static final String cardLast4Digit = "//span[text()='Card Last 4 digit']//ancestor::div[3]//input";
	public static final String authCode = "//span[text()='Authorization Code']//ancestor::div[3]//input";
	public static final String bnkaccntNo = "//span[text()='Bank Account Number']//ancestor::div[3]//input";
	public static final String tranfrReff= "//span[text()='Transfer Reference']//ancestor::div[3]//input";



    //Service Request Page
	public static final String clickondropdownToNavigateServiceRq= "//span[contains(text(),'Shipments')]/preceding-sibling::i";
	public static final String ClickMYRequestbucket = "//div[text()='My Request']";
	public static final String clickonServiceRQ = "//li[contains(text(),'Service Request')]";
	public static final String selectCustNoSAP = "//li[contains(text(),'Customer Number(SAP)')]";
	public static final String selectReqRef = "//li[contains(text(),'Request Reference')]";
	public static final String selectRepayAmnt = "//li[contains(text(),'Repayment Amount')]";
	public static final String selectTransCurr = "//li[contains(text(),'Transaction Currency')]";
	public static final String selectShipVal = "//li[contains(text(),'Shipment Value')]";
	public static final String selectAmntToPaid = "//li[contains(text(),'Amount To Be Paid')]";
	public static final String selectTotlAmntpaid = "//li[contains(text(),'Total Paid Amount')]";
	public static final String selectPayType = "//li[contains(text(),'Payment Type')]";
	public static final String selectTranscDt = "//li[contains(text(),'Transaction Date')]";
	public static final String selectRaisedBy = "//li[contains(text(),'Raised By')]";
	public static final String selectAppRejBy = "//li[contains(text(),'Approved/Rejected By')]";
	public static final String selectAssignTo = "//li[contains(text(),'Assigned To')]";
	public static final String ClickApprovedCashbuck = "//div[text()='Approved Cash']";
	public static final String ClickAllTransactions = "//span[text()='All Transactions']";
	public static final String ClickApprovedCardbuck = "//div[text()='Approved Card']";
	public static final String ClickApprovedByBnkbuck = "//div[text()='Approved Bank Transfer']";

					
	//Cashier Checker
	public static final String AppRejPaymntbtn = "//span[text()='Approve/ Reject Payment']";
	public static final String chkActionDrpdwn= "//span[contains(text(),'Checker Action')]/ancestor::div[@class='ant-col ant-col-12 ant-form-item-label']/following-sibling::div//i";
	public static final String approveByChkr= "//li[contains(text(),'Approve')]";
	public static final String rejectByChkr= "//li[contains(text(),'Reject')]";
	public static final String submitbtn= "//span[text()='Submit']";
	public static final String Caschkrchkbox= "//span[contains(text(),'MOBILE NUMBER')]//following::input[1]";
	public static final String viewTotal= "//span[contains(text(),'View Total')]";
	public static final String updateRepaymnt= "//span[contains(text(),'Update Repayment')]";
	public static final String dropdownOpen= "//button[@class='ant-btn ant-dropdown-trigger ant-dropdown-open']";
	public static final String cancelReapymnt= "//li[contains(text(),'Cancel Repayment')]";
	public static final String genrtDCRBnkTrnsf= "//li[contains(text(),'Generate DCR Bank Transfer')]";
	public static final String rejectbckt = "//div[text()='Rejected']";
	public static final String paymentType="//span[contains(text(),'Payment Type')]/ancestor::div[@class='ant-col ant-col-12 ant-form-item-label']/following-sibling::div//i";
	public static final String cardType="//li[contains(text(),'Card')]";
	public static final String cashType="//li[contains(text(),'Cash')]";
	public static final String bankTrasfrType="//li[contains(text(),'Bank Transfer')]";					
											
													
	//Finance Checker
	public static final String selectSlno = "//li[contains(text(),'Sl No')]";
	public static final String selectDocumentno = "//li[contains(text(),'Document Number')]";
	public static final String selectSAPDocno = "//li[contains(text(),'SAP Document No')]";
	public static final String selectShipmntID = "//li[contains(text(),'Shipment ID')]";
	public static final String selectConsumerNoSAP = "//li[contains(text(),'Consumer Number (SAP)')]";
	public static final String selectEtailerAPNo = "//li[contains(text(),'Etailer AP Number')]";
	public static final String selectDocType = "//li[contains(text(),'Document Type')]";
	public static final String selectTransDt = "//li[contains(text(),'Transaction Date')]";
	public static final String selectTransType = "//li[contains(text(),'Transaction Type')]";
	public static final String selectTransAmnt = "//li[contains(text(),'Transaction Amount')]";
	public static final String selectTransStatus= "//li[contains(text(),'Transaction Status')]";
	public static final String selectTaxType = "//li[contains(text(),'Tax Type')]";
	public static final String selectTaxAmnt = "//li[contains(text(),'Tax Amount')]";
	public static final String selectTaxStatus = "//li[contains(text(),'Tax Status')]";
	public static final String selectConsumerNm= "//li[contains(text(),'Consumer Name')]";
	public static final String selectConsMobNo = "//li[contains(text(),'Consumer Mobile Number')]";
	public static final String selectEtailerNM= "//li[contains(text(),'Etailer name')]";
	public static final String selectEtailerID= "//li[contains(text(),'Etailer ID')]";
	
	public static final String TransDtfromMonth = "//a[contains(text(),'Sep')]";
	public static final String TransDtfromDate = "//td[@title='September 1, 2020']";
	public static final String TransDttoMonth = "//a[contains(text(),'Sep')]";
	public static final String TransDttoDate = "//td[@title='September 3, 2020']";
	
			
			
					
							
									
											
													
															
																	
	
											
													
															
									
											
													
	
					
	
	
							
	

}
