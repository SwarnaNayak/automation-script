package com.aramex.tests;

import java.io.IOException;
import java.util.logging.Logger;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.aramex.base.TestBase;
import com.aramex.pages.CashierMakerConsumerHomePage;
import com.aramex.pages.LoginPage;
import com.aramex.util.ExcelLib;
import com.aramex.util.TestUtil;
import com.aramex.util.WebDriverLib;

public class ConsumerBasicSearchTest extends TestBase{

	WebDriverLib wbLib=new WebDriverLib();
	Logger LOGGER = Logger.getLogger(ConsumerBasicSearchTest.class.getName());
	LoginTest logintest = new LoginTest();
	LoginPage devHomePage=PageFactory.initElements(TestBase.driver, LoginPage.class);
	CashierMakerConsumerHomePage homePage=PageFactory.initElements(TestBase.driver, CashierMakerConsumerHomePage.class);
	TestUtil testutil =  new TestUtil();
	String lmsun1="";
	String lmspwd1="";
	
	@BeforeMethod
	public void logintoAramexAppliction() throws InterruptedException {
		logintest.loginToAppAsTester(prop.getProperty("lmsusername"), prop.getProperty("lmspassword"));
		
	}
	
	@Test(priority=2) 
	  public void consumerbasicSearchMobileNumber() throws InterruptedException, InvalidFormatException, IOException {
		
		wbLib.waitForPageTobeLoad();
		  Thread.sleep(5000);
		  CashierMakerConsumerHomePage homePage=PageFactory.initElements(TestBase.driver, CashierMakerConsumerHomePage.class);
		  homePage.getBasicSearchInputField().click();
		  homePage.getBasicSearchInputField().sendKeys(prop.getProperty("SearchbyMobilenumber"));
		  homePage.getClickonSearchoption().click();
		 
		  Thread.sleep(5000);
	    // Assert.assertEquals(prop.getProperty("SearchbyMobilenumber"), driver.findElement(By.xpath("//td[contains(text(),'')][1]")).getText());
         
		  boolean mobileno = driver.getPageSource().contains(prop.getProperty("SearchbyMobilenumber"));
		  if(mobileno==true)
		  {
			  Assert.assertTrue(true);
			  LOGGER.info("mobile basicsearch test case passed...");
			  testutil.captureScreen(driver, "mobilenumbersearchpassed");
		  }
		  else {
			  LOGGER.info("mobile basic search test case failed...");
			  testutil.captureScreen(driver, "mobilenumbersearchfailed");
			  Assert.assertTrue(false);
		  }

		  homePage.getClearfilter().click();
		 // testutil.takeScreenshotAtEndOfTest();
		  
		  Thread.sleep(9000);
	}
	

	@Test(priority=1) 
	  public void consumerbasicSearchfunctionalityConsumerNM() throws InterruptedException, InvalidFormatException, IOException {
		wbLib.waitForPageTobeLoad();
		  Thread.sleep(5000);
		  CashierMakerConsumerHomePage homePage=PageFactory.initElements(TestBase.driver, CashierMakerConsumerHomePage.class);
		  homePage.getFilterBy().click();
		  homePage.getSelectConsumerName().click();
		  homePage.getBasicSearchInputField().click();
		  homePage.getBasicSearchInputField().sendKeys(prop.getProperty("ConsumerName"));
		  homePage.getClickonSearchoption().click();
		  //testutil.getScreenShot("ConsumerNamescreen");
		  Thread.sleep(5000);
	     // Assert.assertEquals(prop.getProperty("ConsumerName"), driver.findElement(By.xpath("//td[contains(text(),'')][2]")).getText());
		  boolean consumernm = driver.getPageSource().contains(prop.getProperty("ConsumerName"));
		  if(consumernm==true)
		  {
			  Assert.assertTrue(true);
			  LOGGER.info("consumername basic search test case passed...");
			  testutil.captureScreen(driver, "ConsumerName search passed");
		  }
		  else {
			  LOGGER.info("consumername basic search test case failed...");
			  testutil.captureScreen(driver, "ConsumerName search failed");
			  Assert.assertTrue(false);
		  }
		  homePage.getClearfilter().click();
		  Thread.sleep(5000);
	}

	@Test(priority=3) 
	  public void consumerbasicSearchfunctionalityEmail() throws InterruptedException, InvalidFormatException, IOException {
		wbLib.waitForPageTobeLoad();
		  Thread.sleep(5000);
		  CashierMakerConsumerHomePage homePage=PageFactory.initElements(TestBase.driver, CashierMakerConsumerHomePage.class);
		  homePage.getFilterBy().click();
		  homePage.getSelectEmail().click();
		  homePage.getBasicSearchInputField().click();
		  homePage.getBasicSearchInputField().sendKeys(prop.getProperty("Email"));
		  homePage.getClickonSearchoption().click();
		  //testutil.getScreenShot("ConsumerNamescreen");
		  Thread.sleep(5000);

		  boolean email = driver.getPageSource().contains(prop.getProperty("Email"));
		  if(email==true)
		  {
			  Assert.assertTrue(true);
			  LOGGER.info("email basicsearch test case passed...");
			  testutil.captureScreen(driver, "emailsearchpassed");
		  }
		  else {
			  LOGGER.info("email basicsearch test case failed...");
			  testutil.captureScreen(driver, "emailsearchfailed");
			  Assert.assertTrue(false);
		  }
		  
		  homePage.getClearfilter().click();
		  Thread.sleep(5000);
	}
	
	@Test(priority=4) 
	  public void consumerbasicSearchfunctionalityLatestBatch() throws InterruptedException, InvalidFormatException, IOException {
		wbLib.waitForPageTobeLoad();
		  Thread.sleep(5000);
		  CashierMakerConsumerHomePage homePage=PageFactory.initElements(TestBase.driver, CashierMakerConsumerHomePage.class);
		  homePage.getFilterBy().click();
		  homePage.getSelectLatestBatch().click();
		  homePage.getBasicSearchInputField().click();
		  homePage.getBasicSearchInputField().sendKeys(prop.getProperty("LatestBatch"));
		  homePage.getClickonSearchoption().click();
		  //testutil.getScreenShot("ConsumerNamescreen");
		  Thread.sleep(5000);

		  boolean latestBatch = driver.getPageSource().contains(prop.getProperty("LatestBatch"));
		  if(latestBatch==true)
		  {
			  Assert.assertTrue(true);
			  LOGGER.info("latestBatch basicsearch test case passed...");
			  testutil.captureScreen(driver, "latestBatch basicsearch passed");
		  }
		  else {
			  LOGGER.info("latestBatch basic search test case failed...");
			  testutil.captureScreen(driver, "latestBatch basicsearch failed");
			  Assert.assertTrue(false);
		  }
		  
		  homePage.getClearfilter().click();
		  Thread.sleep(5000);
	}
	
	@Test(priority=5) 
	  public void consumerbasicSearchfunctionalityLimit() throws InterruptedException, InvalidFormatException, IOException {
		wbLib.waitForPageTobeLoad();
		  Thread.sleep(5000);
		  CashierMakerConsumerHomePage homePage=PageFactory.initElements(TestBase.driver, CashierMakerConsumerHomePage.class);
		  homePage.getFilterBy().click();
		  homePage.getSelectLimit().click();
		  homePage.getBasicSearchInputField().click();
		  homePage.getBasicSearchInputField().sendKeys(prop.getProperty("Limit"));
		  homePage.getClickonSearchoption().click();
		  //testutil.getScreenShot("ConsumerNamescreen");
		  Thread.sleep(5000);

		  boolean limit = driver.getPageSource().contains(prop.getProperty("Limit"));
		  if(limit==true)
		  {
			  Assert.assertTrue(true);
			  LOGGER.info("limit basicsearch test case passed...");
			  testutil.captureScreen(driver, "limit basicsearch passed");
		  }
		  else {
			  LOGGER.info("limit basic search test case failed...");
			  testutil.captureScreen(driver, "limit basicsearch failed");
			  Assert.assertTrue(false);
		  }
		  
		  homePage.getClearfilter().click();
		  Thread.sleep(5000);
	}
	
	@Test(priority=6) 
	  public void basicSearchfunctionalityAvailableLimit() throws InterruptedException, InvalidFormatException, IOException {
		wbLib.waitForPageTobeLoad();
		  Thread.sleep(5000);
		  CashierMakerConsumerHomePage homePage=PageFactory.initElements(TestBase.driver, CashierMakerConsumerHomePage.class);
		  homePage.getFilterBy().click();
		  homePage.getSelectAvailableLimit().click();
		  homePage.getBasicSearchInputField().click();
		  homePage.getBasicSearchInputField().sendKeys(prop.getProperty("AvailableLimit"));
		  homePage.getClickonSearchoption().click();
		  //testutil.getScreenShot("ConsumerNamescreen");
		  Thread.sleep(5000);

		  boolean availableLimit = driver.getPageSource().contains(prop.getProperty("AvailableLimit"));
		  if(availableLimit==true)
		  {
			  Assert.assertTrue(true);
			  LOGGER.info("availableLimit basicsearch test case passed...");
			  testutil.captureScreen(driver, "availableLimit basicsearch passed");
		  }
		  else {
			  LOGGER.info("availableLimit basic search test case failed...");
			  testutil.captureScreen(driver, "availableLimit basicsearch failed");
			  Assert.assertTrue(false);
		  }
		  
		  homePage.getClearfilter().click();
		  Thread.sleep(5000);
	}
	
	@Test(priority=7) 
	  public void basicSearchfunctionalityLimitCurrency() throws InterruptedException, InvalidFormatException, IOException {
		wbLib.waitForPageTobeLoad();
		  Thread.sleep(5000);
		  CashierMakerConsumerHomePage homePage=PageFactory.initElements(TestBase.driver, CashierMakerConsumerHomePage.class);
		  homePage.getFilterBy().click();
		  homePage.getSelectLimitCurrency().click();
		  homePage.getBasicSearchInputField().click();
		  homePage.getBasicSearchInputField().sendKeys(prop.getProperty("LimitCurrency"));
		  homePage.getClickonSearchoption().click();
		  //testutil.getScreenShot("ConsumerNamescreen");
		  Thread.sleep(5000);

		  boolean limitCurrency = driver.getPageSource().contains(prop.getProperty("LimitCurrency"));
		  if(limitCurrency==true)
		  {
			  Assert.assertTrue(true);
			  LOGGER.info("LimitCurrency basicsearch test case passed...");
			  testutil.captureScreen(driver, "LimitCurrency basicsearch passed");
		  }
		  else {
			  LOGGER.info("LimitCurrency basic search test case failed...");
			  testutil.captureScreen(driver, "LimitCurrency basicsearch failed");
			  Assert.assertTrue(false);
		  }
		  
		  homePage.getClearfilter().click();
		  Thread.sleep(5000);
	}
	
	@Test(priority=8) 
	  public void basicSearchfunctionalityConsumerStatus() throws InterruptedException, InvalidFormatException, IOException {
		wbLib.waitForPageTobeLoad();
		  Thread.sleep(5000);
		  CashierMakerConsumerHomePage homePage=PageFactory.initElements(TestBase.driver, CashierMakerConsumerHomePage.class);
		  homePage.getFilterBy().click();
		  homePage.getSelectLimit().click();
		  homePage.getBasicSearchInputField().click();
		  homePage.getBasicSearchInputField().sendKeys(prop.getProperty("ConsumerStatus"));
		  homePage.getClickonSearchoption().click();
		  //testutil.getScreenShot("ConsumerNamescreen");
		  Thread.sleep(5000);

		  boolean consumerStatus = driver.getPageSource().contains(prop.getProperty("ConsumerStatus"));
		  if(consumerStatus==true)
		  {
			  Assert.assertTrue(true);
			  LOGGER.info("consumerStatus basicsearch test case passed...");
			  testutil.captureScreen(driver, "consumerStatus basicsearch passed");
		  }
		  else {
			  LOGGER.info("consumerStatus basic search test case failed...");
			  testutil.captureScreen(driver, "consumerStatus basicsearch failed");
			  Assert.assertTrue(false);
		  }
		  
		  homePage.getClearfilter().click();
		  Thread.sleep(5000);
	}
	
	@Test(priority=9) 
	  public void basicSearchfunctionalityCreditScore() throws InterruptedException, InvalidFormatException, IOException {
		wbLib.waitForPageTobeLoad();
		  Thread.sleep(5000);
		  CashierMakerConsumerHomePage homePage=PageFactory.initElements(TestBase.driver, CashierMakerConsumerHomePage.class);
		  homePage.getFilterBy().click();
		  homePage.getSelectCreditScore().click();
		  homePage.getBasicSearchInputField().click();
		  homePage.getBasicSearchInputField().sendKeys(prop.getProperty("CreditScore"));
		  homePage.getClickonSearchoption().click();
		  //testutil.getScreenShot("ConsumerNamescreen");
		  Thread.sleep(5000);

		  boolean creditScore = driver.getPageSource().contains(prop.getProperty("CreditScore"));
		  if(creditScore==true)
		  {
			  Assert.assertTrue(true);
			  LOGGER.info("creditScore basicsearch test case passed...");
			  testutil.captureScreen(driver, "creditScore search passed");
		  }
		  else {
			  LOGGER.info("creditScore basic search test case failed...");
			  testutil.captureScreen(driver, "creditScore basicsearch failed");
			  Assert.assertTrue(false);
		  }
		  
		  homePage.getClearfilter().click();
		  Thread.sleep(5000);
	}
	
	@Test(priority=10) 
	  public void basicSearchfunctionalityPropernsityScore() throws InterruptedException, InvalidFormatException, IOException {
		wbLib.waitForPageTobeLoad();
		  Thread.sleep(5000);
		  CashierMakerConsumerHomePage homePage=PageFactory.initElements(TestBase.driver, CashierMakerConsumerHomePage.class);
		  homePage.getFilterBy().click();
		  homePage.getSelectPropensityScore().click();
		  homePage.getBasicSearchInputField().click();
		  homePage.getBasicSearchInputField().sendKeys(prop.getProperty("PropernsityScore"));
		  homePage.getClickonSearchoption().click();
		  //testutil.getScreenShot("ConsumerNamescreen");
		  Thread.sleep(5000);

		  boolean propernsityScore = driver.getPageSource().contains(prop.getProperty("PropernsityScore"));
		  if(propernsityScore==true)
		  {
			  Assert.assertTrue(true);
			  LOGGER.info("propernsityScore basicsearch test case passed...");
			  testutil.captureScreen(driver, "propernsityScore basicsearch passed");
		  }
		  else {
			  LOGGER.info("propernsityScore basicsearch test case failed...");
			  testutil.captureScreen(driver, "propernsityScore basicsearch failed");
			  Assert.assertTrue(false);
		  }
		  
		  homePage.getClearfilter().click();
		  Thread.sleep(5000);
	}
	
	@Test(priority=11) 
	  public void basicSearchfunctionalitySource() throws InterruptedException, InvalidFormatException, IOException {
		wbLib.waitForPageTobeLoad();
		  Thread.sleep(5000);
		  CashierMakerConsumerHomePage homePage=PageFactory.initElements(TestBase.driver, CashierMakerConsumerHomePage.class);
		  homePage.getFilterBy().click();
		  homePage.getSelectSource().click();
		  homePage.getBasicSearchInputField().click();
		  homePage.getBasicSearchInputField().sendKeys(prop.getProperty("Source"));
		  homePage.getClickonSearchoption().click();
		  //testutil.getScreenShot("ConsumerNamescreen");
		  Thread.sleep(5000);

		  boolean source = driver.getPageSource().contains(prop.getProperty("Source"));
		  if(source==true)
		  {
			  Assert.assertTrue(true);
			  LOGGER.info("source basicsearch test case passed...");
			  testutil.captureScreen(driver, "source field basicsearch passed");
		  }
		  else {
			  LOGGER.info("source basic search test case failed...");
			  testutil.captureScreen(driver, "source field basicsearch failed");
			  Assert.assertTrue(false);
		  }
		  
		  homePage.getClearfilter().click();
		  Thread.sleep(5000);
	}
	
	@Test(priority=12) 
	  public void basicSearchfunctionalityConsumerNumber() throws InterruptedException, InvalidFormatException, IOException {
		wbLib.waitForPageTobeLoad();
		  Thread.sleep(5000);
		  CashierMakerConsumerHomePage homePage=PageFactory.initElements(TestBase.driver, CashierMakerConsumerHomePage.class);
		  homePage.getFilterBy().click();
		  homePage.getSelectConsumerNumber().click();
		  homePage.getBasicSearchInputField().click();
		  homePage.getBasicSearchInputField().sendKeys(prop.getProperty("ConsumerNumber"));
		  homePage.getClickonSearchoption().click();
		  //testutil.getScreenShot("ConsumerNamescreen");
		  Thread.sleep(5000);

		  boolean consumerno = driver.getPageSource().contains(prop.getProperty("ConsumerNumber"));
		  if(consumerno==true)
		  {
			  Assert.assertTrue(true);
			  LOGGER.info("consumerno basicsearch test case passed...");
			  testutil.captureScreen(driver, "consumerNo search passed");
		  }
		  else {
			  LOGGER.info("consumerno basic search test case failed...");
			  testutil.captureScreen(driver, "consumerNo search failed");
			  Assert.assertTrue(false);
		  }
		  
		  homePage.getClearfilter().click();
		  Thread.sleep(5000);
	}
	@AfterMethod
	public void tearDown(){;
		driver.quit();
	}
	
}
