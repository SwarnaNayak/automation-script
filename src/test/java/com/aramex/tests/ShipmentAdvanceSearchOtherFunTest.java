package com.aramex.tests;

import java.io.IOException;
import java.util.logging.Logger;

import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.aramex.base.TestBase;
import com.aramex.pages.CashierMakerConsumerHomePage;
import com.aramex.pages.LoginPage;
import com.aramex.pages.ShipmentsBasicSearchPage;
import com.aramex.util.TestUtil;
import com.aramex.util.WebDriverLib;

public class ShipmentAdvanceSearchOtherFunTest extends TestBase{

	WebDriverLib wbLib=new WebDriverLib();
	Logger LOGGER = Logger.getLogger(ShipmentAdvanceSearchOtherFunTest.class.getName());
	LoginTest logintest = new LoginTest();
	LoginPage devHomePage=PageFactory.initElements(TestBase.driver, LoginPage.class);
	CashierMakerConsumerHomePage homePage=PageFactory.initElements(TestBase.driver, CashierMakerConsumerHomePage.class);
	TestUtil testutil =  new TestUtil();

	
	@BeforeMethod
	public void logintoAramexAppliction() throws InterruptedException {
		logintest.loginToAppAsTester(prop.getProperty("lmsusername"), prop.getProperty("lmspassword"));
		
	}
	@Test(priority=2) 
	  public void shipmentadvanceSearchAddMulipleRow() throws InterruptedException, IOException {
		
		wbLib.waitForPageTobeLoad();
		  Thread.sleep(5000);
		  CashierMakerConsumerHomePage homePage=PageFactory.initElements(TestBase.driver, CashierMakerConsumerHomePage.class);
		  ShipmentsBasicSearchPage shipmntbasicsrch=PageFactory.initElements(TestBase.driver, ShipmentsBasicSearchPage.class);
		  shipmntbasicsrch.getClickondropdownToNavigateShipment().click();
		  shipmntbasicsrch.getClickonshipmentmodule().click();
		  Thread.sleep(5000);
		  homePage.getAdvanceSearchButton().click();
		  homePage.getVariabledropdown().click();
		  homePage.getSelectMobileNumber().click();
		  homePage.getOperatordropdown().click();
		  homePage.getOperatorequalsTo().click();
		  Thread.sleep(2000);
		  homePage.getValueinputfield().sendKeys(prop.getProperty("ShipmentMobileNo"));
		  homePage.getAddrow().click();
		  testutil.captureScreen(driver, "ShipmentMobileNoadd 1st row");
		  
		  //add2nd row
		  homePage.getVariabledropdown().click();
		  shipmntbasicsrch.getSelectOrderNumber().click();
		  homePage.getOperatordropdown().click();
		  homePage.getOperatorequalsTo().click();
		  Thread.sleep(2000);
		  homePage.getValueinputfield().sendKeys(prop.getProperty("OrderNo2"));
		  homePage.getAddrow().click();
		  testutil.captureScreen(driver, "OrderNoadd 2nd row");
		  
		//add3rd row
		  homePage.getVariabledropdown().click();
		  shipmntbasicsrch.getSelectCustometName().click();
		  homePage.getOperatordropdown().click();
		  homePage.getOperatorequalsTo().click();
		  Thread.sleep(2000);
		  homePage.getValueinputfield().sendKeys(prop.getProperty("CustomerNM2"));
		  homePage.getAddrow().click();
		  testutil.captureScreen(driver, "CustomerNM2add 3rd row");
		  
		  homePage.getApplyfilter().click();
		  Thread.sleep(7000);
		  driver.findElement(By.xpath("//td[text()='"+prop.getProperty("CustomerNM2")+"']")).click();
		  Thread.sleep(8000);
		  testutil.captureScreen(driver, "after addrow finaloutcome screen");
		
	}
	
	@Test(priority=3) 
	  public void shipmentadvanceSearchDeleteRow() throws InterruptedException, IOException {
		
		wbLib.waitForPageTobeLoad();
		  Thread.sleep(5000);
		  CashierMakerConsumerHomePage homePage=PageFactory.initElements(TestBase.driver, CashierMakerConsumerHomePage.class);
		  ShipmentsBasicSearchPage shipmntbasicsrch=PageFactory.initElements(TestBase.driver, ShipmentsBasicSearchPage.class);
		  shipmntbasicsrch.getClickondropdownToNavigateShipment().click();
		  shipmntbasicsrch.getClickonshipmentmodule().click();
		  Thread.sleep(5000);
		  homePage.getAdvanceSearchButton().click();
		  homePage.getVariabledropdown().click();
		  homePage.getSelectMobileNumber().click();
		  homePage.getOperatordropdown().click();
		  homePage.getOperatorequalsTo().click();
		  Thread.sleep(2000);
		  homePage.getValueinputfield().sendKeys(prop.getProperty("ShipmentMobileNo"));
		  homePage.getAddrow().click();
		  
		  //add2nd row
		  homePage.getVariabledropdown().click();
		  shipmntbasicsrch.getSelectOrderNumber().click();
		  homePage.getOperatordropdown().click();
		  homePage.getOperatorequalsTo().click();
		  Thread.sleep(2000);
		  homePage.getValueinputfield().sendKeys(prop.getProperty("OrderNo2"));
		  homePage.getAddrow().click();
		  homePage.getDeleteRow().click();
		  Thread.sleep(3000);
		  testutil.captureScreen(driver, "OrderNo2delete Row");
		  homePage.getApplyfilter().click();
		  Thread.sleep(7000);
		  driver.findElement(By.xpath("//td[text()='"+prop.getProperty("OrderNo2")+"']")).click();
		  Thread.sleep(7000);	  
		
	}
	@Test(priority=4) 
	  public void shipmentAdvsrchwithCaseSensitiveNegtvtest() throws InterruptedException, IOException {
		
		wbLib.waitForPageTobeLoad();
		  Thread.sleep(5000);
		  CashierMakerConsumerHomePage homePage=PageFactory.initElements(TestBase.driver, CashierMakerConsumerHomePage.class);
		  ShipmentsBasicSearchPage shipmntbasicsrch=PageFactory.initElements(TestBase.driver, ShipmentsBasicSearchPage.class);
		  shipmntbasicsrch.getClickondropdownToNavigateShipment().click();
		  shipmntbasicsrch.getClickonshipmentmodule().click();
		  Thread.sleep(5000);
		  homePage.getAdvanceSearchButton().click();
		  Thread.sleep(3000);
		  homePage.getCaseSensitivechkbox().click();
		  homePage.getVariabledropdown().click();
		  shipmntbasicsrch.getSelectCustometName().click();
		  homePage.getOperatordropdown().click();
		  homePage.getOperatorequalsTo().click();
		  Thread.sleep(2000);
		  homePage.getValueinputfield().sendKeys(prop.getProperty("withcasesensitiveconnm"));
		  homePage.getAddrow().click();
		  Thread.sleep(3000);
		  homePage.getApplyfilter().click();
		  LOGGER.info("should not filter");
		  Thread.sleep(3000);
		  testutil.captureScreen(driver, "SAS casesenstvshould not filter");
	}
	@Test(priority=1) 
	  public void shipmntadvsrchwithCaseSensitivePositvetest() throws InterruptedException, IOException {
		
		wbLib.waitForPageTobeLoad();
		  Thread.sleep(5000);
		  CashierMakerConsumerHomePage homePage=PageFactory.initElements(TestBase.driver, CashierMakerConsumerHomePage.class);
		  ShipmentsBasicSearchPage shipmntbasicsrch=PageFactory.initElements(TestBase.driver, ShipmentsBasicSearchPage.class);
		  shipmntbasicsrch.getClickondropdownToNavigateShipment().click();
		  shipmntbasicsrch.getClickonshipmentmodule().click();
		  Thread.sleep(5000);
		  homePage.getAdvanceSearchButton().click();
		  Thread.sleep(3000);
		  homePage.getCaseSensitivechkbox().click();
		  homePage.getVariabledropdown().click();
		  shipmntbasicsrch.getSelectCustometName().click();
		  homePage.getOperatordropdown().click();
		  homePage.getOperatorequalsTo().click();
		  Thread.sleep(2000);
		  homePage.getValueinputfield().sendKeys(prop.getProperty("CustomerNM"));
		  homePage.getAddrow().click();
		  Thread.sleep(3000);
		  homePage.getApplyfilter().click();
		  LOGGER.info("SAS should filter");
		  Thread.sleep(3000);
		  testutil.captureScreen(driver, "SAS casesensitvshouldfilter");
	}
	  
	@AfterMethod
	public void tearDown(){;
		driver.quit();
	}
	

}
