package com.aramex.tests;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.aramex.base.TestBase;
import com.aramex.pages.CashierCheckerPage;
import com.aramex.pages.CashierMakerConsumerHomePage;
import com.aramex.pages.LoginPage;
import com.aramex.pages.ServiceRequestPage;
import com.aramex.pages.ShipmentDetailViewPage;
import com.aramex.pages.ShipmentsBasicSearchPage;
import com.aramex.util.TestUtil;
import com.aramex.util.WebDriverLib;

public class RejectByCashierTest extends TestBase{

	ShipmentDetailViewPage shipmndetailvwpg = new ShipmentDetailViewPage();
	WebDriverLib wbLib=new WebDriverLib();
	LoginTest logintest = new LoginTest();
	TestUtil testutil =  new TestUtil();
	
	@BeforeMethod
	public void logintoAramexAppliction() throws InterruptedException {
		logintest.loginToAppAsCashierchecker(prop.getProperty("lmsusername"), prop.getProperty("lmspassword"));
		
	}
	
	@Test
	public void rejectByCashier() throws InterruptedException, IOException {
	
		Thread.sleep(6000);
		ShipmentDetailViewPage shipdtlvwpg=PageFactory.initElements(TestBase.driver, ShipmentDetailViewPage.class);
		ServiceRequestPage serviceRQpg=PageFactory.initElements(TestBase.driver, ServiceRequestPage.class);
		ShipmentsBasicSearchPage shipmntbasicsrch=PageFactory.initElements(TestBase.driver, ShipmentsBasicSearchPage.class);
		LoginPage devHomePage=PageFactory.initElements(TestBase.driver, LoginPage.class);
		CashierCheckerPage chaschkrpg=PageFactory.initElements(TestBase.driver, CashierCheckerPage.class);
		Actions act =  new Actions(driver);
		CashierMakerConsumerHomePage homePage=PageFactory.initElements(TestBase.driver, CashierMakerConsumerHomePage.class);

		
		homePage.getFilterBy().click();
		shipmntbasicsrch.getSelectShipmentNumber().click();
		homePage.getBasicSearchInputField().click();
		homePage.getBasicSearchInputField().sendKeys(prop.getProperty("rejectBycashier"));
		homePage.getClickonSearchoption().click();
		Thread.sleep(8000);
		
		if(driver.getPageSource().contains(prop.getProperty("rejectBycashier")))
		{
			System.out.println("rejectBycashier is present");
			driver.findElement(By.xpath("//td[text()='"+prop.getProperty("rejectBycashier")+"']")).click();
			testutil.captureScreen(driver, " ' "+prop.getProperty("rejectBycashier")+" ' present Incashierchkr ApprBnkTrnsfBuckt");
			Thread.sleep(3000);
			
			}else
			{
			System.out.println("rejectBycashier is absent");
			}
	
		Thread.sleep(2000);
		
		Thread.sleep(3000);
		act.moveToElement(chaschkrpg.getAppRejPaymntbtn()).click().perform();
		Thread.sleep(3000);
		chaschkrpg.getChkActionDrpdwn().click();
		chaschkrpg.getRejectByChkr().click();
		act.moveToElement(chaschkrpg.getSubmitbtn()).click().perform();
		Thread.sleep(4000);
		wbLib.waitForTextPresent("OK");
		
		String successmsg ="Successfully Saved";
		String actualmessage1 =driver.findElement(By.xpath("//span[text()='OK']/ancestor::div[@class='ant-modal-footer']/preceding-sibling::div[@class='ant-modal-body']")).getText();
		System.out.println(actualmessage1);
		testutil.captureScreen(driver, "rejectByCashierverifymsg "+actualmessage1 );
		Assert.assertEquals(actualmessage1, successmsg);

		Thread.sleep(3000);
		act.moveToElement(shipdtlvwpg.getClkonOK()).click().perform();
		Thread.sleep(6000);
		wbLib.waitForElementToClickble("//div[text()='Rejected']");
		chaschkrpg.getRejectbckt().click();
		Thread.sleep(7000);
  
		homePage.getFilterBy().click();
		shipmntbasicsrch.getSelectShipmentNumber().click();
		homePage.getBasicSearchInputField().click();
		homePage.getBasicSearchInputField().sendKeys(prop.getProperty("rejectBycashier"));
		homePage.getClickonSearchoption().click();
		Thread.sleep(8000);
		
		if(driver.getPageSource().contains(prop.getProperty("rejectBycashier")))
		{
			System.out.println("rejectBycashier is present");
			testutil.captureScreen(driver, " ' "+prop.getProperty("rejectBycashier")+" ' present Incashierchkr RejectedBuckt");
			Thread.sleep(3000);
			
			}else
			{
			System.out.println("rejectBycashier is absent");
			}
	
		Thread.sleep(2000);

		
	}
	@AfterMethod
	public void tearDown(){;
		driver.quit();
	}


		
	
	
	
}
