package com.aramex.tests;

import java.io.IOException;
import java.util.logging.Logger;

import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.aramex.base.TestBase;
import com.aramex.pages.AdvanceSearchOperatorPage;
import com.aramex.pages.CashierMakerConsumerHomePage;
import com.aramex.pages.LoginPage;
import com.aramex.pages.ServiceRequestPage;
import com.aramex.pages.ShipmentsBasicSearchPage;
import com.aramex.util.TestUtil;
import com.aramex.util.WebDriverLib;

public class ServiceReqAdvSrchDiffOperatorTest extends TestBase{

	WebDriverLib wbLib=new WebDriverLib();
	Logger LOGGER = Logger.getLogger(ShipmentAdvSearchDiffOperatorTest.class.getName());
	LoginTest logintest = new LoginTest();
	LoginPage devHomePage=PageFactory.initElements(TestBase.driver, LoginPage.class);
	TestUtil testutil =  new TestUtil();

	
	@BeforeMethod
	public void logintoAramexAppliction() throws InterruptedException {
		logintest.loginToAppAsTester(prop.getProperty("lmsusername"), prop.getProperty("lmspassword"));
		
	}
	
	@Test(priority=1) 
	  public void ServiceRqnotequalstoOperator() throws InterruptedException, IOException {
		wbLib.waitForPageTobeLoad();
		  Thread.sleep(5000);
		  AdvanceSearchOperatorPage operatorpg=PageFactory.initElements(TestBase.driver, AdvanceSearchOperatorPage.class);
		  ShipmentsBasicSearchPage shipmntbasicsrch=PageFactory.initElements(TestBase.driver, ShipmentsBasicSearchPage.class);
		  CashierMakerConsumerHomePage homePage=PageFactory.initElements(TestBase.driver, CashierMakerConsumerHomePage.class);
		  ServiceRequestPage serviceRQpg=PageFactory.initElements(TestBase.driver, ServiceRequestPage.class);
		  
		  shipmntbasicsrch.getClickondropdownToNavigateShipment().click();
		  serviceRQpg.getclickonServiceRQ().click();
		  serviceRQpg.getClickMYRequestbucket().click();
		  Thread.sleep(3000);
		  homePage.getAdvanceSearchButton().click();
		  Thread.sleep(2000);
		  homePage.getVariabledropdown().click();
		  shipmntbasicsrch.getSelectShipmentNumber().click();
		  homePage.getOperatordropdown().click();
		  operatorpg.getOperatornotEqualsTo().click();
		  Thread.sleep(2000);
		  homePage.getValueinputfield().sendKeys(prop.getProperty("serviceRQShipmntNo"));
		  homePage.getAddrow().click();
		  homePage.getApplyfilter().click();
		  Thread.sleep(5000);
		  testutil.captureScreen(driver, "serviceRQShipmntNoNotequalsToOperator outcome");
		  
	}
	@Test(priority=2) 
	  public void ServiceRqgreaterThanEqualsToOperator() throws InterruptedException, IOException {
		wbLib.waitForPageTobeLoad();
		  Thread.sleep(5000);
		  AdvanceSearchOperatorPage operatorpg=PageFactory.initElements(TestBase.driver, AdvanceSearchOperatorPage.class);
		  ShipmentsBasicSearchPage shipmntbasicsrch=PageFactory.initElements(TestBase.driver, ShipmentsBasicSearchPage.class);
		  CashierMakerConsumerHomePage homePage=PageFactory.initElements(TestBase.driver, CashierMakerConsumerHomePage.class);
		  ServiceRequestPage serviceRQpg=PageFactory.initElements(TestBase.driver, ServiceRequestPage.class);
		  
		  shipmntbasicsrch.getClickondropdownToNavigateShipment().click();
		  serviceRQpg.getclickonServiceRQ().click();
		  serviceRQpg.getClickMYRequestbucket().click();
		  Thread.sleep(3000);
		  homePage.getAdvanceSearchButton().click();
		  Thread.sleep(2000);
		  homePage.getVariabledropdown().click();
		  serviceRQpg.getSelectAmntToPaid().click();
		  homePage.getOperatordropdown().click();
		  operatorpg.getOperatorgreaterthanEqualsTo().click();
		  Thread.sleep(2000);
		  homePage.getValueinputfield().sendKeys(prop.getProperty("ServcReqAmntToPaid2"));
		  homePage.getAddrow().click();
		  homePage.getApplyfilter().click();
		  Thread.sleep(5000);
		  testutil.captureScreen(driver, "ServiceRqgreaterThanEqualsToOperator outcome");
		  
	}
	
	@Test(priority=3) 
	  public void ServiceRqlessThanEqualsToOperator() throws InterruptedException, IOException {
		wbLib.waitForPageTobeLoad();
		  Thread.sleep(2000);
		  AdvanceSearchOperatorPage operatorpg=PageFactory.initElements(TestBase.driver, AdvanceSearchOperatorPage.class);
		  ShipmentsBasicSearchPage shipmntbasicsrch=PageFactory.initElements(TestBase.driver, ShipmentsBasicSearchPage.class);
		  CashierMakerConsumerHomePage homePage=PageFactory.initElements(TestBase.driver, CashierMakerConsumerHomePage.class);
		  ServiceRequestPage serviceRQpg=PageFactory.initElements(TestBase.driver, ServiceRequestPage.class);
		  
		  shipmntbasicsrch.getClickondropdownToNavigateShipment().click();
		  serviceRQpg.getclickonServiceRQ().click();
		  serviceRQpg.getClickMYRequestbucket().click();
		  Thread.sleep(3000);
		  homePage.getAdvanceSearchButton().click();
		  Thread.sleep(2000);
		  homePage.getVariabledropdown().click();
		  serviceRQpg.getSelectAmntToPaid().click();
		  homePage.getOperatordropdown().click();
		  operatorpg.getOperatornotlessthanEqualsTo().click();
		  Thread.sleep(2000);
		  homePage.getValueinputfield().sendKeys(prop.getProperty("ServcReqAmntToPaid"));
		  homePage.getAddrow().click();
		  homePage.getApplyfilter().click();
		  Thread.sleep(5000);
		  testutil.captureScreen(driver, "ServiceRqlessThanEqualsToOperator outcome");
		  
	}
	
	@Test(priority=4) 
	  public void ServiceRqlessThanOperator() throws InterruptedException, IOException {
		wbLib.waitForPageTobeLoad();
		  Thread.sleep(2000);
		  AdvanceSearchOperatorPage operatorpg=PageFactory.initElements(TestBase.driver, AdvanceSearchOperatorPage.class);
		  ShipmentsBasicSearchPage shipmntbasicsrch=PageFactory.initElements(TestBase.driver, ShipmentsBasicSearchPage.class);
		  CashierMakerConsumerHomePage homePage=PageFactory.initElements(TestBase.driver, CashierMakerConsumerHomePage.class);
		  ServiceRequestPage serviceRQpg=PageFactory.initElements(TestBase.driver, ServiceRequestPage.class);
		  
		  shipmntbasicsrch.getClickondropdownToNavigateShipment().click();
		  serviceRQpg.getclickonServiceRQ().click();
		  serviceRQpg.getClickMYRequestbucket().click();
		  Thread.sleep(3000);
		  homePage.getAdvanceSearchButton().click();
		  Thread.sleep(2000);
		  homePage.getVariabledropdown().click();
		  serviceRQpg.getSelectAmntToPaid().click();
		  homePage.getOperatordropdown().click();
		  operatorpg.getOperatorlessthan().click();
		  Thread.sleep(2000);
		  homePage.getValueinputfield().sendKeys(prop.getProperty("ServcReqAmntToPaid"));
		  homePage.getAddrow().click();
		  homePage.getApplyfilter().click();
		  Thread.sleep(5000);
		  testutil.captureScreen(driver, "ServiceRqlessThanOperator outcome");
		  
	}
	
	@Test(priority=5) 
	  public void ServiceRqgreaterThanOperator() throws InterruptedException, IOException {
		wbLib.waitForPageTobeLoad();
		  Thread.sleep(2000);
		  AdvanceSearchOperatorPage operatorpg=PageFactory.initElements(TestBase.driver, AdvanceSearchOperatorPage.class);
		  ShipmentsBasicSearchPage shipmntbasicsrch=PageFactory.initElements(TestBase.driver, ShipmentsBasicSearchPage.class);
		  CashierMakerConsumerHomePage homePage=PageFactory.initElements(TestBase.driver, CashierMakerConsumerHomePage.class);
		  ServiceRequestPage serviceRQpg=PageFactory.initElements(TestBase.driver, ServiceRequestPage.class);
		  
		  shipmntbasicsrch.getClickondropdownToNavigateShipment().click();
		  serviceRQpg.getclickonServiceRQ().click();
		  serviceRQpg.getClickMYRequestbucket().click();
		  Thread.sleep(3000);
		  homePage.getAdvanceSearchButton().click();
		  Thread.sleep(2000);
		  homePage.getVariabledropdown().click();
		  serviceRQpg.getSelectAmntToPaid().click();
		  homePage.getOperatordropdown().click();
		  operatorpg.getOperatorgreaterthan().click();
		  Thread.sleep(2000);
		  homePage.getValueinputfield().sendKeys(prop.getProperty("ServcReqAmntToPaid2"));
		  homePage.getAddrow().click();
		  homePage.getApplyfilter().click();
		  Thread.sleep(5000);
		  testutil.captureScreen(driver, "ServiceRqgreaterToOperator outcome");
		  
	}
	
}
