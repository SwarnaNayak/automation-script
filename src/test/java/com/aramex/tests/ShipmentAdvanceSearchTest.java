package com.aramex.tests;

import java.io.IOException;
import java.util.logging.Logger;

import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.aramex.base.TestBase;
import com.aramex.pages.CashierMakerConsumerHomePage;
import com.aramex.pages.LoginPage;
import com.aramex.pages.ShipmentsBasicSearchPage;
import com.aramex.util.TestUtil;
import com.aramex.util.WebDriverLib;

public class ShipmentAdvanceSearchTest extends TestBase{

	WebDriverLib wbLib=new WebDriverLib();
	Logger LOGGER = Logger.getLogger(ShipmentAdvanceSearchTest.class.getName());
	LoginTest logintest = new LoginTest();
	LoginPage devHomePage=PageFactory.initElements(TestBase.driver, LoginPage.class);
	CashierMakerConsumerHomePage homePage=PageFactory.initElements(TestBase.driver, CashierMakerConsumerHomePage.class);
	TestUtil testutil =  new TestUtil();
	
	@BeforeMethod
	public void logintoAramexAppliction() throws InterruptedException {
		logintest.loginToAppAsTester(prop.getProperty("lmsusername"), prop.getProperty("lmspassword"));
		
	}
	
	@Test(priority=2) 
	  public void shipmentadvanceSearchMobileNumber() throws InterruptedException, IOException {
		
		wbLib.waitForPageTobeLoad();
		  Thread.sleep(5000);
		  CashierMakerConsumerHomePage homePage=PageFactory.initElements(TestBase.driver, CashierMakerConsumerHomePage.class);
		  ShipmentsBasicSearchPage shipmntbasicsrch=PageFactory.initElements(TestBase.driver, ShipmentsBasicSearchPage.class);
		  shipmntbasicsrch.getClickondropdownToNavigateShipment().click();
		  shipmntbasicsrch.getClickonshipmentmodule().click();
		  Thread.sleep(5000);
		  homePage.getAdvanceSearchButton().click();
		  homePage.getVariabledropdown().click();
		  homePage.getSelectMobileNumber().click();
		  homePage.getOperatordropdown().click();
		  homePage.getOperatorequalsTo().click();
		  Thread.sleep(2000);
		  homePage.getValueinputfield().click();
		  homePage.getValueinputfield().sendKeys(prop.getProperty("ShipmentMobileNo"));
		  homePage.getAddrow().click();
		  homePage.getApplyfilter().click();
		  Thread.sleep(8000);
		  
		  boolean ShipmentMobileNo = driver.getPageSource().contains(prop.getProperty("ShipmentMobileNo"));
		  if(ShipmentMobileNo==true)
		  {
			  Assert.assertTrue(true);
			  LOGGER.info("ShipmentMobileNo advancedsearch test case passed...");
			  testutil.captureScreen(driver, "ShipmentMobileNo advancedsearch");
			  driver.findElement(By.xpath("//td[text()='"+prop.getProperty("ShipmentMobileNo")+"']")).click();
			  Thread.sleep(9000);
			  testutil.captureScreen(driver, " ' "+prop.getProperty("ShipmentMobileNo")+" ' ShipmentMobileNoadvancedsearch detailview");
		  }
		  else {
			  LOGGER.info("ShipmentMobileNoile advancedsearchs test case failed...");
			  testutil.captureScreen(driver, "ShipmentMobileNo advancedsearch failed");
			  Assert.assertTrue(false);
		  }
		
	}
	
	@Test(priority=3) 
	  public void shipmentadvanceSearchCustomerNM() throws InterruptedException, IOException {
		
		wbLib.waitForPageTobeLoad();
		  Thread.sleep(5000);
		  CashierMakerConsumerHomePage homePage=PageFactory.initElements(TestBase.driver, CashierMakerConsumerHomePage.class);
		  ShipmentsBasicSearchPage shipmntbasicsrch=PageFactory.initElements(TestBase.driver, ShipmentsBasicSearchPage.class);
		  shipmntbasicsrch.getClickondropdownToNavigateShipment().click();
		  shipmntbasicsrch.getClickonshipmentmodule().click();
		  Thread.sleep(5000);
		  homePage.getAdvanceSearchButton().click();
		  homePage.getVariabledropdown().click();
		  shipmntbasicsrch.getSelectCustometName().click();
		  homePage.getOperatordropdown().click();
		  homePage.getOperatorequalsTo().click();
		  Thread.sleep(2000);
		  homePage.getValueinputfield().click();
		  homePage.getValueinputfield().sendKeys(prop.getProperty("CustomerNM"));
		  homePage.getAddrow().click();
		  homePage.getApplyfilter().click();
		  Thread.sleep(8000);
		  
		  boolean CustomerNM = driver.getPageSource().contains(prop.getProperty("CustomerNM"));
		  if(CustomerNM==true)
		  {
			  Assert.assertTrue(true);
			  LOGGER.info("CustomerNM advancedsearch test case passed...");
			  testutil.captureScreen(driver, "CustomerNM advancedsearch");
			  driver.findElement(By.xpath("//td[text()='"+prop.getProperty("CustomerNM")+"']")).click();
			  Thread.sleep(9000);
			  testutil.captureScreen(driver, " ' "+prop.getProperty("CustomerNM")+" ' ShipmentCustomerNMadvancedsearch detailview");
		  }
		  else {
			  LOGGER.info("CustomerNM advancedsearchs test case failed...");
			  testutil.captureScreen(driver, "CustomerNM advancedsearch failed");
			  Assert.assertTrue(false);
		  }
		
	}

	@Test(priority=4) 
	  public void shipmentadvanceSearchShipmentNo() throws InterruptedException, IOException {
		
		wbLib.waitForPageTobeLoad();
		  Thread.sleep(5000);
		  CashierMakerConsumerHomePage homePage=PageFactory.initElements(TestBase.driver, CashierMakerConsumerHomePage.class);
		  ShipmentsBasicSearchPage shipmntbasicsrch=PageFactory.initElements(TestBase.driver, ShipmentsBasicSearchPage.class);
		  shipmntbasicsrch.getClickondropdownToNavigateShipment().click();
		  shipmntbasicsrch.getClickonshipmentmodule().click();
		  Thread.sleep(5000);
		  homePage.getAdvanceSearchButton().click();
		  homePage.getVariabledropdown().click();
		  shipmntbasicsrch.getSelectShipmentNumber().click();
		  homePage.getOperatordropdown().click();
		  homePage.getOperatorequalsTo().click();
		  Thread.sleep(2000);
		  homePage.getValueinputfield().click();
		  homePage.getValueinputfield().sendKeys(prop.getProperty("ShipmentNo"));
		  homePage.getAddrow().click();
		  homePage.getApplyfilter().click();
		  Thread.sleep(8000);
		  
		  boolean ShipmentNo = driver.getPageSource().contains(prop.getProperty("ShipmentNo"));
		  if(ShipmentNo==true)
		  {
			  Assert.assertTrue(true);
			  LOGGER.info("ShipmentNo advancedsearch test case passed...");
			  testutil.captureScreen(driver, "ShipmentNo advancedsearch");
			  driver.findElement(By.xpath("//td[text()='"+prop.getProperty("ShipmentNo")+"']")).click();
			  Thread.sleep(9000);
			  testutil.captureScreen(driver, " ' "+prop.getProperty("ShipmentNo")+" ' ShipmentNoadvancedsearch detailview");
		  }
		  else {
			  LOGGER.info("ShipmentNo advancedsearchs test case failed...");
			  testutil.captureScreen(driver, "ShipmentNo advancedsearch failed");
			  Assert.assertTrue(false);
		  }
		
	}
	
	@Test(priority=5) 
	  public void shipmentadvanceSearchOrderNo() throws InterruptedException, IOException {
		
		wbLib.waitForPageTobeLoad();
		  Thread.sleep(5000);
		  CashierMakerConsumerHomePage homePage=PageFactory.initElements(TestBase.driver, CashierMakerConsumerHomePage.class);
		  ShipmentsBasicSearchPage shipmntbasicsrch=PageFactory.initElements(TestBase.driver, ShipmentsBasicSearchPage.class);
		  shipmntbasicsrch.getClickondropdownToNavigateShipment().click();
		  shipmntbasicsrch.getClickonshipmentmodule().click();
		  Thread.sleep(5000);
		  homePage.getAdvanceSearchButton().click();
		  homePage.getVariabledropdown().click();
		  shipmntbasicsrch.getSelectOrderNumber().click();
		  homePage.getOperatordropdown().click();
		  homePage.getOperatorequalsTo().click();
		  Thread.sleep(2000);
		  homePage.getValueinputfield().click();
		  homePage.getValueinputfield().sendKeys(prop.getProperty("OrderNo"));
		  homePage.getAddrow().click();
		  homePage.getApplyfilter().click();
		  Thread.sleep(8000);
		  
		  boolean OrderNo = driver.getPageSource().contains(prop.getProperty("OrderNo"));
		  if(OrderNo==true)
		  {
			  Assert.assertTrue(true);
			  LOGGER.info("OrderNo advancedsearch test case passed...");
			  testutil.captureScreen(driver, "OrderNo advancedsearch");
			  driver.findElement(By.xpath("//td[text()='"+prop.getProperty("OrderNo")+"']")).click();
			  Thread.sleep(9000);
			  testutil.captureScreen(driver, " ' "+prop.getProperty("OrderNo")+" ' ShipmentOrderNoadvancedsearch detailview");
		  }
		  else {
			  LOGGER.info("OrderNo advancedsearchs test case failed...");
			  testutil.captureScreen(driver, "OrderNo advancedsearch failed");
			  Assert.assertTrue(false);
		  }
	}
	
	@Test(priority=6) 
	  public void shipmentadvanceSearchEtailer() throws InterruptedException, IOException {
		
		wbLib.waitForPageTobeLoad();
		  Thread.sleep(5000);
		  CashierMakerConsumerHomePage homePage=PageFactory.initElements(TestBase.driver, CashierMakerConsumerHomePage.class);
		  ShipmentsBasicSearchPage shipmntbasicsrch=PageFactory.initElements(TestBase.driver, ShipmentsBasicSearchPage.class);
		  shipmntbasicsrch.getClickondropdownToNavigateShipment().click();
		  shipmntbasicsrch.getClickonshipmentmodule().click();
		  Thread.sleep(5000);
		  homePage.getAdvanceSearchButton().click();
		  homePage.getVariabledropdown().click();
		  shipmntbasicsrch.getSelectEtailer().click();
		  homePage.getOperatordropdown().click();
		  homePage.getOperatorequalsTo().click();
		  Thread.sleep(2000);
		  homePage.getValueinputfield().click();
		  homePage.getValueinputfield().sendKeys(prop.getProperty("Etailer"));
		  homePage.getAddrow().click();
		  homePage.getApplyfilter().click();
		  Thread.sleep(8000);
		  
		  boolean Etailer = driver.getPageSource().contains(prop.getProperty("Etailer"));
		  if(Etailer==true)
		  {
			  Assert.assertTrue(true);
			  LOGGER.info("Etailer advancedsearch test case passed...");
			  testutil.captureScreen(driver, "Etailer advancedsearch");
			  driver.findElement(By.xpath("//td[text()='"+prop.getProperty("Etailer")+"']")).click();
			  Thread.sleep(9000);
			  testutil.captureScreen(driver, " ' "+prop.getProperty("Etailer")+" ' ShipmentEtaileradvancedsearch detailview");
		  }
		  else {
			  LOGGER.info("Etailer advancedsearchs test case failed...");
			  testutil.captureScreen(driver, "Etailer advancedsearch failed");
			  Assert.assertTrue(false);
		  }
	}
	
	@Test(priority=7) 
	  public void shipmentadvanceSearchOrigin() throws InterruptedException, IOException {
		
		wbLib.waitForPageTobeLoad();
		  Thread.sleep(5000);
		  CashierMakerConsumerHomePage homePage=PageFactory.initElements(TestBase.driver, CashierMakerConsumerHomePage.class);
		  ShipmentsBasicSearchPage shipmntbasicsrch=PageFactory.initElements(TestBase.driver, ShipmentsBasicSearchPage.class);
		  shipmntbasicsrch.getClickondropdownToNavigateShipment().click();
		  shipmntbasicsrch.getClickonshipmentmodule().click();
		  Thread.sleep(5000);
		  homePage.getAdvanceSearchButton().click();
		  homePage.getVariabledropdown().click();
		  shipmntbasicsrch.getSelectorigin().click();
		  homePage.getOperatordropdown().click();
		  homePage.getOperatorequalsTo().click();
		  Thread.sleep(2000);
		  homePage.getValueinputfield().click();
		  homePage.getValueinputfield().sendKeys(prop.getProperty("Origin"));
		  homePage.getAddrow().click();
		  homePage.getApplyfilter().click();
		  Thread.sleep(8000);
		  
		  boolean Origin = driver.getPageSource().contains(prop.getProperty("Origin"));
		  if(Origin==true)
		  {
			  Assert.assertTrue(true);
			  LOGGER.info("Origin advancedsearch test case passed...");
			  testutil.captureScreen(driver, "Origin advancedsearch");
			  driver.findElement(By.xpath("//td[text()='"+prop.getProperty("Origin")+"']")).click();
			  Thread.sleep(9000);
			  testutil.captureScreen(driver, " ' "+prop.getProperty("Origin")+" ' ShipmentOriginadvancedsearch detailview");
		  }
		  else {
			  LOGGER.info("Origin advancedsearchs test case failed...");
			  testutil.captureScreen(driver, "Origin advancedsearch failed");
			  Assert.assertTrue(false);
		  }
		
	}
	
	@Test(priority=8) 
	  public void shipmentadvanceSearchDestination() throws InterruptedException, IOException {
		
		wbLib.waitForPageTobeLoad();
		  Thread.sleep(5000);
		  CashierMakerConsumerHomePage homePage=PageFactory.initElements(TestBase.driver, CashierMakerConsumerHomePage.class);
		  ShipmentsBasicSearchPage shipmntbasicsrch=PageFactory.initElements(TestBase.driver, ShipmentsBasicSearchPage.class);
		  shipmntbasicsrch.getClickondropdownToNavigateShipment().click();
		  shipmntbasicsrch.getClickonshipmentmodule().click();
		  Thread.sleep(5000);
		  homePage.getAdvanceSearchButton().click();
		  homePage.getVariabledropdown().click();
		  shipmntbasicsrch.getSelectDestination().click();
		  homePage.getOperatordropdown().click();
		  homePage.getOperatorequalsTo().click();
		  Thread.sleep(2000);
		  homePage.getValueinputfield().click();
		  homePage.getValueinputfield().sendKeys(prop.getProperty("Destination"));
		  homePage.getAddrow().click();
		  homePage.getApplyfilter().click();
		  Thread.sleep(8000);
		  
		  boolean Destination = driver.getPageSource().contains(prop.getProperty("Destination"));
		  if(Destination==true)
		  {
			  Assert.assertTrue(true);
			  LOGGER.info("Destination advancedsearch test case passed...");
			  testutil.captureScreen(driver, "Destination advancedsearch");
			  driver.findElement(By.xpath("//td[text()='"+prop.getProperty("Destination")+"']")).click();
			  Thread.sleep(9000);
			  testutil.captureScreen(driver, " ' "+prop.getProperty("Destination")+" ' ShipmentDestinationadvancedsearch detailview");
		  }
		  else {
			  LOGGER.info("Destination advancedsearchs test case failed...");
			  testutil.captureScreen(driver, "Destination advancedsearch failed");
			  Assert.assertTrue(false);
		  }
		
	}
	

	@Test(priority=9) 
	  public void shipmentadvanceSearchCurrencyCode() throws InterruptedException, IOException {
		
		wbLib.waitForPageTobeLoad();
		  Thread.sleep(5000);
		  CashierMakerConsumerHomePage homePage=PageFactory.initElements(TestBase.driver, CashierMakerConsumerHomePage.class);
		  ShipmentsBasicSearchPage shipmntbasicsrch=PageFactory.initElements(TestBase.driver, ShipmentsBasicSearchPage.class);
		  shipmntbasicsrch.getClickondropdownToNavigateShipment().click();
		  shipmntbasicsrch.getClickonshipmentmodule().click();
		  Thread.sleep(5000);
		  homePage.getAdvanceSearchButton().click();
		  homePage.getVariabledropdown().click();
		  shipmntbasicsrch.getSelectCurrencyCode().click();
		  homePage.getOperatordropdown().click();
		  homePage.getOperatorequalsTo().click();
		  Thread.sleep(2000);
		  homePage.getValueinputfield().click();
		  homePage.getValueinputfield().sendKeys(prop.getProperty("CurrencyCode"));
		  homePage.getAddrow().click();
		  homePage.getApplyfilter().click();
		  Thread.sleep(8000);
		  
		  boolean CurrencyCode = driver.getPageSource().contains(prop.getProperty("CurrencyCode"));
		  if(CurrencyCode==true)
		  {
			  Assert.assertTrue(true);
			  LOGGER.info("CurrencyCode advancedsearch test case passed...");
			  testutil.captureScreen(driver, "CurrencyCode advancedsearch");
			  driver.findElement(By.xpath("//td[text()='"+prop.getProperty("CurrencyCode")+"']")).click();
			  Thread.sleep(9000);
			  testutil.captureScreen(driver, " ' "+prop.getProperty("CurrencyCode")+" ' ShipmentCurrencyCodeadvancedsearch detailview");
		  }
		  else {
			  LOGGER.info("CurrencyCode advancedsearchs test case failed...");
			  testutil.captureScreen(driver, "CurrencyCode advancedsearch failed");
			  Assert.assertTrue(false);
		  }
		
	}
	

	@Test(priority=10) 
	  public void shipmentadvanceSearchStatus() throws InterruptedException, IOException {
		
		wbLib.waitForPageTobeLoad();
		  Thread.sleep(5000);
		  CashierMakerConsumerHomePage homePage=PageFactory.initElements(TestBase.driver, CashierMakerConsumerHomePage.class);
		  ShipmentsBasicSearchPage shipmntbasicsrch=PageFactory.initElements(TestBase.driver, ShipmentsBasicSearchPage.class);
		  shipmntbasicsrch.getClickondropdownToNavigateShipment().click();
		  shipmntbasicsrch.getClickonshipmentmodule().click();
		  Thread.sleep(5000);
		  homePage.getAdvanceSearchButton().click();
		  homePage.getVariabledropdown().click();
		  shipmntbasicsrch.getSelectstatus().click();
		  homePage.getOperatordropdown().click();
		  homePage.getOperatorequalsTo().click();
		  Thread.sleep(2000);
		  homePage.getValueinputfield().click();
		  homePage.getValueinputfield().sendKeys(prop.getProperty("Status"));
		  homePage.getAddrow().click();
		  homePage.getApplyfilter().click();
		  Thread.sleep(8000);
		  
		  boolean Status = driver.getPageSource().contains(prop.getProperty("Status"));
		  if(Status==true)
		  {
			  Assert.assertTrue(true);
			  LOGGER.info("Status advancedsearch test case passed...");
			  testutil.captureScreen(driver, "Status advancedsearch");
			  driver.findElement(By.xpath("//td[text()='"+prop.getProperty("Status")+"']")).click();
			  Thread.sleep(9000);
			  testutil.captureScreen(driver, " ' "+prop.getProperty("Status")+" ' ShipmentStatusadvancedsearch detailview");
		  }
		  else {
			  LOGGER.info("Status advancedsearchs test case failed...");
			  testutil.captureScreen(driver, "Status advancedsearch failed");
			  Assert.assertTrue(false);
		  }
		
	}
	

	@Test(priority=1) 
	  public void shipmentadvanceSearchLoanStatus() throws InterruptedException, IOException {
		
		wbLib.waitForPageTobeLoad();
		  Thread.sleep(5000);
		  CashierMakerConsumerHomePage homePage=PageFactory.initElements(TestBase.driver, CashierMakerConsumerHomePage.class);
		  ShipmentsBasicSearchPage shipmntbasicsrch=PageFactory.initElements(TestBase.driver, ShipmentsBasicSearchPage.class);
		  shipmntbasicsrch.getClickondropdownToNavigateShipment().click();
		  shipmntbasicsrch.getClickonshipmentmodule().click();
		  Thread.sleep(5000);
		  homePage.getAdvanceSearchButton().click();
		  homePage.getVariabledropdown().click();
		  shipmntbasicsrch.getSelectLoanStatus().click();
		  homePage.getOperatordropdown().click();
		  homePage.getOperatorequalsTo().click();
		  Thread.sleep(2000);
		  homePage.getValueinputfield().click();
		  homePage.getValueinputfield().sendKeys(prop.getProperty("LoanStatus"));
		  homePage.getAddrow().click();
		  homePage.getApplyfilter().click();
		  Thread.sleep(8000);
		  
		  boolean LoanStatus = driver.getPageSource().contains(prop.getProperty("LoanStatus"));
		  if(LoanStatus==true)
		  {
			  Assert.assertTrue(true);
			  LOGGER.info("LoanStatus advancedsearch test case passed...");
			  testutil.captureScreen(driver, "LoanStatus advancedsearch");
			  driver.findElement(By.xpath("//td[text()='"+prop.getProperty("LoanStatus")+"']")).click();
			  Thread.sleep(9000);
			  testutil.captureScreen(driver, " ' "+prop.getProperty("LoanStatus")+" ' ShipmentLoanStatusadvancedsearch detailview");
		  }
		  else {
			  LOGGER.info("LoanStatus advancedsearchs test case failed...");
			  testutil.captureScreen(driver, "LoanStatus advancedsearch failed");
			  Assert.assertTrue(false);
		  }
		
	}
	
	@AfterMethod
	public void tearDown(){;
		driver.quit();
	}
}
