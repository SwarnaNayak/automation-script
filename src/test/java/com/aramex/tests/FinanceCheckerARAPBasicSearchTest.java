package com.aramex.tests;

import java.io.IOException;
import java.util.logging.Logger;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import com.aramex.base.TestBase;
import com.aramex.pages.AdvanceSearchOperatorPage;
import com.aramex.pages.CashierMakerConsumerHomePage;
import com.aramex.pages.FinanceCheckerPage;
import com.aramex.pages.LoginPage;
import com.aramex.util.TestUtil;
import com.aramex.util.WebDriverLib;

public class FinanceCheckerARAPBasicSearchTest extends TestBase{

	WebDriverLib wbLib=new WebDriverLib();
	Logger LOGGER = Logger.getLogger(ShipmentBasicSearchTest.class.getName());
	LoginTest logintest = new LoginTest();
	LoginPage devHomePage=PageFactory.initElements(TestBase.driver, LoginPage.class);
	AdvanceSearchOperatorPage operatorpg=PageFactory.initElements(TestBase.driver, AdvanceSearchOperatorPage.class);
	  CashierMakerConsumerHomePage homePage=PageFactory.initElements(TestBase.driver, CashierMakerConsumerHomePage.class);
	TestUtil testutil =  new TestUtil();

	
	@BeforeMethod
	public void logintoAramexAppliction() throws InterruptedException {
		logintest.loginToAppAsFinancechecker(prop.getProperty("lmsusername"), prop.getProperty("lmspassword"));
		
	}
	
	@Test(priority=1) 
	  public void financechkrbasicSearchSlNo() throws IOException, InterruptedException {
		wbLib.waitForPageTobeLoad();
		  Thread.sleep(5000);
		  FinanceCheckerPage financechkrpg=PageFactory.initElements(TestBase.driver, FinanceCheckerPage.class);
		  CashierMakerConsumerHomePage homePage=PageFactory.initElements(TestBase.driver, CashierMakerConsumerHomePage.class);
		 // Thread.sleep(5000);
		  homePage.getFilterBy().click();
		  
		  financechkrpg.getSelectSlno().click();
		  homePage.getBasicSearchInputField().click();
		  homePage.getBasicSearchInputField().sendKeys(prop.getProperty("slNumber"));
		  homePage.getClickonSearchoption().click();
		  
		  Thread.sleep(5000);       
		  boolean SLNo = driver.getPageSource().contains(prop.getProperty("slNumber"));
		  if(SLNo==true)
		  {
			  Assert.assertTrue(true);
			  LOGGER.info("SLNo basicsearch test case passed...");
			  testutil.captureScreen(driver, "SLNo searchpassed");
		  }
		  else {
			  LOGGER.info("SLNo basic search test case failed...");
			  testutil.captureScreen(driver, "SLNo searchfailed");
			  Assert.assertTrue(false);
		  }

		  homePage.getClearfilter().click();		  
		  Thread.sleep(9000);

	
	}
	
	@Test(priority=2) 
	  public void financechkrbasicSearchDocumntNo() throws IOException, InterruptedException {
		wbLib.waitForPageTobeLoad();
		  Thread.sleep(5000);
		  FinanceCheckerPage financechkrpg=PageFactory.initElements(TestBase.driver, FinanceCheckerPage.class);
		  CashierMakerConsumerHomePage homePage=PageFactory.initElements(TestBase.driver, CashierMakerConsumerHomePage.class);
		 // Thread.sleep(5000);
		  homePage.getFilterBy().click();
		  
		  financechkrpg.getSelectDocumentno().click();
		  homePage.getBasicSearchInputField().click();
		  homePage.getBasicSearchInputField().sendKeys(prop.getProperty("DocumentNo"));
		  homePage.getClickonSearchoption().click();
		  
		  Thread.sleep(5000);       
		  boolean DocumentNo = driver.getPageSource().contains(prop.getProperty("DocumentNo"));
		  if(DocumentNo==true)
		  {
			  Assert.assertTrue(true);
			  LOGGER.info("DocumentNo basicsearch test case passed...");
			  testutil.captureScreen(driver, "DocumentNo searchpassed");
		  }
		  else {
			  LOGGER.info("DocumentNo basic search test case failed...");
			  testutil.captureScreen(driver, "DocumentNo searchfailed");
			  Assert.assertTrue(false);
		  }

		  homePage.getClearfilter().click();		  
		  Thread.sleep(9000);

	}
	
	/*
	 * @Test(priority=3) public void financechkrbasicSearchShipmntID() throws
	 * IOException, InterruptedException { wbLib.waitForPageTobeLoad();
	 * Thread.sleep(5000); FinanceCheckerPage
	 * financechkrpg=PageFactory.initElements(TestBase.driver,
	 * FinanceCheckerPage.class); CashierMakerConsumerHomePage
	 * homePage=PageFactory.initElements(TestBase.driver,
	 * CashierMakerConsumerHomePage.class); // Thread.sleep(5000);
	 * homePage.getFilterBy().click();
	 * 
	 * financechkrpg.getSelectShipmntID().click();
	 * homePage.getBasicSearchInputField().click();
	 * homePage.getBasicSearchInputField().sendKeys(prop.getProperty("ShipmentID"));
	 * homePage.getClickonSearchoption().click();
	 * 
	 * Thread.sleep(5000); boolean ShipmentID =
	 * driver.getPageSource().contains(prop.getProperty("ShipmentID"));
	 * if(ShipmentID==true) { Assert.assertTrue(true);
	 * LOGGER.info("ShipmentID basicsearch test case passed...");
	 * testutil.captureScreen(driver, "ShipmentID searchpassed"); } else {
	 * LOGGER.info("ShipmentID basic search test case failed...");
	 * testutil.captureScreen(driver, "ShipmentID searchfailed");
	 * Assert.assertTrue(false); }
	 * 
	 * homePage.getClearfilter().click(); Thread.sleep(9000);
	 * 
	 * }
	 * 
	 * @Test(priority=4) public void financechkrbasicSearchEtailerApNo() throws
	 * IOException, InterruptedException { wbLib.waitForPageTobeLoad();
	 * Thread.sleep(5000); FinanceCheckerPage
	 * financechkrpg=PageFactory.initElements(TestBase.driver,
	 * FinanceCheckerPage.class); CashierMakerConsumerHomePage
	 * homePage=PageFactory.initElements(TestBase.driver,
	 * CashierMakerConsumerHomePage.class); // Thread.sleep(5000);
	 * homePage.getFilterBy().click();
	 * 
	 * financechkrpg.getSelectEtailerAPNo().click();
	 * homePage.getBasicSearchInputField().click();
	 * homePage.getBasicSearchInputField().sendKeys(prop.getProperty("EtailerAPNo"))
	 * ; homePage.getClickonSearchoption().click();
	 * 
	 * Thread.sleep(5000); boolean EtailerAPNo =
	 * driver.getPageSource().contains(prop.getProperty("EtailerAPNo"));
	 * if(EtailerAPNo==true) { Assert.assertTrue(true);
	 * LOGGER.info("EtailerAPNo basicsearch test case passed...");
	 * testutil.captureScreen(driver, "EtailerAPNo searchpassed"); } else {
	 * LOGGER.info("EtailerAPNo basic search test case failed...");
	 * testutil.captureScreen(driver, "EtailerAPNo searchfailed");
	 * Assert.assertTrue(false); }
	 * 
	 * homePage.getClearfilter().click(); Thread.sleep(9000);
	 * 
	 * }
	 * 
	 * @Test(priority=5) public void financechkrbasicSearchDocType() throws
	 * IOException, InterruptedException { wbLib.waitForPageTobeLoad();
	 * Thread.sleep(5000); FinanceCheckerPage
	 * financechkrpg=PageFactory.initElements(TestBase.driver,
	 * FinanceCheckerPage.class); CashierMakerConsumerHomePage
	 * homePage=PageFactory.initElements(TestBase.driver,
	 * CashierMakerConsumerHomePage.class); // Thread.sleep(5000);
	 * homePage.getFilterBy().click();
	 * 
	 * financechkrpg.getSelectDocType().click();
	 * homePage.getBasicSearchInputField().click();
	 * homePage.getBasicSearchInputField().sendKeys(prop.getProperty("DocumentType")
	 * ); homePage.getClickonSearchoption().click();
	 * 
	 * Thread.sleep(5000); boolean DocumentType =
	 * driver.getPageSource().contains(prop.getProperty("DocumentType"));
	 * if(DocumentType==true) { Assert.assertTrue(true);
	 * LOGGER.info("DocumentType basicsearch test case passed...");
	 * testutil.captureScreen(driver, "DocumentType searchpassed"); } else {
	 * LOGGER.info("DocumentType basic search test case failed...");
	 * testutil.captureScreen(driver, "DocumentType searchfailed");
	 * Assert.assertTrue(false); }
	 * 
	 * homePage.getClearfilter().click(); Thread.sleep(9000);
	 * 
	 * }
	 * 
	 * @Test(priority=6) public void financechkrbasicSearchtransDt() throws
	 * IOException, InterruptedException { wbLib.waitForPageTobeLoad();
	 * Thread.sleep(5000); FinanceCheckerPage
	 * financechkrpg=PageFactory.initElements(TestBase.driver,
	 * FinanceCheckerPage.class); CashierMakerConsumerHomePage
	 * homePage=PageFactory.initElements(TestBase.driver,
	 * CashierMakerConsumerHomePage.class); // Thread.sleep(5000);
	 * homePage.getFilterBy().click();
	 * 
	 * financechkrpg.getSelectTransDt().click();
	 * homePage.getBasicSearchInputField().click();
	 * homePage.getBasicSearchInputField().sendKeys(prop.getProperty("TranscDt"));
	 * homePage.getClickonSearchoption().click();
	 * 
	 * Thread.sleep(5000); boolean TranscDt =
	 * driver.getPageSource().contains(prop.getProperty("TranscDt"));
	 * if(TranscDt==true) { Assert.assertTrue(true);
	 * LOGGER.info("TranscDt basicsearch test case passed...");
	 * testutil.captureScreen(driver, "TranscDt searchpassed"); } else {
	 * LOGGER.info("TranscDt basic search test case failed...");
	 * testutil.captureScreen(driver, "TranscDt searchfailed");
	 * Assert.assertTrue(false); }
	 * 
	 * homePage.getClearfilter().click(); Thread.sleep(9000);
	 * 
	 * }
	 * 
	 * @Test(priority=7) public void financechkrbasicSearchtransAmnt() throws
	 * IOException, InterruptedException { wbLib.waitForPageTobeLoad();
	 * Thread.sleep(5000); FinanceCheckerPage
	 * financechkrpg=PageFactory.initElements(TestBase.driver,
	 * FinanceCheckerPage.class); CashierMakerConsumerHomePage
	 * homePage=PageFactory.initElements(TestBase.driver,
	 * CashierMakerConsumerHomePage.class); // Thread.sleep(5000);
	 * homePage.getFilterBy().click();
	 * 
	 * financechkrpg.getSelectTransAmnt().click();
	 * homePage.getBasicSearchInputField().click();
	 * homePage.getBasicSearchInputField().sendKeys(prop.getProperty("TransAmnt"));
	 * homePage.getClickonSearchoption().click();
	 * 
	 * Thread.sleep(5000); boolean TransAmnt =
	 * driver.getPageSource().contains(prop.getProperty("TransAmnt"));
	 * if(TransAmnt==true) { Assert.assertTrue(true);
	 * LOGGER.info("TransAmnt basicsearch test case passed...");
	 * testutil.captureScreen(driver, "TransAmnt searchpassed"); } else {
	 * LOGGER.info("TransAmnt basic search test case failed...");
	 * testutil.captureScreen(driver, "TransAmnt searchfailed");
	 * Assert.assertTrue(false); }
	 * 
	 * homePage.getClearfilter().click(); Thread.sleep(9000);
	 * 
	 * }
	 * 
	 * @Test(priority=8) public void financechkrbasicSearchConsuNm() throws
	 * IOException, InterruptedException { wbLib.waitForPageTobeLoad();
	 * Thread.sleep(5000); FinanceCheckerPage
	 * financechkrpg=PageFactory.initElements(TestBase.driver,
	 * FinanceCheckerPage.class); CashierMakerConsumerHomePage
	 * homePage=PageFactory.initElements(TestBase.driver,
	 * CashierMakerConsumerHomePage.class); // Thread.sleep(5000);
	 * homePage.getFilterBy().click();
	 * 
	 * financechkrpg.getSelectConsumerNm().click();
	 * homePage.getBasicSearchInputField().click();
	 * homePage.getBasicSearchInputField().sendKeys(prop.getProperty("consumerNm"));
	 * homePage.getClickonSearchoption().click();
	 * 
	 * Thread.sleep(5000); boolean consumerNm =
	 * driver.getPageSource().contains(prop.getProperty("consumerNm"));
	 * if(consumerNm==true) { Assert.assertTrue(true);
	 * LOGGER.info("consumerNm basicsearch test case passed...");
	 * testutil.captureScreen(driver, "consumerNm searchpassed"); } else {
	 * LOGGER.info("consumerNm basic search test case failed...");
	 * testutil.captureScreen(driver, "consumerNm searchfailed");
	 * Assert.assertTrue(false); }
	 * 
	 * homePage.getClearfilter().click(); Thread.sleep(9000);
	 * 
	 * }
	 * 
	 * @Test(priority=9) public void financechkrbasicSearchConsuMobNo() throws
	 * IOException, InterruptedException { wbLib.waitForPageTobeLoad();
	 * Thread.sleep(5000); FinanceCheckerPage
	 * financechkrpg=PageFactory.initElements(TestBase.driver,
	 * FinanceCheckerPage.class); CashierMakerConsumerHomePage
	 * homePage=PageFactory.initElements(TestBase.driver,
	 * CashierMakerConsumerHomePage.class); //Thread.sleep(5000);
	 * homePage.getFilterBy().click();
	 * 
	 * financechkrpg.getSelectConsMobNo().click();
	 * homePage.getBasicSearchInputField().click();
	 * homePage.getBasicSearchInputField().sendKeys(prop.getProperty("consumerMobNo"
	 * )); homePage.getClickonSearchoption().click();
	 * 
	 * Thread.sleep(5000); boolean consumerMobNo =
	 * driver.getPageSource().contains(prop.getProperty("consumerMobNo"));
	 * if(consumerMobNo==true) { Assert.assertTrue(true);
	 * LOGGER.info("consumerMobNo basicsearch test case passed...");
	 * testutil.captureScreen(driver, "consumerMobNo searchpassed"); } else {
	 * LOGGER.info("consumerMobNo basic search test case failed...");
	 * testutil.captureScreen(driver, "consumerMobNo searchfailed");
	 * Assert.assertTrue(false); }
	 * 
	 * homePage.getClearfilter().click(); Thread.sleep(9000);
	 * 
	 * }
	 */	
	@AfterMethod
	public void tearDown(){;
		driver.quit();
	}
}

