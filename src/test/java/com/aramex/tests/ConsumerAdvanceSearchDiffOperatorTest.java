package com.aramex.tests;

import java.io.IOException;
import java.util.logging.Logger;

import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.aramex.base.TestBase;
import com.aramex.pages.AdvanceSearchOperatorPage;
import com.aramex.pages.CashierMakerConsumerHomePage;
import com.aramex.pages.LoginPage;
import com.aramex.util.TestUtil;
import com.aramex.util.WebDriverLib;

public class ConsumerAdvanceSearchDiffOperatorTest extends TestBase{

	WebDriverLib wbLib=new WebDriverLib();
	Logger LOGGER = Logger.getLogger(ConsumerAdvanceSearchDiffOperatorTest.class.getName());
	LoginTest logintest = new LoginTest();
	LoginPage devHomePage=PageFactory.initElements(TestBase.driver, LoginPage.class);
	AdvanceSearchOperatorPage operatorpg=PageFactory.initElements(TestBase.driver, AdvanceSearchOperatorPage.class);
	  CashierMakerConsumerHomePage homePage=PageFactory.initElements(TestBase.driver, CashierMakerConsumerHomePage.class);
	TestUtil testutil =  new TestUtil();

	
	@BeforeMethod
	public void logintoAramexAppliction() throws InterruptedException {
		logintest.loginToAppAsTester(prop.getProperty("lmsusername"), prop.getProperty("lmspassword"));
		
	}
	
	@Test(priority=5) 
	  public void notequalstoOperator() throws InterruptedException, IOException {
		wbLib.waitForPageTobeLoad();
		  Thread.sleep(5000);
		  CashierMakerConsumerHomePage homePage=PageFactory.initElements(TestBase.driver, CashierMakerConsumerHomePage.class);
		AdvanceSearchOperatorPage operatorpg=PageFactory.initElements(TestBase.driver, AdvanceSearchOperatorPage.class);
		
		  homePage.getAdvanceSearchButton().click();
		  Thread.sleep(3000);
		  homePage.getVariabledropdown().click();
		  homePage.getSelectMobileNumber().click();
		  homePage.getOperatordropdown().click();
		  operatorpg.getOperatornotEqualsTo().click();
		  Thread.sleep(2000);
		  homePage.getValueinputfield().sendKeys(prop.getProperty("SearchbyMobilenumber"));
		  homePage.getAddrow().click();
		  homePage.getApplyfilter().click();
		  Thread.sleep(5000);
		  testutil.captureScreen(driver, "notequalsToOperator outcome");
		  
	}
	
	@Test(priority=3) 
	  public void greaterThanEqualsToOperator() throws InterruptedException, IOException {
		wbLib.waitForPageTobeLoad();
		  Thread.sleep(5000);
		  CashierMakerConsumerHomePage homePage=PageFactory.initElements(TestBase.driver, CashierMakerConsumerHomePage.class);
		  AdvanceSearchOperatorPage operatorpg=PageFactory.initElements(TestBase.driver, AdvanceSearchOperatorPage.class);
		  homePage.getAdvanceSearchButton().click();
		  wbLib.waitForTextPresent("Variable");
		  Thread.sleep(2000);
		  homePage.getVariabledropdown().click();
		  homePage.getSelectAvailableLimit().click();
		  homePage.getOperatordropdown().click();
		  operatorpg.getOperatorgreaterthanEqualsTo().click();
		  Thread.sleep(2000);
		  homePage.getValueinputfield().sendKeys(prop.getProperty("AvailableLimit1"));
		  homePage.getAddrow().click();
		  homePage.getApplyfilter().click();
		  Thread.sleep(5000);
		  testutil.captureScreen(driver, "greaterThanEqualsToOperator outcome");
		  
	}
	
	@Test(priority=4) 
	  public void lessThanEqualsToOperator() throws InterruptedException, IOException {
		wbLib.waitForPageTobeLoad();
		  Thread.sleep(2000);
		  CashierMakerConsumerHomePage homePage=PageFactory.initElements(TestBase.driver, CashierMakerConsumerHomePage.class);
		  AdvanceSearchOperatorPage operatorpg=PageFactory.initElements(TestBase.driver, AdvanceSearchOperatorPage.class);
		  homePage.getAdvanceSearchButton().click();
		  wbLib.waitForTextPresent("Variable");
		  Thread.sleep(2000);
		  homePage.getVariabledropdown().click();
		  homePage.getSelectAvailableLimit().click();
		  homePage.getOperatordropdown().click();
		  operatorpg.getOperatornotlessthanEqualsTo().click();
		  Thread.sleep(2000);
		  homePage.getValueinputfield().sendKeys(prop.getProperty("AvailableLimit1"));
		  homePage.getAddrow().click();
		  homePage.getApplyfilter().click();
		  Thread.sleep(5000);
		  testutil.captureScreen(driver, "lessThanEqualsToOperator outcome");
		  
	}
	
	@Test(priority=7) 
	  public void lessThanOperator() throws InterruptedException, IOException {
		wbLib.waitForPageTobeLoad();
		  Thread.sleep(2000);
		  CashierMakerConsumerHomePage homePage=PageFactory.initElements(TestBase.driver, CashierMakerConsumerHomePage.class);
		  AdvanceSearchOperatorPage operatorpg=PageFactory.initElements(TestBase.driver, AdvanceSearchOperatorPage.class);
		  homePage.getAdvanceSearchButton().click();
		  wbLib.waitForTextPresent("Variable");
		  Thread.sleep(2000);
		  homePage.getVariabledropdown().click();
		  homePage.getSelectCreditScore().click();
		  homePage.getOperatordropdown().click();
		  operatorpg.getOperatorlessthan().click();
		  Thread.sleep(2000);
		  homePage.getValueinputfield().sendKeys(prop.getProperty("CreditScore1"));
		  homePage.getAddrow().click();
		  homePage.getApplyfilter().click();
		  Thread.sleep(5000);
		  testutil.captureScreen(driver, "lessThanOperator outcome");
		  
	}
	
	@Test(priority=6) 
	  public void greaterThanOperator() throws InterruptedException, IOException {
		wbLib.waitForPageTobeLoad();
		  Thread.sleep(2000);
		  CashierMakerConsumerHomePage homePage=PageFactory.initElements(TestBase.driver, CashierMakerConsumerHomePage.class);
		  AdvanceSearchOperatorPage operatorpg=PageFactory.initElements(TestBase.driver, AdvanceSearchOperatorPage.class);
		  homePage.getAdvanceSearchButton().click();
		  wbLib.waitForTextPresent("Variable");
		  Thread.sleep(2000);
		  homePage.getVariabledropdown().click();
		  homePage.getSelectCreditScore().click();
		  homePage.getOperatordropdown().click();
		  operatorpg.getOperatorgreaterthan().click();
		  Thread.sleep(2000);
		  homePage.getValueinputfield().sendKeys(prop.getProperty("CreditScore1"));
		  homePage.getAddrow().click();
		  homePage.getApplyfilter().click();
		  Thread.sleep(5000);
		  testutil.captureScreen(driver, "greaterToOperator outcome");
		  
	}
	
	@Test(priority=1) 
	  public void inOperator() throws InterruptedException, IOException {
		wbLib.waitForPageTobeLoad();
		  Thread.sleep(2000);
		  CashierMakerConsumerHomePage homePage=PageFactory.initElements(TestBase.driver, CashierMakerConsumerHomePage.class);
		  AdvanceSearchOperatorPage operatorpg=PageFactory.initElements(TestBase.driver, AdvanceSearchOperatorPage.class);
		  homePage.getAdvanceSearchButton().click();
		  wbLib.waitForTextPresent("Variable");
		  Thread.sleep(2000);
		  homePage.getVariabledropdown().click();
		  homePage.getSelectLimit().click();
		  homePage.getOperatordropdown().click();
		  operatorpg.getOperatorin().click();
		  Thread.sleep(2000);
		  homePage.getValueinputfield().sendKeys(prop.getProperty("limit1"));
		  operatorpg.getAddsymbol().click();
		  homePage.getValueinputfield().sendKeys(prop.getProperty("limit2"));
		  operatorpg.getAddsymbol().click();
		  homePage.getAddrow().click();
		  homePage.getApplyfilter().click();
		  Thread.sleep(5000);
		  testutil.captureScreen(driver, "inToOperator outcome");
		  
	}
	
	@Test(priority=2) 
	  public void rangeOperator() throws InterruptedException, IOException {
		wbLib.waitForPageTobeLoad();
		  Thread.sleep(2000);
		  CashierMakerConsumerHomePage homePage=PageFactory.initElements(TestBase.driver, CashierMakerConsumerHomePage.class);
		  AdvanceSearchOperatorPage operatorpg=PageFactory.initElements(TestBase.driver, AdvanceSearchOperatorPage.class);
		  homePage.getAdvanceSearchButton().click();
		  wbLib.waitForTextPresent("Variable");
		  Thread.sleep(2000);
		  homePage.getVariabledropdown().click();
		  homePage.getSelectPropensityScore().click();
		  homePage.getOperatordropdown().click();
		  operatorpg.getOperatorrange().click();
		  Thread.sleep(2000);
		  operatorpg.getRangeinput1().sendKeys(prop.getProperty("PropernsityScore1"));
		  operatorpg.getRangeinpuut2().sendKeys(prop.getProperty("PropernsityScore2"));
		  homePage.getAddrow().click();
		  homePage.getApplyfilter().click();
		  wbLib.waitForPageTobeLoad();
		  Thread.sleep(5000);
		  testutil.captureScreen(driver, "rangeOperator outcome");
		  
	}
	
	
	@AfterMethod
	public void tearDown(){;
		driver.quit();
	}
}
