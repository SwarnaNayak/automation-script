package com.aramex.tests;

import java.io.IOException;
import java.util.logging.Logger;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import com.aramex.base.TestBase;
import com.aramex.pages.AdvanceSearchOperatorPage;
import com.aramex.pages.CashierMakerConsumerHomePage;
import com.aramex.pages.FinanceCheckerPage;
import com.aramex.pages.LoginPage;
import com.aramex.util.TestUtil;
import com.aramex.util.WebDriverLib;

public class FinanceCheckerARAPAdvSearchTest extends TestBase{

	WebDriverLib wbLib=new WebDriverLib();
	Logger LOGGER = Logger.getLogger(ShipmentBasicSearchTest.class.getName());
	LoginTest logintest = new LoginTest();
	LoginPage devHomePage=PageFactory.initElements(TestBase.driver, LoginPage.class);
	AdvanceSearchOperatorPage operatorpg=PageFactory.initElements(TestBase.driver, AdvanceSearchOperatorPage.class);
	  CashierMakerConsumerHomePage homePage=PageFactory.initElements(TestBase.driver, CashierMakerConsumerHomePage.class);
	TestUtil testutil =  new TestUtil();

	
	@BeforeMethod
	public void logintoAramexAppliction() throws InterruptedException {
		logintest.loginToAppAsFinancechecker(prop.getProperty("lmsusername"), prop.getProperty("lmspassword"));
		
	}
	
	@Test(priority=1) 
	  public void financechkradvanceSearchDocNo() throws InterruptedException, IOException {
		
		wbLib.waitForPageTobeLoad();
		  Thread.sleep(5000);
		  FinanceCheckerPage financechkrpg=PageFactory.initElements(TestBase.driver, FinanceCheckerPage.class);
		  CashierMakerConsumerHomePage homePage=PageFactory.initElements(TestBase.driver, CashierMakerConsumerHomePage.class);
		  
		 // Thread.sleep(5000);
		  homePage.getAdvanceSearchButton().click();
		  homePage.getVariabledropdown().click();
		  financechkrpg.getSelectDocumentno().click();
		  homePage.getOperatordropdown().click();
		  homePage.getOperatorequalsTo().click();
		  Thread.sleep(2000);
		  homePage.getValueinputfield().click();
		  homePage.getValueinputfield().sendKeys(prop.getProperty("DocumentNo"));
		  homePage.getAddrow().click();
		  homePage.getApplyfilter().click();
		  Thread.sleep(8000);
		  
		  boolean DocumentNo = driver.getPageSource().contains(prop.getProperty("DocumentNo"));
		  if(DocumentNo==true)
		  {
			  Assert.assertTrue(true);
			  LOGGER.info("DocumentNo advancedsearch test case passed...");
			  testutil.captureScreen(driver, "DocumentNo advancedsearch");
			  
		  }
		  else {
			  LOGGER.info("DocumentNo advancedsearchs test case failed...");
			  testutil.captureScreen(driver, "DocumentNo advancedsearch failed");
			  Assert.assertTrue(false);
		  }
		
	}
	
	@Test(priority=2) 
	  public void financechkradvanceSearchDocType() throws InterruptedException, IOException {
		
		wbLib.waitForPageTobeLoad();
		  Thread.sleep(5000);
		  FinanceCheckerPage financechkrpg=PageFactory.initElements(TestBase.driver, FinanceCheckerPage.class);
		  CashierMakerConsumerHomePage homePage=PageFactory.initElements(TestBase.driver, CashierMakerConsumerHomePage.class);
		  
		  //Thread.sleep(5000);
		  homePage.getAdvanceSearchButton().click();
		  homePage.getVariabledropdown().click();
		  financechkrpg.getSelectDocType().click();
		  homePage.getOperatordropdown().click();
		  homePage.getOperatorequalsTo().click();
		  Thread.sleep(2000);
		  homePage.getValueinputfield().click();
		  homePage.getValueinputfield().sendKeys(prop.getProperty("DocumentType"));
		  homePage.getAddrow().click();
		  homePage.getApplyfilter().click();
		  Thread.sleep(8000);
		  
		  boolean DocumentType = driver.getPageSource().contains(prop.getProperty("DocumentType"));
		  if(DocumentType==true)
		  {
			  Assert.assertTrue(true);
			  LOGGER.info("DocumentType advancedsearch test case passed...");
			  testutil.captureScreen(driver, "DocumentType advancedsearch");
			  
		  }
		  else {
			  LOGGER.info("DocumentType advancedsearchs test case failed...");
			  testutil.captureScreen(driver, "DocumentType advancedsearch failed");
			  Assert.assertTrue(false);
		  }
		
	}
	
	/*
	 * @Test(priority=3) public void financechkradvanceSearchTransAmnt() throws
	 * InterruptedException, IOException {
	 * 
	 * wbLib.waitForPageTobeLoad(); Thread.sleep(5000); FinanceCheckerPage
	 * financechkrpg=PageFactory.initElements(TestBase.driver,
	 * FinanceCheckerPage.class); CashierMakerConsumerHomePage
	 * homePage=PageFactory.initElements(TestBase.driver,
	 * CashierMakerConsumerHomePage.class);
	 * 
	 * // Thread.sleep(5000); homePage.getAdvanceSearchButton().click();
	 * homePage.getVariabledropdown().click();
	 * financechkrpg.getSelectTransAmnt().click();
	 * homePage.getOperatordropdown().click();
	 * homePage.getOperatorequalsTo().click(); Thread.sleep(2000);
	 * homePage.getValueinputfield().click();
	 * homePage.getValueinputfield().sendKeys(prop.getProperty("TransAmnt"));
	 * homePage.getAddrow().click(); homePage.getApplyfilter().click();
	 * Thread.sleep(8000);
	 * 
	 * boolean TransAmnt =
	 * driver.getPageSource().contains(prop.getProperty("TransAmnt"));
	 * if(TransAmnt==true) { Assert.assertTrue(true);
	 * LOGGER.info("TransAmnt advancedsearch test case passed...");
	 * testutil.captureScreen(driver, "TransAmnt advancedsearch");
	 * 
	 * } else { LOGGER.info("TransAmnt advancedsearchs test case failed...");
	 * testutil.captureScreen(driver, "TransAmnt advancedsearch failed");
	 * Assert.assertTrue(false); }
	 * 
	 * }
	 */
	/*
	 * @Test(priority=4) public void financechkradvanceSearchEtailerID() throws
	 * InterruptedException, IOException {
	 * 
	 * wbLib.waitForPageTobeLoad(); Thread.sleep(5000); FinanceCheckerPage
	 * financechkrpg=PageFactory.initElements(TestBase.driver,
	 * FinanceCheckerPage.class); CashierMakerConsumerHomePage
	 * homePage=PageFactory.initElements(TestBase.driver,
	 * CashierMakerConsumerHomePage.class);
	 * 
	 * Thread.sleep(5000); homePage.getAdvanceSearchButton().click();
	 * homePage.getVariabledropdown().click();
	 * financechkrpg.getSelectEtailerID().click();
	 * homePage.getOperatordropdown().click();
	 * homePage.getOperatorequalsTo().click(); Thread.sleep(2000);
	 * homePage.getValueinputfield().click();
	 * homePage.getValueinputfield().sendKeys(prop.getProperty("EtailerID"));
	 * homePage.getAddrow().click(); homePage.getApplyfilter().click();
	 * Thread.sleep(8000);
	 * 
	 * boolean EtailerID =
	 * driver.getPageSource().contains(prop.getProperty("EtailerID"));
	 * if(EtailerID==true) { Assert.assertTrue(true);
	 * LOGGER.info("EtailerID advancedsearch test case passed...");
	 * testutil.captureScreen(driver, "EtailerID advancedsearch");
	 * 
	 * } else { LOGGER.info("EtailerID advancedsearchs test case failed...");
	 * testutil.captureScreen(driver, "EtailerID advancedsearch failed");
	 * Assert.assertTrue(false); }
	 * 
	 * }
	 * 
	 * @Test(priority=5) public void financechkradvanceSearchSLNo() throws
	 * InterruptedException, IOException {
	 * 
	 * wbLib.waitForPageTobeLoad(); Thread.sleep(5000); FinanceCheckerPage
	 * financechkrpg=PageFactory.initElements(TestBase.driver,
	 * FinanceCheckerPage.class); CashierMakerConsumerHomePage
	 * homePage=PageFactory.initElements(TestBase.driver,
	 * CashierMakerConsumerHomePage.class);
	 * 
	 * // Thread.sleep(5000); homePage.getAdvanceSearchButton().click();
	 * homePage.getVariabledropdown().click();
	 * financechkrpg.getSelectSlno().click();
	 * homePage.getOperatordropdown().click();
	 * homePage.getOperatorequalsTo().click(); Thread.sleep(2000);
	 * homePage.getValueinputfield().click();
	 * homePage.getValueinputfield().sendKeys(prop.getProperty("slNumber"));
	 * homePage.getAddrow().click(); homePage.getApplyfilter().click();
	 * Thread.sleep(8000);
	 * 
	 * boolean slNumber =
	 * driver.getPageSource().contains(prop.getProperty("slNumber"));
	 * if(slNumber==true) { Assert.assertTrue(true);
	 * LOGGER.info("slNumber advancedsearch test case passed...");
	 * testutil.captureScreen(driver, "slNumber advancedsearch");
	 * 
	 * } else { LOGGER.info("slNumber advancedsearchs test case failed...");
	 * testutil.captureScreen(driver, "slNumber advancedsearch failed");
	 * Assert.assertTrue(false); }
	 * 
	 * }
	 * 
	 * @Test(priority=6) public void financechkradvanceSearchShipmentID() throws
	 * InterruptedException, IOException {
	 * 
	 * wbLib.waitForPageTobeLoad(); Thread.sleep(5000); FinanceCheckerPage
	 * financechkrpg=PageFactory.initElements(TestBase.driver,
	 * FinanceCheckerPage.class); CashierMakerConsumerHomePage
	 * homePage=PageFactory.initElements(TestBase.driver,
	 * CashierMakerConsumerHomePage.class);
	 * 
	 * //Thread.sleep(5000); homePage.getAdvanceSearchButton().click();
	 * homePage.getVariabledropdown().click();
	 * financechkrpg.getSelectShipmntID().click();
	 * homePage.getOperatordropdown().click();
	 * homePage.getOperatorequalsTo().click(); Thread.sleep(2000);
	 * homePage.getValueinputfield().click();
	 * homePage.getValueinputfield().sendKeys(prop.getProperty("ShipmentID"));
	 * homePage.getAddrow().click(); homePage.getApplyfilter().click();
	 * Thread.sleep(8000);
	 * 
	 * boolean ShipmentID =
	 * driver.getPageSource().contains(prop.getProperty("ShipmentID"));
	 * if(ShipmentID==true) { Assert.assertTrue(true);
	 * LOGGER.info("ShipmentID advancedsearch test case passed...");
	 * testutil.captureScreen(driver, "ShipmentID advancedsearch");
	 * 
	 * } else { LOGGER.info("ShipmentID advancedsearchs test case failed...");
	 * testutil.captureScreen(driver, "ShipmentID advancedsearch failed");
	 * Assert.assertTrue(false); }
	 * 
	 * }
	 */	
	@AfterMethod
	public void tearDown(){;
		driver.quit();
	}
}
