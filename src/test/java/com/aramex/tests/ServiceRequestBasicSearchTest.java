package com.aramex.tests;

import java.io.IOException;
import java.util.logging.Logger;

import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.aramex.base.TestBase;
import com.aramex.pages.AdvanceSearchOperatorPage;
import com.aramex.pages.CashierMakerConsumerHomePage;
import com.aramex.pages.LoginPage;
import com.aramex.pages.ServiceRequestPage;
import com.aramex.pages.ShipmentsBasicSearchPage;
import com.aramex.util.TestUtil;
import com.aramex.util.WebDriverLib;

public class ServiceRequestBasicSearchTest extends TestBase{
	
	WebDriverLib wbLib=new WebDriverLib();
	Logger LOGGER = Logger.getLogger(ServiceRequestBasicSearchTest.class.getName());
	LoginTest logintest = new LoginTest();
	LoginPage devHomePage=PageFactory.initElements(TestBase.driver, LoginPage.class);
	AdvanceSearchOperatorPage operatorpg=PageFactory.initElements(TestBase.driver, AdvanceSearchOperatorPage.class);
	  CashierMakerConsumerHomePage homePage=PageFactory.initElements(TestBase.driver, CashierMakerConsumerHomePage.class);
	TestUtil testutil =  new TestUtil();

	
	@BeforeMethod
	public void logintoAramexAppliction() throws InterruptedException {
		logintest.loginToAppAsTester(prop.getProperty("lmsusername"), prop.getProperty("lmspassword"));
		
	}

	@Test(priority=2) 
	  public void serviceRQbasicSearchMobileNumber() throws IOException, InterruptedException {
		wbLib.waitForPageTobeLoad();
		  Thread.sleep(5000);
		  ShipmentsBasicSearchPage shipmntbasicsrch=PageFactory.initElements(TestBase.driver, ShipmentsBasicSearchPage.class);
		  CashierMakerConsumerHomePage homePage=PageFactory.initElements(TestBase.driver, CashierMakerConsumerHomePage.class);
		  ServiceRequestPage serviceRQpg=PageFactory.initElements(TestBase.driver, ServiceRequestPage.class);
		  
		  shipmntbasicsrch.getClickondropdownToNavigateShipment().click();
		  serviceRQpg.getclickonServiceRQ().click();
		  serviceRQpg.getClickMYRequestbucket().click();
		  Thread.sleep(5000);
		  homePage.getFilterBy().click();
		  homePage.getSelectMobileNumber().click();
		  homePage.getBasicSearchInputField().click();
		  homePage.getBasicSearchInputField().sendKeys(prop.getProperty("serviceRQMobNo"));
		  homePage.getClickonSearchoption().click();
		 
		  Thread.sleep(5000);       
		  boolean serviceRQMobNo = driver.getPageSource().contains(prop.getProperty("serviceRQMobNo"));
		  if(serviceRQMobNo==true)
		  {
			  Assert.assertTrue(true);
			  LOGGER.info("serviceRQMobNo basicsearch test case passed...");
			  testutil.captureScreen(driver, "serviceRQMobNo searchpassed");
			  
		  }
		  else {
			  LOGGER.info("serviceRQMobNo basic search test case failed...");
			  testutil.captureScreen(driver, "serviceRQMobNo searchfailed");
			  Assert.assertTrue(false);
		  }

		  homePage.getClearfilter().click();		  
		  Thread.sleep(9000);
	}
	
	@Test(priority=3) 
	  public void serviceRQbasicSearchCustomerNM() throws IOException, InterruptedException {
		wbLib.waitForPageTobeLoad();
		  Thread.sleep(5000);
		  ShipmentsBasicSearchPage shipmntbasicsrch=PageFactory.initElements(TestBase.driver, ShipmentsBasicSearchPage.class);
		  CashierMakerConsumerHomePage homePage=PageFactory.initElements(TestBase.driver, CashierMakerConsumerHomePage.class);
		  ServiceRequestPage serviceRQpg=PageFactory.initElements(TestBase.driver, ServiceRequestPage.class);
		  
		  shipmntbasicsrch.getClickondropdownToNavigateShipment().click();
		  serviceRQpg.getclickonServiceRQ().click();
		  serviceRQpg.getClickMYRequestbucket().click();
		  Thread.sleep(5000);
		  homePage.getFilterBy().click();
		  shipmntbasicsrch.getSelectCustometName().click();
		  homePage.getBasicSearchInputField().click();
		  homePage.getBasicSearchInputField().sendKeys(prop.getProperty("serviceRQCustNM"));
		  homePage.getClickonSearchoption().click();
		 
		  Thread.sleep(5000);       
		  boolean serviceRQCustNM = driver.getPageSource().contains(prop.getProperty("serviceRQCustNM"));
		  if(serviceRQCustNM==true)
		  {
			  Assert.assertTrue(true);
			  LOGGER.info("serviceRQCustNM basicsearch test case passed...");
			  testutil.captureScreen(driver, "serviceRQCustNM searchpassed");
			  
		  }
		  else {
			  LOGGER.info("serviceRQCustNM basic search test case failed...");
			  testutil.captureScreen(driver, "serviceRQCustNM searchfailed");
			  Assert.assertTrue(false);
		  }

		  homePage.getClearfilter().click();		  
		  Thread.sleep(9000);
	}
	
	@Test(priority=4) 
	  public void serviceRQbasicSearchCustomerNoSAP() throws IOException, InterruptedException {
		wbLib.waitForPageTobeLoad();
		  Thread.sleep(5000);
		  ShipmentsBasicSearchPage shipmntbasicsrch=PageFactory.initElements(TestBase.driver, ShipmentsBasicSearchPage.class);
		  CashierMakerConsumerHomePage homePage=PageFactory.initElements(TestBase.driver, CashierMakerConsumerHomePage.class);
		  ServiceRequestPage serviceRQpg=PageFactory.initElements(TestBase.driver, ServiceRequestPage.class);
		  
		  shipmntbasicsrch.getClickondropdownToNavigateShipment().click();
		  serviceRQpg.getclickonServiceRQ().click();
		  serviceRQpg.getClickMYRequestbucket().click();
		  Thread.sleep(5000);
		  homePage.getFilterBy().click();
		  serviceRQpg.getSelectCustNoSAP().click();
		  homePage.getBasicSearchInputField().click();
		  homePage.getBasicSearchInputField().sendKeys(prop.getProperty("serviceRQCustomerNoSAP"));
		  homePage.getClickonSearchoption().click();
		 
		  Thread.sleep(5000);       
		  boolean CustomerNoSAP = driver.getPageSource().contains(prop.getProperty("serviceRQCustomerNoSAP"));
		  if(CustomerNoSAP==true)
		  {
			  Assert.assertTrue(true);
			  LOGGER.info("serviceRQCustomerNoSAP basicsearch test case passed...");
			  testutil.captureScreen(driver, "serviceRQCustomerNoSAP basicsearchpassed");
			  
		  }
		  else {
			  LOGGER.info("serviceRQCustomerNoSAP basic search test case failed...");
			  testutil.captureScreen(driver, "serviceRQCustomerNoSAP basicsearchfailed");
			  Assert.assertTrue(false);
		  }

		  homePage.getClearfilter().click();		  
		  Thread.sleep(9000);
	}
	@Test(priority=5) 
	  public void serviceRQbasicSearchReqRef() throws IOException, InterruptedException {
		wbLib.waitForPageTobeLoad();
		  Thread.sleep(5000);
		  ShipmentsBasicSearchPage shipmntbasicsrch=PageFactory.initElements(TestBase.driver, ShipmentsBasicSearchPage.class);
		  CashierMakerConsumerHomePage homePage=PageFactory.initElements(TestBase.driver, CashierMakerConsumerHomePage.class);
		  ServiceRequestPage serviceRQpg=PageFactory.initElements(TestBase.driver, ServiceRequestPage.class);
		  
		  shipmntbasicsrch.getClickondropdownToNavigateShipment().click();
		  serviceRQpg.getclickonServiceRQ().click();
		  serviceRQpg.getClickMYRequestbucket().click();
		  Thread.sleep(5000);
		  homePage.getFilterBy().click();
		  serviceRQpg.getSelectReqRef().click();
		  homePage.getBasicSearchInputField().click();
		  homePage.getBasicSearchInputField().sendKeys(prop.getProperty("serviceRQReqRef"));
		  homePage.getClickonSearchoption().click();
		 
		  Thread.sleep(5000);       
		  boolean serviceRQReqRef = driver.getPageSource().contains(prop.getProperty("serviceRQReqRef"));
		  if(serviceRQReqRef==true)
		  {
			  Assert.assertTrue(true);
			  LOGGER.info("serviceRQReqRef basicsearch test case passed...");
			  testutil.captureScreen(driver, "serviceRQReqRef basicsearchpassed");
			  
		  }
		  else {
			  LOGGER.info("serviceRQReqRef basic search test case failed...");
			  testutil.captureScreen(driver, "serviceRQReqRef basicsearchfailed");
			  Assert.assertTrue(false);
		  }

		  homePage.getClearfilter().click();		  
		  Thread.sleep(9000);
	}
	
	@Test(priority=6) 
	  public void serviceRQbasicSearchRepaymntAMNT() throws IOException, InterruptedException {
		wbLib.waitForPageTobeLoad();
		  Thread.sleep(5000);
		  ShipmentsBasicSearchPage shipmntbasicsrch=PageFactory.initElements(TestBase.driver, ShipmentsBasicSearchPage.class);
		  CashierMakerConsumerHomePage homePage=PageFactory.initElements(TestBase.driver, CashierMakerConsumerHomePage.class);
		  ServiceRequestPage serviceRQpg=PageFactory.initElements(TestBase.driver, ServiceRequestPage.class);
		  
		  shipmntbasicsrch.getClickondropdownToNavigateShipment().click();
		  serviceRQpg.getclickonServiceRQ().click();
		  serviceRQpg.getClickMYRequestbucket().click();
		  Thread.sleep(5000);
		  homePage.getFilterBy().click();
		  serviceRQpg.getSelectRepayAmnt().click();
		  homePage.getBasicSearchInputField().click();
		  homePage.getBasicSearchInputField().sendKeys(prop.getProperty("serviceRQRepaymentAmount"));
		  homePage.getClickonSearchoption().click();
		 
		  Thread.sleep(5000);       
		  boolean serviceRQRepaymentAmount = driver.getPageSource().contains(prop.getProperty("serviceRQRepaymentAmount"));
		  if(serviceRQRepaymentAmount==true)
		  {
			  Assert.assertTrue(true);
			  LOGGER.info("serviceRQRepaymentAmount basicsearch test case passed...");
			  testutil.captureScreen(driver, "serviceRQRepaymentAmount basicsearchpassed");
			  
		  }
		  else {
			  LOGGER.info("serviceRQRepaymentAmount basic search test case failed...");
			  testutil.captureScreen(driver, "serviceRQRepaymentAmount basicsearchfailed");
			  Assert.assertTrue(false);
		  }

		  homePage.getClearfilter().click();		  
		  Thread.sleep(7000);
	}
	@Test(priority=7) 
	  public void serviceRQbasicSearchTrancCurr() throws IOException, InterruptedException {
		wbLib.waitForPageTobeLoad();
		  Thread.sleep(5000);
		  ShipmentsBasicSearchPage shipmntbasicsrch=PageFactory.initElements(TestBase.driver, ShipmentsBasicSearchPage.class);
		  CashierMakerConsumerHomePage homePage=PageFactory.initElements(TestBase.driver, CashierMakerConsumerHomePage.class);
		  ServiceRequestPage serviceRQpg=PageFactory.initElements(TestBase.driver, ServiceRequestPage.class);
		  
		  shipmntbasicsrch.getClickondropdownToNavigateShipment().click();
		  serviceRQpg.getclickonServiceRQ().click();
		  serviceRQpg.getClickMYRequestbucket().click();
		  Thread.sleep(5000);
		  homePage.getFilterBy().click();
		  serviceRQpg.getSelectTransCurr().click();
		  homePage.getBasicSearchInputField().click();
		  homePage.getBasicSearchInputField().sendKeys(prop.getProperty("CurrencyCode"));
		  homePage.getClickonSearchoption().click();
		 
		  Thread.sleep(5000);       
		  boolean CurrencyCode = driver.getPageSource().contains(prop.getProperty("CurrencyCode"));
		  if(CurrencyCode==true)
		  {
			  Assert.assertTrue(true);
			  LOGGER.info("serviceRQCurrencyCode basicsearch test case passed...");
			  testutil.captureScreen(driver, "serviceRQCurrencyCode basicsearchpassed");
			  
		  }
		  else {
			  LOGGER.info("CurrencyCode basic search test case failed...");
			  testutil.captureScreen(driver, "serviceRQCurrencyCode basicsearchfailed");
			  Assert.assertTrue(false);
		  }

		  homePage.getClearfilter().click();		  
		  Thread.sleep(7000);
	}
	@Test(priority=8) 
	  public void serviceRQbasicSearchShipmntValue() throws IOException, InterruptedException {
		wbLib.waitForPageTobeLoad();
		  Thread.sleep(5000);
		  ShipmentsBasicSearchPage shipmntbasicsrch=PageFactory.initElements(TestBase.driver, ShipmentsBasicSearchPage.class);
		  CashierMakerConsumerHomePage homePage=PageFactory.initElements(TestBase.driver, CashierMakerConsumerHomePage.class);
		  ServiceRequestPage serviceRQpg=PageFactory.initElements(TestBase.driver, ServiceRequestPage.class);
		  
		  shipmntbasicsrch.getClickondropdownToNavigateShipment().click();
		  serviceRQpg.getclickonServiceRQ().click();
		  serviceRQpg.getClickMYRequestbucket().click();
		  Thread.sleep(5000);
		  homePage.getFilterBy().click();
		  serviceRQpg.getSelectShipVal().click();
		  homePage.getBasicSearchInputField().click();
		  homePage.getBasicSearchInputField().sendKeys(prop.getProperty("servcRQShipVal"));
		  homePage.getClickonSearchoption().click();
		 
		  Thread.sleep(5000);       
		  boolean servcRQShipVal = driver.getPageSource().contains(prop.getProperty("servcRQShipVal"));
		  if(servcRQShipVal==true)
		  {
			  Assert.assertTrue(true);
			  LOGGER.info("servcRQShipVal basicsearch test case passed...");
			  testutil.captureScreen(driver, "servcRQShipVal basicsearch");
			  
		  }
		  else {
			  LOGGER.info("servcRQShipVal basic search test case failed...");
			  testutil.captureScreen(driver, "servcRQShipVal basicsearchfailed");
			  Assert.assertTrue(false);
		  }

		  homePage.getClearfilter().click();		  
		  Thread.sleep(7000);
	}
	@Test(priority=9) 
	  public void serviceRQbasicSearchAmntToPaid() throws IOException, InterruptedException {
		wbLib.waitForPageTobeLoad();
		  Thread.sleep(5000);
		  ShipmentsBasicSearchPage shipmntbasicsrch=PageFactory.initElements(TestBase.driver, ShipmentsBasicSearchPage.class);
		  CashierMakerConsumerHomePage homePage=PageFactory.initElements(TestBase.driver, CashierMakerConsumerHomePage.class);
		  ServiceRequestPage serviceRQpg=PageFactory.initElements(TestBase.driver, ServiceRequestPage.class);
		  
		  shipmntbasicsrch.getClickondropdownToNavigateShipment().click();
		  serviceRQpg.getclickonServiceRQ().click();
		  serviceRQpg.getClickMYRequestbucket().click();
		  Thread.sleep(5000);
		  homePage.getFilterBy().click();
		  serviceRQpg.getSelectAmntToPaid().click();
		  homePage.getBasicSearchInputField().click();
		  homePage.getBasicSearchInputField().sendKeys(prop.getProperty("ServcReqAmntToPaid"));
		  homePage.getClickonSearchoption().click();
		 
		  Thread.sleep(5000);       
		  boolean ServcReqAmntToPaid = driver.getPageSource().contains(prop.getProperty("ServcReqAmntToPaid"));
		  if(ServcReqAmntToPaid==true)
		  {
			  Assert.assertTrue(true);
			  LOGGER.info("ServcReqAmntToPaid basicsearch test case passed...");
			  testutil.captureScreen(driver, "ServcReqAmntToPaid basicsearch");
			  
		  }
		  else {
			  LOGGER.info("ServcReqAmntToPaid basic search test case failed...");
			  testutil.captureScreen(driver, "ServcReqAmntToPaid basicsearchfailed");
			  Assert.assertTrue(false);
		  }

		  homePage.getClearfilter().click();		  
		  Thread.sleep(7000);
	}
	@Test(priority=10) 
	  public void serviceRQbasicSearchTotlPaidAmnt() throws IOException, InterruptedException {
		wbLib.waitForPageTobeLoad();
		  Thread.sleep(5000);
		  ShipmentsBasicSearchPage shipmntbasicsrch=PageFactory.initElements(TestBase.driver, ShipmentsBasicSearchPage.class);
		  CashierMakerConsumerHomePage homePage=PageFactory.initElements(TestBase.driver, CashierMakerConsumerHomePage.class);
		  ServiceRequestPage serviceRQpg=PageFactory.initElements(TestBase.driver, ServiceRequestPage.class);
		  
		  shipmntbasicsrch.getClickondropdownToNavigateShipment().click();
		  serviceRQpg.getclickonServiceRQ().click();
		  serviceRQpg.getClickMYRequestbucket().click();
		  Thread.sleep(5000);
		  homePage.getFilterBy().click();
		  serviceRQpg.getSelectTotlAmntpaid().click();
		  homePage.getBasicSearchInputField().click();
		  homePage.getBasicSearchInputField().sendKeys(prop.getProperty("ServcReqtotPaidAmnt"));
		  homePage.getClickonSearchoption().click();
		 
		  Thread.sleep(5000);       
		  boolean ServcReqtotPaidAmnt = driver.getPageSource().contains(prop.getProperty("ServcReqtotPaidAmnt"));
		  if(ServcReqtotPaidAmnt==true)
		  {
			  Assert.assertTrue(true);
			  LOGGER.info("ServcReqtotPaidAmnt basicsearch test case passed...");
			  testutil.captureScreen(driver, "ServcReqtotPaidAmnt basicsearch");
			  
		  }
		  else {
			  LOGGER.info("ServcReqtotPaidAmnt basic search test case failed...");
			  testutil.captureScreen(driver, "ServcReqtotPaidAmnt basicsearchfailed");
			  Assert.assertTrue(false);
		  }

		  homePage.getClearfilter().click();		  
		  Thread.sleep(7000);
	}

	@Test(priority=11) 
	  public void serviceRQbasicSearchPaymntType() throws IOException, InterruptedException {
		wbLib.waitForPageTobeLoad();
		  Thread.sleep(5000);
		  ShipmentsBasicSearchPage shipmntbasicsrch=PageFactory.initElements(TestBase.driver, ShipmentsBasicSearchPage.class);
		  CashierMakerConsumerHomePage homePage=PageFactory.initElements(TestBase.driver, CashierMakerConsumerHomePage.class);
		  ServiceRequestPage serviceRQpg=PageFactory.initElements(TestBase.driver, ServiceRequestPage.class);
		  
		  shipmntbasicsrch.getClickondropdownToNavigateShipment().click();
		  serviceRQpg.getclickonServiceRQ().click();
		  serviceRQpg.getClickMYRequestbucket().click();
		  Thread.sleep(5000);
		  homePage.getFilterBy().click();
		  serviceRQpg.getSelectPayType().click();
		  homePage.getBasicSearchInputField().click();
		  homePage.getBasicSearchInputField().sendKeys(prop.getProperty("serviceRQPaymntType"));
		  homePage.getClickonSearchoption().click();
		 
		  Thread.sleep(5000);       
		  boolean serviceRQPaymntType = driver.getPageSource().contains(prop.getProperty("serviceRQPaymntType"));
		  if(serviceRQPaymntType==true)
		  {
			  Assert.assertTrue(true);
			  LOGGER.info("serviceRQPaymntType basicsearch test case passed...");
			  testutil.captureScreen(driver, "serviceRQPaymntType basicsearch");
			  
		  }
		  else {
			  LOGGER.info("serviceRQPaymntType basic search test case failed...");
			  testutil.captureScreen(driver, "serviceRQPaymntType basicsearchfailed");
			  Assert.assertTrue(false);
		  }

		  homePage.getClearfilter().click();		  
		  Thread.sleep(7000);
	}

	@Test(priority=12) 
	  public void serviceRQbasicSearchTransDate() throws IOException, InterruptedException {
		wbLib.waitForPageTobeLoad();
		  Thread.sleep(5000);
		  ShipmentsBasicSearchPage shipmntbasicsrch=PageFactory.initElements(TestBase.driver, ShipmentsBasicSearchPage.class);
		  CashierMakerConsumerHomePage homePage=PageFactory.initElements(TestBase.driver, CashierMakerConsumerHomePage.class);
		  ServiceRequestPage serviceRQpg=PageFactory.initElements(TestBase.driver, ServiceRequestPage.class);
		  
		  shipmntbasicsrch.getClickondropdownToNavigateShipment().click();
		  serviceRQpg.getclickonServiceRQ().click();
		  serviceRQpg.getClickMYRequestbucket().click();
		  Thread.sleep(5000);
		  homePage.getFilterBy().click();
		  serviceRQpg.getSelectTranscDt().click();
		  homePage.getBasicSearchInputField().click();
		  homePage.getBasicSearchInputField().sendKeys(prop.getProperty("serviceRQTransDt"));
		  homePage.getClickonSearchoption().click();
		 
		  Thread.sleep(5000);       
		  boolean serviceRQTransDt = driver.getPageSource().contains(prop.getProperty("serviceRQTransDt"));
		  if(serviceRQTransDt==true)
		  {
			  Assert.assertTrue(true);
			  LOGGER.info("serviceRQTransDt basicsearch test case passed...");
			  testutil.captureScreen(driver, "serviceRQTransDt basicsearch");
			  
		  }
		  else {
			  LOGGER.info("serviceRQTransDt basic search test case failed...");
			  testutil.captureScreen(driver, "serviceRQTransDt basicsearchfailed");
			  Assert.assertTrue(false);
		  }

		  homePage.getClearfilter().click();		  
		  Thread.sleep(7000);
	}

	@Test(priority=1) 
	  public void serviceRQbasicSearchRaisedBy() throws IOException, InterruptedException {
		wbLib.waitForPageTobeLoad();
		  Thread.sleep(5000);
		  ShipmentsBasicSearchPage shipmntbasicsrch=PageFactory.initElements(TestBase.driver, ShipmentsBasicSearchPage.class);
		  CashierMakerConsumerHomePage homePage=PageFactory.initElements(TestBase.driver, CashierMakerConsumerHomePage.class);
		  ServiceRequestPage serviceRQpg=PageFactory.initElements(TestBase.driver, ServiceRequestPage.class);
		  
		  shipmntbasicsrch.getClickondropdownToNavigateShipment().click();
		  serviceRQpg.getclickonServiceRQ().click();
		  serviceRQpg.getClickMYRequestbucket().click();
		  Thread.sleep(5000);
		  homePage.getFilterBy().click();
		  serviceRQpg.getSelectRaisedBy().click();
		  homePage.getBasicSearchInputField().click();
		  homePage.getBasicSearchInputField().sendKeys(prop.getProperty("serviceRQRaisedBy"));
		  homePage.getClickonSearchoption().click();
		 
		  Thread.sleep(5000);       
		  boolean serviceRQRaisedBy = driver.getPageSource().contains(prop.getProperty("serviceRQRaisedBy"));
		  if(serviceRQRaisedBy==true)
		  {
			  Assert.assertTrue(true);
			  LOGGER.info("serviceRQRaisedBy basicsearch test case passed...");
			  testutil.captureScreen(driver, "serviceRQRaisedBy basicsearch");
			  
		  }
		  else {
			  LOGGER.info("serviceRQRaisedBy basic search test case failed...");
			  testutil.captureScreen(driver, "serviceRQRaisedBy basicsearchfailed");
			  Assert.assertTrue(false);
		  }

		  homePage.getClearfilter().click();		  
		  Thread.sleep(7000);
	}

	@Test(priority=13) 
	  public void serviceRQbasicSearchApprRejBy() throws IOException, InterruptedException {
		wbLib.waitForPageTobeLoad();
		  Thread.sleep(5000);
		  ShipmentsBasicSearchPage shipmntbasicsrch=PageFactory.initElements(TestBase.driver, ShipmentsBasicSearchPage.class);
		  CashierMakerConsumerHomePage homePage=PageFactory.initElements(TestBase.driver, CashierMakerConsumerHomePage.class);
		  ServiceRequestPage serviceRQpg=PageFactory.initElements(TestBase.driver, ServiceRequestPage.class);
		  
		  shipmntbasicsrch.getClickondropdownToNavigateShipment().click();
		  serviceRQpg.getclickonServiceRQ().click();
		  serviceRQpg.getClickMYRequestbucket().click();
		  Thread.sleep(5000);
		  homePage.getFilterBy().click();
		  serviceRQpg.getSelectAppRejBy().click();
		  homePage.getBasicSearchInputField().click();
		  homePage.getBasicSearchInputField().sendKeys(prop.getProperty("serviceRQApprvRejBy"));
		  homePage.getClickonSearchoption().click();
		 
		  Thread.sleep(5000);       
		  boolean serviceRQApprvRejBy = driver.getPageSource().contains(prop.getProperty("serviceRQApprvRejBy"));
		  if(serviceRQApprvRejBy==true)
		  {
			  Assert.assertTrue(true);
			  LOGGER.info("serviceRQApprvRejBy basicsearch test case passed...");
			  testutil.captureScreen(driver, "serviceRQApprvRejBy basicsearch");
			  
		  }
		  else {
			  LOGGER.info("serviceRQApprvRejBy basic search test case failed...");
			  testutil.captureScreen(driver, "serviceRQApprvRejBy basicsearchfailed");
			  Assert.assertTrue(false);
		  }

		  homePage.getClearfilter().click();		  
		  Thread.sleep(7000);
	}

	@Test(priority=14) 
	  public void serviceRQbasicSearchAssignTo() throws IOException, InterruptedException {
		wbLib.waitForPageTobeLoad();
		  Thread.sleep(5000);
		  ShipmentsBasicSearchPage shipmntbasicsrch=PageFactory.initElements(TestBase.driver, ShipmentsBasicSearchPage.class);
		  CashierMakerConsumerHomePage homePage=PageFactory.initElements(TestBase.driver, CashierMakerConsumerHomePage.class);
		  ServiceRequestPage serviceRQpg=PageFactory.initElements(TestBase.driver, ServiceRequestPage.class);
		  
		  shipmntbasicsrch.getClickondropdownToNavigateShipment().click();
		  serviceRQpg.getclickonServiceRQ().click();
		  serviceRQpg.getClickMYRequestbucket().click();
		  Thread.sleep(5000);
		  homePage.getFilterBy().click();
		  serviceRQpg.getSelectAssignTo().click();
		  homePage.getBasicSearchInputField().click();
		  homePage.getBasicSearchInputField().sendKeys(prop.getProperty("serviceRQAssignTo"));
		  homePage.getClickonSearchoption().click();
		 
		  Thread.sleep(5000);       
		  boolean serviceRQAssignTo = driver.getPageSource().contains(prop.getProperty("serviceRQAssignTo"));
		  if(serviceRQAssignTo==true)
		  {
			  Assert.assertTrue(true);
			  LOGGER.info("serviceRQAssignTo basicsearch test case passed...");
			  testutil.captureScreen(driver, "serviceRQAssignTo basicsearch");
			  
		  }
		  else {
			  LOGGER.info("serviceRQAssignTo basic search test case failed...");
			  testutil.captureScreen(driver, "serviceRQAssignTo basicsearchfailed");
			  Assert.assertTrue(false);
		  }

		  homePage.getClearfilter().click();		  
		  Thread.sleep(7000);
	}

	@Test(priority=15) 
	  public void serviceRQbasicSearchShipNo() throws IOException, InterruptedException {
		wbLib.waitForPageTobeLoad();
		  Thread.sleep(5000);
		  ShipmentsBasicSearchPage shipmntbasicsrch=PageFactory.initElements(TestBase.driver, ShipmentsBasicSearchPage.class);
		  CashierMakerConsumerHomePage homePage=PageFactory.initElements(TestBase.driver, CashierMakerConsumerHomePage.class);
		  ServiceRequestPage serviceRQpg=PageFactory.initElements(TestBase.driver, ServiceRequestPage.class);
		  
		  shipmntbasicsrch.getClickondropdownToNavigateShipment().click();
		  serviceRQpg.getclickonServiceRQ().click();
		  serviceRQpg.getClickMYRequestbucket().click();
		  Thread.sleep(5000);
		  homePage.getFilterBy().click();
		  shipmntbasicsrch.getSelectShipmentNumber().click();
		  homePage.getBasicSearchInputField().click();
		  homePage.getBasicSearchInputField().sendKeys(prop.getProperty("serviceRQShipmntNo"));
		  homePage.getClickonSearchoption().click();
		 
		  Thread.sleep(5000);       
		  boolean serviceRQShipmntNo = driver.getPageSource().contains(prop.getProperty("serviceRQShipmntNo"));
		  if(serviceRQShipmntNo==true)
		  {
			  Assert.assertTrue(true);
			  LOGGER.info("serviceRQShipmntNo basicsearch test case passed...");
			  testutil.captureScreen(driver, "serviceRQShipmntNo basicsearch");
			  
		  }
		  else {
			  LOGGER.info("serviceRQShipmntNo basic search test case failed...");
			  testutil.captureScreen(driver, "serviceRQShipmntNo basicsearchfailed");
			  Assert.assertTrue(false);
		  }

		  homePage.getClearfilter().click();		  
		  Thread.sleep(7000);
	}

	@Test(priority=16) 
	  public void serviceRQbasicSearchOrdNo() throws IOException, InterruptedException {
		wbLib.waitForPageTobeLoad();
		  Thread.sleep(5000);
		  ShipmentsBasicSearchPage shipmntbasicsrch=PageFactory.initElements(TestBase.driver, ShipmentsBasicSearchPage.class);
		  CashierMakerConsumerHomePage homePage=PageFactory.initElements(TestBase.driver, CashierMakerConsumerHomePage.class);
		  ServiceRequestPage serviceRQpg=PageFactory.initElements(TestBase.driver, ServiceRequestPage.class);
		  
		  shipmntbasicsrch.getClickondropdownToNavigateShipment().click();
		  serviceRQpg.getclickonServiceRQ().click();
		  serviceRQpg.getClickMYRequestbucket().click();
		  Thread.sleep(5000);
		  homePage.getFilterBy().click();
		  shipmntbasicsrch.getSelectOrderNumber().click();
		  homePage.getBasicSearchInputField().click();
		  homePage.getBasicSearchInputField().sendKeys(prop.getProperty("serviceRQOrdNo"));
		  homePage.getClickonSearchoption().click();
		 
		  Thread.sleep(5000);       
		  boolean serviceRQOrdNo = driver.getPageSource().contains(prop.getProperty("serviceRQOrdNo"));
		  if(serviceRQOrdNo==true)
		  {
			  Assert.assertTrue(true);
			  LOGGER.info("serviceRQOrdNo basicsearch test case passed...");
			  testutil.captureScreen(driver, "serviceRQOrdNo basicsearch");
			  
		  }
		  else {
			  LOGGER.info("serviceRQOrdNo basic search test case failed...");
			  testutil.captureScreen(driver, "serviceRQOrdNo basicsearchfailed");
			  Assert.assertTrue(false);
		  }

		  homePage.getClearfilter().click();		  
		  Thread.sleep(7000);
	}

	@Test(priority=17) 
	  public void serviceRQbasicSearchStatus() throws IOException, InterruptedException {
		wbLib.waitForPageTobeLoad();
		  Thread.sleep(5000);
		  ShipmentsBasicSearchPage shipmntbasicsrch=PageFactory.initElements(TestBase.driver, ShipmentsBasicSearchPage.class);
		  CashierMakerConsumerHomePage homePage=PageFactory.initElements(TestBase.driver, CashierMakerConsumerHomePage.class);
		  ServiceRequestPage serviceRQpg=PageFactory.initElements(TestBase.driver, ServiceRequestPage.class);
		  
		  shipmntbasicsrch.getClickondropdownToNavigateShipment().click();
		  serviceRQpg.getclickonServiceRQ().click();
		  serviceRQpg.getClickMYRequestbucket().click();
		  Thread.sleep(5000);
		  homePage.getFilterBy().click();
		  shipmntbasicsrch.getSelectstatus().click();
		  homePage.getBasicSearchInputField().click();
		  homePage.getBasicSearchInputField().sendKeys(prop.getProperty("serviceRQStatus"));
		  homePage.getClickonSearchoption().click();
		 
		  Thread.sleep(5000);       
		  boolean serviceRQStatus = driver.getPageSource().contains(prop.getProperty("serviceRQStatus"));
		  if(serviceRQStatus==true)
		  {
			  Assert.assertTrue(true);
			  LOGGER.info("serviceRQStatus basicsearch test case passed...");
			  testutil.captureScreen(driver, "serviceRQStatus basicsearch");
			  
		  }
		  else {
			  LOGGER.info("serviceRQStatus basic search test case failed...");
			  testutil.captureScreen(driver, "serviceRQStatus basicsearchfailed");
			  Assert.assertTrue(false);
		  }

		  homePage.getClearfilter().click();		  
		  Thread.sleep(7000);
	}

	@Test(priority=18) 
	  public void serviceRQbasicSearchLoanStatus() throws IOException, InterruptedException {
		wbLib.waitForPageTobeLoad();
		  Thread.sleep(5000);
		  ShipmentsBasicSearchPage shipmntbasicsrch=PageFactory.initElements(TestBase.driver, ShipmentsBasicSearchPage.class);
		  CashierMakerConsumerHomePage homePage=PageFactory.initElements(TestBase.driver, CashierMakerConsumerHomePage.class);
		  ServiceRequestPage serviceRQpg=PageFactory.initElements(TestBase.driver, ServiceRequestPage.class);
		  
		  shipmntbasicsrch.getClickondropdownToNavigateShipment().click();
		  serviceRQpg.getclickonServiceRQ().click();
		  serviceRQpg.getClickMYRequestbucket().click();
		  Thread.sleep(5000);
		  homePage.getFilterBy().click();
		  shipmntbasicsrch.getSelectLoanStatus().click();
		  homePage.getBasicSearchInputField().click();
		  homePage.getBasicSearchInputField().sendKeys(prop.getProperty("serviceRQLoanStus"));
		  homePage.getClickonSearchoption().click();
		 
		  Thread.sleep(5000);       
		  boolean serviceRQLoanStus = driver.getPageSource().contains(prop.getProperty("serviceRQLoanStus"));
		  if(serviceRQLoanStus==true)
		  {
			  Assert.assertTrue(true);
			  LOGGER.info("serviceRQLoanStus basicsearch test case passed...");
			  testutil.captureScreen(driver, "serviceRQLoanStus basicsearch");
			  
		  }
		  else {
			  LOGGER.info("serviceRQLoanStus basic search test case failed...");
			  testutil.captureScreen(driver, "serviceRQLoanStus basicsearchfailed");
			  Assert.assertTrue(false);
		  }

		  homePage.getClearfilter().click();		  
		  Thread.sleep(7000);
	}
	
	@AfterMethod
	public void tearDown(){;
		driver.quit();
	}

}
