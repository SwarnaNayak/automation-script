package com.aramex.tests;

import java.io.IOException;
import java.util.logging.Logger;

import org.openqa.selenium.By;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.aramex.base.TestBase;
import com.aramex.pages.CashierCheckerPage;
import com.aramex.pages.CashierMakerConsumerHomePage;
import com.aramex.pages.LoginPage;
import com.aramex.pages.ServiceRequestPage;
import com.aramex.pages.ShipmentDetailViewPage;
import com.aramex.pages.ShipmentsBasicSearchPage;
import com.aramex.util.TestUtil;
import com.aramex.util.WebDriverLib;

public class ApprByBankTransferTest extends TestBase{

	ShipmentDetailViewPage shipmndetailvwpg = new ShipmentDetailViewPage();
	WebDriverLib wbLib=new WebDriverLib();
	LoginTest logintest = new LoginTest();
	TestUtil testutil =  new TestUtil();
	
	@BeforeMethod
	public void logintoAramexAppliction() throws InterruptedException {
		logintest.loginToAppAsTester(prop.getProperty("lmsusername"), prop.getProperty("lmspassword"));
		
	}
	
	@Test
	public void approveBnkTrnsfr() throws InterruptedException, IOException {
		
		ShipmentDetailViewPage shipdtlvwpg=PageFactory.initElements(TestBase.driver, ShipmentDetailViewPage.class);
		ServiceRequestPage serviceRQpg=PageFactory.initElements(TestBase.driver, ServiceRequestPage.class);
		ShipmentsBasicSearchPage shipmntbasicsrch=PageFactory.initElements(TestBase.driver, ShipmentsBasicSearchPage.class);
		LoginPage devHomePage=PageFactory.initElements(TestBase.driver, LoginPage.class);
		CashierCheckerPage chaschkrpg=PageFactory.initElements(TestBase.driver, CashierCheckerPage.class);
		Actions act =  new Actions(driver);
		CashierMakerConsumerHomePage homePage=PageFactory.initElements(TestBase.driver, CashierMakerConsumerHomePage.class);

		shipmndetailvwpg.clickon_search_Shipment(prop.getProperty("shipmentNoToApproveBankTrnsf"));
		//act.moveToElement(shipdtlvwpg.getShipmntselctarrow()).click().perform();
		shipdtlvwpg.getShipmntselctarrow().click();
		shipdtlvwpg.getSelectPayBYBnkTrsnf().click();
		Thread.sleep(3000);
		//Actions act =  new Actions(driver);
		act.moveToElement(driver.findElement(By.xpath("//span[text()='Raise']"))).click().perform();
		Thread.sleep(3000);
		//shipdtlvwpg.getClkRaise().click();
		shipdtlvwpg.getTransactioninput().sendKeys("1");
		shipdtlvwpg.getTranfrReff().sendKeys("1212");
		//shipdtlvwpg.getBnkaccntNo().sendKeys("2324412");
		//shipdtlvwpg.getComments().sendKeys("Test");
		Thread.sleep(3000);
		act.moveToElement(driver.findElement(By.xpath("//span[text()='Pay']"))).click().perform();
		//shipdtlvwpg.getPaybtn().click();
		Thread.sleep(4000);
		wbLib.waitForTextPresent("OK");
		
		String expectedMessage ="Successfully Saved";
		String actualmessage =driver.findElement(By.xpath("//span[text()='OK']/ancestor::div[@class='ant-modal-footer']/preceding-sibling::div[@class='ant-modal-body']")).getText();
		System.out.println(actualmessage);
		testutil.captureScreen(driver, "ApprovByBnkverifymessageIs "+actualmessage );
		Assert.assertEquals(actualmessage, expectedMessage);


		Thread.sleep(3000);
		act.moveToElement(shipdtlvwpg.getClkonOK()).click().perform();
		Thread.sleep(8000);
		wbLib.waitForPageTobeLoad();
		wbLib.waitForElementToClickble("//span[contains(text(),'Cashier Maker')]/preceding-sibling::i");		
		
		//navigate to cashier checker
		devHomePage.getChangetoCashierChkr().click();
		Thread.sleep(2000);
		devHomePage.getNavigateToCahsierChkr().click();
		wbLib.waitForPageTobeLoad();
		Thread.sleep(8000);
		homePage.getFilterBy().click();
		shipmntbasicsrch.getSelectShipmentNumber().click();
		homePage.getBasicSearchInputField().click();
		homePage.getBasicSearchInputField().sendKeys(prop.getProperty("shipmentNoToApproveBankTrnsf"));
		homePage.getClickonSearchoption().click();
		Thread.sleep(8000);
		
		if(driver.getPageSource().contains(prop.getProperty("shipmentNoToApproveBankTrnsf")))
		{
			System.out.println("shipmentNoToApproveBankTrnsf is present");
			driver.findElement(By.xpath("//td[text()='"+prop.getProperty("shipmentNoToApproveBankTrnsf")+"']")).click();
			testutil.captureScreen(driver, " ' "+prop.getProperty("shipmentNoToApproveBankTrnsf")+" ' present Incashierchkr pendingApprBuckt");
			Thread.sleep(3000);
			
			}else
			{
			System.out.println("shipmentNoToApproveBankTrnsf is absent");
			}
		Thread.sleep(3000);
		act.moveToElement(chaschkrpg.getAppRejPaymntbtn()).click().perform();
		Thread.sleep(3000);
		chaschkrpg.getChkActionDrpdwn().click();
		chaschkrpg.getApproveByChkr().click();
		act.moveToElement(chaschkrpg.getSubmitbtn()).click().perform();
		Thread.sleep(4000);
		wbLib.waitForTextPresent("OK");
		
		String successmsg ="Successfully Saved";
		String actualmessage1 =driver.findElement(By.xpath("//span[text()='OK']/ancestor::div[@class='ant-modal-footer']/preceding-sibling::div[@class='ant-modal-body']")).getText();
		System.out.println(actualmessage1);
		testutil.captureScreen(driver, "ApprovByCashierverifymsg "+actualmessage1 );
		Assert.assertEquals(actualmessage1, successmsg);

		Thread.sleep(3000);
		act.moveToElement(shipdtlvwpg.getClkonOK()).click().perform();
		Thread.sleep(6000);
		wbLib.waitForElementToClickble("//div[text()='Approved Bank Transfer']");
		serviceRQpg.getClickApprovedByBnkbuck().click();
		Thread.sleep(7000);
  
		homePage.getFilterBy().click();
		shipmntbasicsrch.getSelectShipmentNumber().click();
		homePage.getBasicSearchInputField().click();
		homePage.getBasicSearchInputField().sendKeys(prop.getProperty("shipmentNoToApproveBankTrnsf"));
		homePage.getClickonSearchoption().click();
		Thread.sleep(8000);
		
		if(driver.getPageSource().contains(prop.getProperty("shipmentNoToApproveBankTrnsf")))
		{
			System.out.println("shipmentNoToApproveBankTrnsf is present");
			driver.findElement(By.xpath("//td[text()='"+prop.getProperty("shipmentNoToApproveBankTrnsf")+"']")).click();
			testutil.captureScreen(driver, " ' "+prop.getProperty("shipmentNoToApproveBankTrnsf")+" ' present Incashierchkr ApprBnkTrnsfBuckt");
			Thread.sleep(3000);
			
			}else
			{
			System.out.println("shipmentNoToApproveBankTrnsf is absent");
			}
	
		Thread.sleep(2000);
		/*driver.findElement(By.xpath("//td[text()='"+prop.getProperty("demoshimpntno")+"']")).click();
		Thread.sleep(9000);*/
		
	}
	@AfterMethod
	public void tearDown(){;
		driver.quit();
	}

	
}
