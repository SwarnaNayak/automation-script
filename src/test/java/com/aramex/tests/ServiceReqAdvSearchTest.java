package com.aramex.tests;

import java.io.IOException;
import java.util.logging.Logger;

import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.aramex.base.TestBase;
import com.aramex.pages.CashierMakerConsumerHomePage;
import com.aramex.pages.LoginPage;
import com.aramex.pages.ServiceRequestPage;
import com.aramex.pages.ShipmentsBasicSearchPage;
import com.aramex.util.TestUtil;
import com.aramex.util.WebDriverLib;

public class ServiceReqAdvSearchTest extends TestBase{

	WebDriverLib wbLib=new WebDriverLib();
	Logger LOGGER = Logger.getLogger(ConsumerAdvanceSearchFuncTest.class.getName());
	LoginTest logintest = new LoginTest();
	TestUtil testutil =  new TestUtil();

	
	@BeforeMethod
	public void logintoAramexAppliction() throws InterruptedException {
		logintest.loginToAppAsTester(prop.getProperty("lmsusername"), prop.getProperty("lmspassword"));
		
	}
	
	@Test(priority=1) 
	  public void serviceRqAdvSearchMobileNumber() throws InterruptedException, IOException {
		
		wbLib.waitForPageTobeLoad();
		  Thread.sleep(5000);
		  ShipmentsBasicSearchPage shipmntbasicsrch=PageFactory.initElements(TestBase.driver, ShipmentsBasicSearchPage.class);
		  CashierMakerConsumerHomePage homePage=PageFactory.initElements(TestBase.driver, CashierMakerConsumerHomePage.class);
		  ServiceRequestPage serviceRQpg=PageFactory.initElements(TestBase.driver, ServiceRequestPage.class);
		  
		  shipmntbasicsrch.getClickondropdownToNavigateShipment().click();
		  serviceRQpg.getclickonServiceRQ().click();
		  serviceRQpg.getClickMYRequestbucket().click();
		  Thread.sleep(2000);
		  homePage.getAdvanceSearchButton().click();
		  homePage.getVariabledropdown().click();
		  homePage.getSelectMobileNumber().click();
		  homePage.getOperatordropdown().click();
		  homePage.getOperatorequalsTo().click();
		  Thread.sleep(2000);
		  homePage.getValueinputfield().click();
		  homePage.getValueinputfield().sendKeys(prop.getProperty("serviceRQMobNo"));
		  homePage.getAddrow().click();
		  homePage.getApplyfilter().click();
		  Thread.sleep(7000);
		  
		  boolean serviceRQMobNo = driver.getPageSource().contains(prop.getProperty("serviceRQMobNo"));
		  if(serviceRQMobNo==true)
		  {
			  Assert.assertTrue(true);
			  LOGGER.info("serviceRQMobNo advancedsearch test case passed...");
			  testutil.captureScreen(driver, "serviceRQMobNo advancedsearch");
		  }
		  else {
			  LOGGER.info("serviceRQMobNo advancedsearchs test case failed...");
			  testutil.captureScreen(driver, "serviceRQMobNo advancedsearch failed");
			  Assert.assertTrue(false);
		  }
	}
	
	@Test(priority=2) 
	  public void serviceRqAdvSearchCustomerNM() throws InterruptedException, IOException {
		
		wbLib.waitForPageTobeLoad();
		  Thread.sleep(5000);
		  ShipmentsBasicSearchPage shipmntbasicsrch=PageFactory.initElements(TestBase.driver, ShipmentsBasicSearchPage.class);
		  CashierMakerConsumerHomePage homePage=PageFactory.initElements(TestBase.driver, CashierMakerConsumerHomePage.class);
		  ServiceRequestPage serviceRQpg=PageFactory.initElements(TestBase.driver, ServiceRequestPage.class);
		  
		  shipmntbasicsrch.getClickondropdownToNavigateShipment().click();
		  serviceRQpg.getclickonServiceRQ().click();
		  serviceRQpg.getClickMYRequestbucket().click();
		  Thread.sleep(3000);
		  homePage.getAdvanceSearchButton().click();
		  homePage.getVariabledropdown().click();
		  shipmntbasicsrch.getSelectCustometName().click();
		  homePage.getOperatordropdown().click();
		  homePage.getOperatorequalsTo().click();
		  Thread.sleep(2000);
		  homePage.getValueinputfield().click();
		  homePage.getValueinputfield().sendKeys(prop.getProperty("serviceRQCustNM"));
		  homePage.getAddrow().click();
		  homePage.getApplyfilter().click();
		  Thread.sleep(7000);
		  
		  boolean serviceRQCustNM = driver.getPageSource().contains(prop.getProperty("serviceRQCustNM"));
		  if(serviceRQCustNM==true)
		  {
			  Assert.assertTrue(true);
			  LOGGER.info("serviceRQCustNM advancedsearch test case passed...");
			  testutil.captureScreen(driver, "serviceRQCustNM advancedsearch");
		  }
		  else {
			  LOGGER.info("serviceRQCustNM advancedsearchs test case failed...");
			  testutil.captureScreen(driver, "serviceRQCustNM advancedsearch failed");
			  Assert.assertTrue(false);
		  }
	}

	@Test(priority=3) 
	  public void serviceRqAdvSearchCustomerNoSAP() throws InterruptedException, IOException {
		
		wbLib.waitForPageTobeLoad();
		  Thread.sleep(5000);
		  ShipmentsBasicSearchPage shipmntbasicsrch=PageFactory.initElements(TestBase.driver, ShipmentsBasicSearchPage.class);
		  CashierMakerConsumerHomePage homePage=PageFactory.initElements(TestBase.driver, CashierMakerConsumerHomePage.class);
		  ServiceRequestPage serviceRQpg=PageFactory.initElements(TestBase.driver, ServiceRequestPage.class);
		  
		  shipmntbasicsrch.getClickondropdownToNavigateShipment().click();
		  serviceRQpg.getclickonServiceRQ().click();
		  serviceRQpg.getClickMYRequestbucket().click();
		  Thread.sleep(3000);
		  homePage.getAdvanceSearchButton().click();
		  homePage.getVariabledropdown().click();
		  serviceRQpg.getSelectCustNoSAP().click();
		  homePage.getOperatordropdown().click();
		  homePage.getOperatorequalsTo().click();
		  Thread.sleep(2000);
		  homePage.getValueinputfield().click();
		  homePage.getValueinputfield().sendKeys(prop.getProperty("serviceRQCustomerNoSAP"));
		  homePage.getAddrow().click();
		  homePage.getApplyfilter().click();
		  Thread.sleep(7000);
		  
		  boolean serviceRQCustomerNoSAP = driver.getPageSource().contains(prop.getProperty("serviceRQCustomerNoSAP"));
		  if(serviceRQCustomerNoSAP==true)
		  {
			  Assert.assertTrue(true);
			  LOGGER.info("serviceRQCustomerNoSAP advancedsearch test case passed...");
			  testutil.captureScreen(driver, "serviceRQCustomerNoSAP advancedsearch");
		  }
		  else {
			  LOGGER.info("serviceRQCustomerNoSAP advancedsearchs test case failed...");
			  testutil.captureScreen(driver, "serviceRQCustomerNoSAP advancedsearch failed");
			  Assert.assertTrue(false);
		  }
	}

	@Test(priority=4) 
	  public void serviceRqAdvSearchReqRef() throws InterruptedException, IOException {
		
		wbLib.waitForPageTobeLoad();
		  Thread.sleep(5000);
		  ShipmentsBasicSearchPage shipmntbasicsrch=PageFactory.initElements(TestBase.driver, ShipmentsBasicSearchPage.class);
		  CashierMakerConsumerHomePage homePage=PageFactory.initElements(TestBase.driver, CashierMakerConsumerHomePage.class);
		  ServiceRequestPage serviceRQpg=PageFactory.initElements(TestBase.driver, ServiceRequestPage.class);
		  
		  shipmntbasicsrch.getClickondropdownToNavigateShipment().click();
		  serviceRQpg.getclickonServiceRQ().click();
		  serviceRQpg.getClickMYRequestbucket().click();
		  Thread.sleep(3000);
		  homePage.getAdvanceSearchButton().click();
		  homePage.getVariabledropdown().click();
		  serviceRQpg.getSelectReqRef().click();
		  homePage.getOperatordropdown().click();
		  homePage.getOperatorequalsTo().click();
		  Thread.sleep(2000);
		  homePage.getValueinputfield().click();
		  homePage.getValueinputfield().sendKeys(prop.getProperty("serviceRQReqRef"));
		  homePage.getAddrow().click();
		  homePage.getApplyfilter().click();
		  Thread.sleep(7000);
		  
		  boolean serviceRQReqRef = driver.getPageSource().contains(prop.getProperty("serviceRQReqRef"));
		  if(serviceRQReqRef==true)
		  {
			  Assert.assertTrue(true);
			  LOGGER.info("serviceRQReqRef advancedsearch test case passed...");
			  testutil.captureScreen(driver, "serviceRQReqRef advancedsearch");
		  }
		  else {
			  LOGGER.info("serviceRQReqRef advancedsearchs test case failed...");
			  testutil.captureScreen(driver, "serviceRQReqRef advancedsearch failed");
			  Assert.assertTrue(false);
		  }
	}

	@Test(priority=5) 
	  public void serviceRqAdvSearchRepaymntAMNT() throws InterruptedException, IOException {
		
		wbLib.waitForPageTobeLoad();
		  Thread.sleep(5000);
		  ShipmentsBasicSearchPage shipmntbasicsrch=PageFactory.initElements(TestBase.driver, ShipmentsBasicSearchPage.class);
		  CashierMakerConsumerHomePage homePage=PageFactory.initElements(TestBase.driver, CashierMakerConsumerHomePage.class);
		  ServiceRequestPage serviceRQpg=PageFactory.initElements(TestBase.driver, ServiceRequestPage.class);
		  
		  shipmntbasicsrch.getClickondropdownToNavigateShipment().click();
		  serviceRQpg.getclickonServiceRQ().click();
		  serviceRQpg.getClickMYRequestbucket().click();
		  Thread.sleep(3000);
		  homePage.getAdvanceSearchButton().click();
		  homePage.getVariabledropdown().click();
		  serviceRQpg.getSelectRepayAmnt().click();
		  homePage.getOperatordropdown().click();
		  homePage.getOperatorequalsTo().click();
		  Thread.sleep(2000);
		  homePage.getValueinputfield().click();
		  homePage.getValueinputfield().sendKeys(prop.getProperty("serviceRQRepaymentAmount"));
		  homePage.getAddrow().click();
		  homePage.getApplyfilter().click();
		  Thread.sleep(7000);
		  
		  boolean serviceRQRepaymentAmount = driver.getPageSource().contains(prop.getProperty("serviceRQRepaymentAmount"));
		  if(serviceRQRepaymentAmount==true)
		  {
			  Assert.assertTrue(true);
			  LOGGER.info("serviceRQRepaymentAmount advancedsearch test case passed...");
			  testutil.captureScreen(driver, "serviceRQRepaymentAmount advancedsearch");
		  }
		  else {
			  LOGGER.info("serviceRQRepaymentAmount advancedsearchs test case failed...");
			  testutil.captureScreen(driver, "serviceRQRepaymentAmount advancedsearch failed");
			  Assert.assertTrue(false);
		  }
	}

	@Test(priority=6) 
	  public void serviceRqAdvSearchTrancCurr() throws InterruptedException, IOException {
		
		wbLib.waitForPageTobeLoad();
		  Thread.sleep(5000);
		  ShipmentsBasicSearchPage shipmntbasicsrch=PageFactory.initElements(TestBase.driver, ShipmentsBasicSearchPage.class);
		  CashierMakerConsumerHomePage homePage=PageFactory.initElements(TestBase.driver, CashierMakerConsumerHomePage.class);
		  ServiceRequestPage serviceRQpg=PageFactory.initElements(TestBase.driver, ServiceRequestPage.class);
		  
		  shipmntbasicsrch.getClickondropdownToNavigateShipment().click();
		  serviceRQpg.getclickonServiceRQ().click();
		  serviceRQpg.getClickMYRequestbucket().click();
		  Thread.sleep(3000);
		  homePage.getAdvanceSearchButton().click();
		  homePage.getVariabledropdown().click();
		  serviceRQpg.getSelectTransCurr().click();
		  homePage.getOperatordropdown().click();
		  homePage.getOperatorequalsTo().click();
		  Thread.sleep(2000);
		  homePage.getValueinputfield().click();
		  homePage.getValueinputfield().sendKeys(prop.getProperty("CurrencyCode"));
		  homePage.getAddrow().click();
		  homePage.getApplyfilter().click();
		  Thread.sleep(7000);
		  
		  boolean serviceTrancCurr = driver.getPageSource().contains(prop.getProperty("CurrencyCode"));
		  if(serviceTrancCurr==true)
		  {
			  Assert.assertTrue(true);
			  LOGGER.info("serviceTrancCurr advancedsearch test case passed...");
			  testutil.captureScreen(driver, "serviceTrancCurr advancedsearch");
		  }
		  else {
			  LOGGER.info("serviceTrancCurr advancedsearchs test case failed...");
			  testutil.captureScreen(driver, "serviceTrancCurr advancedsearch failed");
			  Assert.assertTrue(false);
		  }
	}

	@Test(priority=7) 
	  public void serviceRqAdvSearchShipmntValue() throws InterruptedException, IOException {
		
		wbLib.waitForPageTobeLoad();
		  Thread.sleep(5000);
		  ShipmentsBasicSearchPage shipmntbasicsrch=PageFactory.initElements(TestBase.driver, ShipmentsBasicSearchPage.class);
		  CashierMakerConsumerHomePage homePage=PageFactory.initElements(TestBase.driver, CashierMakerConsumerHomePage.class);
		  ServiceRequestPage serviceRQpg=PageFactory.initElements(TestBase.driver, ServiceRequestPage.class);
		  
		  shipmntbasicsrch.getClickondropdownToNavigateShipment().click();
		  serviceRQpg.getclickonServiceRQ().click();
		  serviceRQpg.getClickMYRequestbucket().click();
		  Thread.sleep(3000);
		  homePage.getAdvanceSearchButton().click();
		  homePage.getVariabledropdown().click();
		  serviceRQpg.getSelectShipVal().click();
		  homePage.getOperatordropdown().click();
		  homePage.getOperatorequalsTo().click();
		  Thread.sleep(2000);
		  homePage.getValueinputfield().click();
		  homePage.getValueinputfield().sendKeys(prop.getProperty("servcRQShipVal"));
		  homePage.getAddrow().click();
		  homePage.getApplyfilter().click();
		  Thread.sleep(7000);
		  
		  boolean servcRQShipVal = driver.getPageSource().contains(prop.getProperty("servcRQShipVal"));
		  if(servcRQShipVal==true)
		  {
			  Assert.assertTrue(true);
			  LOGGER.info("servcRQShipVal advancedsearch test case passed...");
			  testutil.captureScreen(driver, "servcRQShipVal advancedsearch");
		  }
		  else {
			  LOGGER.info("servcRQShipVal advancedsearchs test case failed...");
			  testutil.captureScreen(driver, "servcRQShipVal advancedsearch failed");
			  Assert.assertTrue(false);
		  }
	}

	@Test(priority=8) 
	  public void serviceRqAdvSearchAmntToPaid() throws InterruptedException, IOException {
		
		wbLib.waitForPageTobeLoad();
		  Thread.sleep(5000);
		  ShipmentsBasicSearchPage shipmntbasicsrch=PageFactory.initElements(TestBase.driver, ShipmentsBasicSearchPage.class);
		  CashierMakerConsumerHomePage homePage=PageFactory.initElements(TestBase.driver, CashierMakerConsumerHomePage.class);
		  ServiceRequestPage serviceRQpg=PageFactory.initElements(TestBase.driver, ServiceRequestPage.class);
		  
		  shipmntbasicsrch.getClickondropdownToNavigateShipment().click();
		  serviceRQpg.getclickonServiceRQ().click();
		  serviceRQpg.getClickMYRequestbucket().click();
		  Thread.sleep(3000);
		  homePage.getAdvanceSearchButton().click();
		  homePage.getVariabledropdown().click();
		  serviceRQpg.getSelectAmntToPaid().click();
		  homePage.getOperatordropdown().click();
		  homePage.getOperatorequalsTo().click();
		  Thread.sleep(2000);
		  homePage.getValueinputfield().click();
		  homePage.getValueinputfield().sendKeys(prop.getProperty("ServcReqAmntToPaid"));
		  homePage.getAddrow().click();
		  homePage.getApplyfilter().click();
		  Thread.sleep(7000);
		  
		  boolean ServcReqAmntToPaid = driver.getPageSource().contains(prop.getProperty("ServcReqAmntToPaid"));
		  if(ServcReqAmntToPaid==true)
		  {
			  Assert.assertTrue(true);
			  LOGGER.info("ServcReqAmntToPaid advancedsearch test case passed...");
			  testutil.captureScreen(driver, "ServcReqAmntToPaid advancedsearch");
		  }
		  else {
			  LOGGER.info("ServcReqAmntToPaid advancedsearchs test case failed...");
			  testutil.captureScreen(driver, "ServcReqAmntToPaid advancedsearch failed");
			  Assert.assertTrue(false);
		  }
	}

	@Test(priority=9) 
	  public void serviceRqAdvSearchTotlPaidAmnt() throws InterruptedException, IOException {
		
		wbLib.waitForPageTobeLoad();
		  Thread.sleep(5000);
		  ShipmentsBasicSearchPage shipmntbasicsrch=PageFactory.initElements(TestBase.driver, ShipmentsBasicSearchPage.class);
		  CashierMakerConsumerHomePage homePage=PageFactory.initElements(TestBase.driver, CashierMakerConsumerHomePage.class);
		  ServiceRequestPage serviceRQpg=PageFactory.initElements(TestBase.driver, ServiceRequestPage.class);
		  
		  shipmntbasicsrch.getClickondropdownToNavigateShipment().click();
		  serviceRQpg.getclickonServiceRQ().click();
		  serviceRQpg.getClickMYRequestbucket().click();
		  Thread.sleep(3000);
		  homePage.getAdvanceSearchButton().click();
		  homePage.getVariabledropdown().click();
		  serviceRQpg.getSelectTotlAmntpaid().click();
		  homePage.getOperatordropdown().click();
		  homePage.getOperatorequalsTo().click();
		  Thread.sleep(2000);
		  homePage.getValueinputfield().click();
		  homePage.getValueinputfield().sendKeys(prop.getProperty("ServcReqtotPaidAmnt"));
		  homePage.getAddrow().click();
		  homePage.getApplyfilter().click();
		  Thread.sleep(7000);
		  
		  boolean ServcReqtotPaidAmnt = driver.getPageSource().contains(prop.getProperty("ServcReqtotPaidAmnt"));
		  if(ServcReqtotPaidAmnt==true)
		  {
			  Assert.assertTrue(true);
			  LOGGER.info("ServcReqtotPaidAmnt advancedsearch test case passed...");
			  testutil.captureScreen(driver, "ServcReqtotPaidAmnt advancedsearch");
		  }
		  else {
			  LOGGER.info("ServcReqtotPaidAmnt advancedsearchs test case failed...");
			  testutil.captureScreen(driver, "ServcReqtotPaidAmnt advancedsearch failed");
			  Assert.assertTrue(false);
		  }
	}
	
	@Test(priority=10) 
	  public void serviceRqAdvSearchShipNo() throws InterruptedException, IOException {
		
		wbLib.waitForPageTobeLoad();
		  Thread.sleep(5000);
		  ShipmentsBasicSearchPage shipmntbasicsrch=PageFactory.initElements(TestBase.driver, ShipmentsBasicSearchPage.class);
		  CashierMakerConsumerHomePage homePage=PageFactory.initElements(TestBase.driver, CashierMakerConsumerHomePage.class);
		  ServiceRequestPage serviceRQpg=PageFactory.initElements(TestBase.driver, ServiceRequestPage.class);
		  
		  shipmntbasicsrch.getClickondropdownToNavigateShipment().click();
		  serviceRQpg.getclickonServiceRQ().click();
		  serviceRQpg.getClickMYRequestbucket().click();
		  Thread.sleep(3000);
		  homePage.getAdvanceSearchButton().click();
		  homePage.getVariabledropdown().click();
		  shipmntbasicsrch.getSelectShipmentNumber().click();
		  homePage.getOperatordropdown().click();
		  homePage.getOperatorequalsTo().click();
		  Thread.sleep(2000);
		  homePage.getValueinputfield().click();
		  homePage.getValueinputfield().sendKeys(prop.getProperty("serviceRQShipmntNo"));
		  homePage.getAddrow().click();
		  homePage.getApplyfilter().click();
		  Thread.sleep(7000);
		  
		  boolean serviceRQShipmntNo = driver.getPageSource().contains(prop.getProperty("serviceRQShipmntNo"));
		  if(serviceRQShipmntNo==true)
		  {
			  Assert.assertTrue(true);
			  LOGGER.info("serviceRQShipmntNo advancedsearch test case passed...");
			  testutil.captureScreen(driver, "serviceRQShipmntNo advancedsearch");
		  }
		  else {
			  LOGGER.info("serviceRQShipmntNo advancedsearchs test case failed...");
			  testutil.captureScreen(driver, "serviceRQShipmntNo advancedsearch failed");
			  Assert.assertTrue(false);
		  }
	}

	@Test(priority=11) 
	  public void serviceRqAdvSearchOrdNo() throws InterruptedException, IOException {
		
		wbLib.waitForPageTobeLoad();
		  Thread.sleep(5000);
		  ShipmentsBasicSearchPage shipmntbasicsrch=PageFactory.initElements(TestBase.driver, ShipmentsBasicSearchPage.class);
		  CashierMakerConsumerHomePage homePage=PageFactory.initElements(TestBase.driver, CashierMakerConsumerHomePage.class);
		  ServiceRequestPage serviceRQpg=PageFactory.initElements(TestBase.driver, ServiceRequestPage.class);
		  
		  shipmntbasicsrch.getClickondropdownToNavigateShipment().click();
		  serviceRQpg.getclickonServiceRQ().click();
		  serviceRQpg.getClickMYRequestbucket().click();
		  Thread.sleep(3000);
		  homePage.getAdvanceSearchButton().click();
		  homePage.getVariabledropdown().click();
		  shipmntbasicsrch.getSelectOrderNumber().click();
		  homePage.getOperatordropdown().click();
		  homePage.getOperatorequalsTo().click();
		  Thread.sleep(2000);
		  homePage.getValueinputfield().click();
		  homePage.getValueinputfield().sendKeys(prop.getProperty("serviceRQOrdNo"));
		  homePage.getAddrow().click();
		  homePage.getApplyfilter().click();
		  Thread.sleep(7000);
		  
		  boolean serviceRQOrdNo = driver.getPageSource().contains(prop.getProperty("serviceRQOrdNo"));
		  if(serviceRQOrdNo==true)
		  {
			  Assert.assertTrue(true);
			  LOGGER.info("serviceRQOrdNo advancedsearch test case passed...");
			  testutil.captureScreen(driver, "serviceRQOrdNo advancedsearch");
		  }
		  else {
			  LOGGER.info("serviceRQOrdNo advancedsearchs test case failed...");
			  testutil.captureScreen(driver, "serviceRQOrdNo advancedsearch failed");
			  Assert.assertTrue(false);
		  }
	}

	@Test(priority=12) 
	  public void serviceRqAdvSearchLoanStatus() throws InterruptedException, IOException {
		
		wbLib.waitForPageTobeLoad();
		  Thread.sleep(5000);
		  ShipmentsBasicSearchPage shipmntbasicsrch=PageFactory.initElements(TestBase.driver, ShipmentsBasicSearchPage.class);
		  CashierMakerConsumerHomePage homePage=PageFactory.initElements(TestBase.driver, CashierMakerConsumerHomePage.class);
		  ServiceRequestPage serviceRQpg=PageFactory.initElements(TestBase.driver, ServiceRequestPage.class);
		  
		  shipmntbasicsrch.getClickondropdownToNavigateShipment().click();
		  serviceRQpg.getclickonServiceRQ().click();
		  serviceRQpg.getClickMYRequestbucket().click();
		  Thread.sleep(3000);
		  homePage.getAdvanceSearchButton().click();
		  homePage.getVariabledropdown().click();
		  shipmntbasicsrch.getSelectLoanStatus().click();
		  homePage.getOperatordropdown().click();
		  homePage.getOperatorequalsTo().click();
		  Thread.sleep(2000);
		  homePage.getValueinputfield().click();
		  homePage.getValueinputfield().sendKeys(prop.getProperty("serviceRQLoanStus"));
		  homePage.getAddrow().click();
		  homePage.getApplyfilter().click();
		  Thread.sleep(7000);
		  
		  boolean serviceRQLoanStus = driver.getPageSource().contains(prop.getProperty("serviceRQLoanStus"));
		  if(serviceRQLoanStus==true)
		  {
			  Assert.assertTrue(true);
			  LOGGER.info("serviceRQLoanStus advancedsearch test case passed...");
			  testutil.captureScreen(driver, "serviceRQLoanStus advancedsearch");
		  }
		  else {
			  LOGGER.info("serviceRQLoanStus advancedsearchs test case failed...");
			  testutil.captureScreen(driver, "serviceRQLoanStus advancedsearch failed");
			  Assert.assertTrue(false);
		  }
	}
	@AfterMethod
	public void tearDown(){;
		driver.quit();
	}

}
