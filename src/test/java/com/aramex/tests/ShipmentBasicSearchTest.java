package com.aramex.tests;

import java.io.IOException;
import java.util.logging.Logger;

import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.aramex.base.TestBase;
import com.aramex.pages.AdvanceSearchOperatorPage;
import com.aramex.pages.CashierMakerConsumerHomePage;
import com.aramex.pages.LoginPage;
import com.aramex.pages.ShipmentsBasicSearchPage;
import com.aramex.util.TestUtil;
import com.aramex.util.WebDriverLib;

public class ShipmentBasicSearchTest extends TestBase{

	WebDriverLib wbLib=new WebDriverLib();
	Logger LOGGER = Logger.getLogger(ShipmentBasicSearchTest.class.getName());
	LoginTest logintest = new LoginTest();
	LoginPage devHomePage=PageFactory.initElements(TestBase.driver, LoginPage.class);
	AdvanceSearchOperatorPage operatorpg=PageFactory.initElements(TestBase.driver, AdvanceSearchOperatorPage.class);
	  CashierMakerConsumerHomePage homePage=PageFactory.initElements(TestBase.driver, CashierMakerConsumerHomePage.class);
	TestUtil testutil =  new TestUtil();

	
	@BeforeMethod
	public void logintoAramexAppliction() throws InterruptedException {
		logintest.loginToAppAsTester(prop.getProperty("lmsusername"), prop.getProperty("lmspassword"));
		
	}
	
	@Test(priority=3) 
	  public void shipmentbasicSearchMobileNumber() throws IOException, InterruptedException {
		wbLib.waitForPageTobeLoad();
		  Thread.sleep(5000);
		  ShipmentsBasicSearchPage shipmntbasicsrch=PageFactory.initElements(TestBase.driver, ShipmentsBasicSearchPage.class);
		  CashierMakerConsumerHomePage homePage=PageFactory.initElements(TestBase.driver, CashierMakerConsumerHomePage.class);
		  shipmntbasicsrch.getClickondropdownToNavigateShipment().click();
		  shipmntbasicsrch.getClickonshipmentmodule().click();
		  Thread.sleep(5000);
		  homePage.getFilterBy().click();
		  homePage.getSelectMobileNumber().click();
		  homePage.getBasicSearchInputField().click();
		  homePage.getBasicSearchInputField().sendKeys(prop.getProperty("ShipmentMobileNo"));
		  homePage.getClickonSearchoption().click();
		 
		  Thread.sleep(5000);       
		  boolean shipmntmobileno = driver.getPageSource().contains(prop.getProperty("ShipmentMobileNo"));
		  if(shipmntmobileno==true)
		  {
			  Assert.assertTrue(true);
			  LOGGER.info("ShipmentMobileNo basicsearch test case passed...");
			  testutil.captureScreen(driver, "ShipmentMobileNo searchpassed");
		  }
		  else {
			  LOGGER.info("ShipmentMobileNo basic search test case failed...");
			  testutil.captureScreen(driver, "ShipmentMobileNo searchfailed");
			  Assert.assertTrue(false);
		  }

		  homePage.getClearfilter().click();		  
		  Thread.sleep(9000);
	}
	
	@Test(priority=2) 
	 public void shipmentbasicSearchCustomerNM() throws IOException, InterruptedException {
			wbLib.waitForPageTobeLoad();
			  Thread.sleep(5000);
			  ShipmentsBasicSearchPage shipmntbasicsrch=PageFactory.initElements(TestBase.driver, ShipmentsBasicSearchPage.class);
			  CashierMakerConsumerHomePage homePage=PageFactory.initElements(TestBase.driver, CashierMakerConsumerHomePage.class);
			  shipmntbasicsrch.getClickondropdownToNavigateShipment().click();
			  shipmntbasicsrch.getClickonshipmentmodule().click();
			  Thread.sleep(5000);
			  homePage.getFilterBy().click();
			  shipmntbasicsrch.getSelectCustometName().click();
			  homePage.getBasicSearchInputField().click();
			  homePage.getBasicSearchInputField().sendKeys(prop.getProperty("CustomerNM"));
			  homePage.getClickonSearchoption().click();
			 
			  Thread.sleep(5000);       
			  boolean shipmntCustomerNM = driver.getPageSource().contains(prop.getProperty("CustomerNM"));
			  if(shipmntCustomerNM==true)
			  {
				  Assert.assertTrue(true);
				  LOGGER.info("ShipmentCustomerNM basicsearch test case passed...");
				  testutil.captureScreen(driver, "Shipment CustomerNM searchpassed");
			  }
			  else {
				  LOGGER.info("ShipmentCustomerNM basic search test case failed...");
				  testutil.captureScreen(driver, "Shipment CustomerNM searchfailed");
				  Assert.assertTrue(false);
			  }

			  homePage.getClearfilter().click();		  
			  Thread.sleep(9000);
		}
	
	@Test(priority=4) 
	 public void shipmentbasicSearchShipmentNo() throws IOException, InterruptedException {
			wbLib.waitForPageTobeLoad();
			  Thread.sleep(5000);
			  ShipmentsBasicSearchPage shipmntbasicsrch=PageFactory.initElements(TestBase.driver, ShipmentsBasicSearchPage.class);
			  CashierMakerConsumerHomePage homePage=PageFactory.initElements(TestBase.driver, CashierMakerConsumerHomePage.class);
			  shipmntbasicsrch.getClickondropdownToNavigateShipment().click();
			  shipmntbasicsrch.getClickonshipmentmodule().click();
			  Thread.sleep(5000);
			  homePage.getFilterBy().click();
			  shipmntbasicsrch.getSelectShipmentNumber().click();
			  homePage.getBasicSearchInputField().click();
			  homePage.getBasicSearchInputField().sendKeys(prop.getProperty("ShipmentNo"));
			  homePage.getClickonSearchoption().click();
			 
			  Thread.sleep(5000);       
			  boolean ShipmentNo = driver.getPageSource().contains(prop.getProperty("ShipmentNo"));
			  if(ShipmentNo==true)
			  {
				  Assert.assertTrue(true);
				  LOGGER.info("ShipmentNo basicsearch test case passed...");
				  testutil.captureScreen(driver, "ShipmentNo searchpassed");
			  }
			  else {
				  LOGGER.info("ShipmentNo basic search test case failed...");
				  testutil.captureScreen(driver, "ShipmentNo searchfailed");
				  Assert.assertTrue(false);
			  }

			  homePage.getClearfilter().click();		  
			  Thread.sleep(9000);
		}
	
	@Test(priority=5) 
	 public void shipmentbasicSearchOrderNo() throws IOException, InterruptedException {
			wbLib.waitForPageTobeLoad();
			  Thread.sleep(5000);
			  ShipmentsBasicSearchPage shipmntbasicsrch=PageFactory.initElements(TestBase.driver, ShipmentsBasicSearchPage.class);
			  CashierMakerConsumerHomePage homePage=PageFactory.initElements(TestBase.driver, CashierMakerConsumerHomePage.class);
			  shipmntbasicsrch.getClickondropdownToNavigateShipment().click();
			  shipmntbasicsrch.getClickonshipmentmodule().click();
			  Thread.sleep(5000);
			  homePage.getFilterBy().click();
			  shipmntbasicsrch.getSelectOrderNumber().click();
			  homePage.getBasicSearchInputField().click();
			  homePage.getBasicSearchInputField().sendKeys(prop.getProperty("OrderNo"));
			  homePage.getClickonSearchoption().click();
			 
			  Thread.sleep(5000);       
			  boolean OrderNo = driver.getPageSource().contains(prop.getProperty("OrderNo"));
			  if(OrderNo==true)
			  {
				  Assert.assertTrue(true);
				  LOGGER.info("OrderNo basicsearch test case passed...");
				  testutil.captureScreen(driver, "OrderNo searchpassed");
			  }
			  else {
				  LOGGER.info("OrderNo basic search test case failed...");
				  testutil.captureScreen(driver, "OrderNo searchfailed");
				  Assert.assertTrue(false);
			  }

			  homePage.getClearfilter().click();		  
			  Thread.sleep(9000);
		}

	@Test(priority=6) 
	 public void shipmentbasicSearchEtailer() throws IOException, InterruptedException {
			wbLib.waitForPageTobeLoad();
			  Thread.sleep(5000);
			  ShipmentsBasicSearchPage shipmntbasicsrch=PageFactory.initElements(TestBase.driver, ShipmentsBasicSearchPage.class);
			  CashierMakerConsumerHomePage homePage=PageFactory.initElements(TestBase.driver, CashierMakerConsumerHomePage.class);
			  shipmntbasicsrch.getClickondropdownToNavigateShipment().click();
			  shipmntbasicsrch.getClickonshipmentmodule().click();
			  Thread.sleep(5000);
			  homePage.getFilterBy().click();
			  shipmntbasicsrch.getSelectEtailer().click();
			  homePage.getBasicSearchInputField().click();
			  homePage.getBasicSearchInputField().sendKeys(prop.getProperty("Etailer"));
			  homePage.getClickonSearchoption().click();
			 
			  Thread.sleep(5000);       
			  boolean Etailer = driver.getPageSource().contains(prop.getProperty("Etailer"));
			  if(Etailer==true)
			  {
				  Assert.assertTrue(true);
				  LOGGER.info("Etailer basicsearch test case passed...");
				  testutil.captureScreen(driver, "Etailer searchpassed");
			  }
			  else {
				  LOGGER.info("Etailer basic search test case failed...");
				  testutil.captureScreen(driver, "Etailer searchfailed");
				  Assert.assertTrue(false);
			  }

			  homePage.getClearfilter().click();		  
			  Thread.sleep(9000);
		}
	
	@Test(priority=7) 
	 public void shipmentbasicSearchOrigin() throws IOException, InterruptedException {
			wbLib.waitForPageTobeLoad();
			  Thread.sleep(5000);
			  ShipmentsBasicSearchPage shipmntbasicsrch=PageFactory.initElements(TestBase.driver, ShipmentsBasicSearchPage.class);
			  CashierMakerConsumerHomePage homePage=PageFactory.initElements(TestBase.driver, CashierMakerConsumerHomePage.class);
			  shipmntbasicsrch.getClickondropdownToNavigateShipment().click();
			  shipmntbasicsrch.getClickonshipmentmodule().click();
			  Thread.sleep(5000);
			  homePage.getFilterBy().click();
			  shipmntbasicsrch.getSelectorigin().click();
			  homePage.getBasicSearchInputField().click();
			  homePage.getBasicSearchInputField().sendKeys(prop.getProperty("Origin"));
			  homePage.getClickonSearchoption().click();
			 
			  Thread.sleep(5000);       
			  boolean Origin = driver.getPageSource().contains(prop.getProperty("Origin"));
			  if(Origin==true)
			  {
				  Assert.assertTrue(true);
				  LOGGER.info("Origin basicsearch test case passed...");
				  testutil.captureScreen(driver, "Origin searchpassed");
			  }
			  else {
				  LOGGER.info("Origin basic search test case failed...");
				  testutil.captureScreen(driver, "Origin searchfailed");
				  Assert.assertTrue(false);
			  }

			  homePage.getClearfilter().click();		  
			  Thread.sleep(9000);
		}
	
	@Test(priority=8) 
	 public void shipmentbasicSearchDestination() throws IOException, InterruptedException {
			wbLib.waitForPageTobeLoad();
			  Thread.sleep(5000);
			  ShipmentsBasicSearchPage shipmntbasicsrch=PageFactory.initElements(TestBase.driver, ShipmentsBasicSearchPage.class);
			  CashierMakerConsumerHomePage homePage=PageFactory.initElements(TestBase.driver, CashierMakerConsumerHomePage.class);
			  shipmntbasicsrch.getClickondropdownToNavigateShipment().click();
			  shipmntbasicsrch.getClickonshipmentmodule().click();
			  Thread.sleep(5000);
			  homePage.getFilterBy().click();
			  shipmntbasicsrch.getSelectDestination().click();
			  homePage.getBasicSearchInputField().click();
			  homePage.getBasicSearchInputField().sendKeys(prop.getProperty("Destination"));
			  homePage.getClickonSearchoption().click();
			 
			  Thread.sleep(5000);       
			  boolean Destination = driver.getPageSource().contains(prop.getProperty("Destination"));
			  if(Destination==true)
			  {
				  Assert.assertTrue(true);
				  LOGGER.info("Destination basicsearch test case passed...");
				  testutil.captureScreen(driver, "Destination searchpassed");
			  }
			  else {
				  LOGGER.info("Destination basic search test case failed...");
				  testutil.captureScreen(driver, "Destination searchfailed");
				  Assert.assertTrue(false);
			  }

			  homePage.getClearfilter().click();		  
			  Thread.sleep(9000);
		}
	
	@Test(priority=9) 
	 public void shipmentbasicSearchCurrencyCode() throws IOException, InterruptedException {
			wbLib.waitForPageTobeLoad();
			  Thread.sleep(5000);
			  ShipmentsBasicSearchPage shipmntbasicsrch=PageFactory.initElements(TestBase.driver, ShipmentsBasicSearchPage.class);
			  CashierMakerConsumerHomePage homePage=PageFactory.initElements(TestBase.driver, CashierMakerConsumerHomePage.class);
			  shipmntbasicsrch.getClickondropdownToNavigateShipment().click();
			  shipmntbasicsrch.getClickonshipmentmodule().click();
			  Thread.sleep(5000);
			  homePage.getFilterBy().click();
			  shipmntbasicsrch.getSelectCurrencyCode().click();
			  homePage.getBasicSearchInputField().click();
			  homePage.getBasicSearchInputField().sendKeys(prop.getProperty("CurrencyCode"));
			  homePage.getClickonSearchoption().click();
			 
			  Thread.sleep(5000);       
			  boolean CurrencyCode = driver.getPageSource().contains(prop.getProperty("CurrencyCode"));
			  if(CurrencyCode==true)
			  {
				  Assert.assertTrue(true);
				  LOGGER.info("CurrencyCode basicsearch test case passed...");
				  testutil.captureScreen(driver, "CurrencyCode searchpassed");
			  }
			  else {
				  LOGGER.info("CurrencyCode basic search test case failed...");
				  testutil.captureScreen(driver, "CurrencyCode searchfailed");
				  Assert.assertTrue(false);
			  }

			  homePage.getClearfilter().click();		  
			  Thread.sleep(9000);
		}

	@Test(priority=10) 
	 public void shipmentbasicSearchStatus() throws IOException, InterruptedException {
			wbLib.waitForPageTobeLoad();
			  Thread.sleep(5000);
			  ShipmentsBasicSearchPage shipmntbasicsrch=PageFactory.initElements(TestBase.driver, ShipmentsBasicSearchPage.class);
			  CashierMakerConsumerHomePage homePage=PageFactory.initElements(TestBase.driver, CashierMakerConsumerHomePage.class);
			  shipmntbasicsrch.getClickondropdownToNavigateShipment().click();
			  shipmntbasicsrch.getClickonshipmentmodule().click();
			  Thread.sleep(5000);
			  homePage.getFilterBy().click();
			  shipmntbasicsrch.getSelectstatus().click();
			  homePage.getBasicSearchInputField().click();
			  homePage.getBasicSearchInputField().sendKeys(prop.getProperty("Status"));
			  homePage.getClickonSearchoption().click();
			 
			  Thread.sleep(5000);       
			  boolean Status = driver.getPageSource().contains(prop.getProperty("Status"));
			  if(Status==true)
			  {
				  Assert.assertTrue(true);
				  LOGGER.info("Status basicsearch test case passed...");
				  testutil.captureScreen(driver, "Status searchpassed");
			  }
			  else {
				  LOGGER.info("Status basic search test case failed...");
				  testutil.captureScreen(driver, "Status searchfailed");
				  Assert.assertTrue(false);
			  }

			  homePage.getClearfilter().click();		  
			  Thread.sleep(9000);
		}

	@Test(priority=1) 
	 public void shipmentbasicSearchLoanStatus() throws IOException, InterruptedException {
			wbLib.waitForPageTobeLoad();
			  Thread.sleep(5000);
			  ShipmentsBasicSearchPage shipmntbasicsrch=PageFactory.initElements(TestBase.driver, ShipmentsBasicSearchPage.class);
			  CashierMakerConsumerHomePage homePage=PageFactory.initElements(TestBase.driver, CashierMakerConsumerHomePage.class);
			  shipmntbasicsrch.getClickondropdownToNavigateShipment().click();
			  shipmntbasicsrch.getClickonshipmentmodule().click();
			  Thread.sleep(5000);
			  homePage.getFilterBy().click();
			  shipmntbasicsrch.getSelectLoanStatus().click();
			  homePage.getBasicSearchInputField().click();
			  homePage.getBasicSearchInputField().sendKeys(prop.getProperty("LoanStatus"));
			  homePage.getClickonSearchoption().click();
			 
			  Thread.sleep(5000);       
			  boolean LoanStatus = driver.getPageSource().contains(prop.getProperty("LoanStatus"));
			  if(LoanStatus==true)
			  {
				  Assert.assertTrue(true);
				  LOGGER.info("LoanStatus basicsearch test case passed...");
				  testutil.captureScreen(driver, "LoanStatus searchpassed");
			  }
			  else {
				  LOGGER.info("LoanStatus basic search test case failed...");
				  testutil.captureScreen(driver, "LoanStatus searchfailed");
				  Assert.assertTrue(false);
			  }

			  homePage.getClearfilter().click();		  
			  Thread.sleep(9000);
		}
	
	@AfterMethod
	public void tearDown(){;
		driver.quit();
	}

		

}
