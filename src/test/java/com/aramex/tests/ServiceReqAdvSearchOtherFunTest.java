package com.aramex.tests;

import java.io.IOException;
import java.util.logging.Logger;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import com.aramex.base.TestBase;
import com.aramex.pages.CashierMakerConsumerHomePage;
import com.aramex.pages.ServiceRequestPage;
import com.aramex.pages.ShipmentsBasicSearchPage;
import com.aramex.util.TestUtil;
import com.aramex.util.WebDriverLib;

public class ServiceReqAdvSearchOtherFunTest extends TestBase{

	WebDriverLib wbLib=new WebDriverLib();
	Logger LOGGER = Logger.getLogger(ShipmentAdvanceSearchOtherFunTest.class.getName());
	LoginTest logintest = new LoginTest();
	TestUtil testutil =  new TestUtil();

	
	@BeforeMethod
	public void logintoAramexAppliction() throws InterruptedException {
		logintest.loginToAppAsTester(prop.getProperty("lmsusername"), prop.getProperty("lmspassword"));
		
	}

	@Test(priority=1) 
	  public void serviceReqadvanceSearchAddMulipleRow() throws InterruptedException, IOException {
		
		wbLib.waitForPageTobeLoad();
		  Thread.sleep(4000);
		  ShipmentsBasicSearchPage shipmntbasicsrch=PageFactory.initElements(TestBase.driver, ShipmentsBasicSearchPage.class);
		  CashierMakerConsumerHomePage homePage=PageFactory.initElements(TestBase.driver, CashierMakerConsumerHomePage.class);
		  ServiceRequestPage serviceRQpg=PageFactory.initElements(TestBase.driver, ServiceRequestPage.class);
		  
		  shipmntbasicsrch.getClickondropdownToNavigateShipment().click();
		  serviceRQpg.getclickonServiceRQ().click();
		  serviceRQpg.getClickMYRequestbucket().click();
		  Thread.sleep(4000);
		  homePage.getAdvanceSearchButton().click();
		  homePage.getVariabledropdown().click();
		  homePage.getSelectMobileNumber().click();
		  homePage.getOperatordropdown().click();
		  homePage.getOperatorequalsTo().click();
		  Thread.sleep(2000);
		  homePage.getValueinputfield().sendKeys(prop.getProperty("serviceRQMobNo"));
		  homePage.getAddrow().click();
		  testutil.captureScreen(driver, "serviceRQMobNo 1st row");
		  
		  //add2nd row
		  Thread.sleep(2000);
		  homePage.getVariabledropdown().click();
		  shipmntbasicsrch.getSelectCustometName().click();
		  homePage.getOperatordropdown().click();
		  homePage.getOperatorequalsTo().click();
		  Thread.sleep(2000);
		  homePage.getValueinputfield().sendKeys(prop.getProperty("serviceRQCustNM2"));
		  homePage.getAddrow().click();
		  testutil.captureScreen(driver, "OrderserviceRQCustNM Noadd 2nd row");
		  
		  homePage.getApplyfilter().click();
		  Thread.sleep(2000);

		
	}
	
	@Test(priority=2) 
	  public void serviceRqadvanceSearchDeleteRow() throws InterruptedException, IOException {
		
		wbLib.waitForPageTobeLoad();
		  Thread.sleep(4000);
		  ShipmentsBasicSearchPage shipmntbasicsrch=PageFactory.initElements(TestBase.driver, ShipmentsBasicSearchPage.class);
		  CashierMakerConsumerHomePage homePage=PageFactory.initElements(TestBase.driver, CashierMakerConsumerHomePage.class);
		  ServiceRequestPage serviceRQpg=PageFactory.initElements(TestBase.driver, ServiceRequestPage.class);
		  
		  shipmntbasicsrch.getClickondropdownToNavigateShipment().click();
		  serviceRQpg.getclickonServiceRQ().click();
		  serviceRQpg.getClickMYRequestbucket().click();
		  Thread.sleep(4000);
		  homePage.getAdvanceSearchButton().click();
		  homePage.getVariabledropdown().click();
		  homePage.getSelectMobileNumber().click();
		  homePage.getOperatordropdown().click();
		  homePage.getOperatorequalsTo().click();
		  Thread.sleep(2000);
		  homePage.getValueinputfield().sendKeys(prop.getProperty("serviceRQMobNo"));
		  homePage.getAddrow().click();
		  
		  //add2nd row
		  Thread.sleep(2000);
		  homePage.getVariabledropdown().click();
		  shipmntbasicsrch.getSelectCustometName().click();
		  homePage.getOperatordropdown().click();
		  homePage.getOperatorequalsTo().click();
		  Thread.sleep(2000);
		  homePage.getValueinputfield().sendKeys(prop.getProperty("serviceRQCustNM2"));
		  homePage.getAddrow().click();
		  homePage.getDeleteRow().click();
		  Thread.sleep(3000);
		  testutil.captureScreen(driver, "serviceRQCustNMdelete Row");
		  homePage.getApplyfilter().click();
		  Thread.sleep(1000);
		
	}
		@Test(priority=3) 
	  public void serviceRqAdvsrchwithCaseSensitiveNegtvtest() throws InterruptedException, IOException {
		
		wbLib.waitForPageTobeLoad();
		  Thread.sleep(4000);
		  ShipmentsBasicSearchPage shipmntbasicsrch=PageFactory.initElements(TestBase.driver, ShipmentsBasicSearchPage.class);
		  CashierMakerConsumerHomePage homePage=PageFactory.initElements(TestBase.driver, CashierMakerConsumerHomePage.class);
		  ServiceRequestPage serviceRQpg=PageFactory.initElements(TestBase.driver, ServiceRequestPage.class);
		  
		  shipmntbasicsrch.getClickondropdownToNavigateShipment().click();
		  serviceRQpg.getclickonServiceRQ().click();
		  serviceRQpg.getClickMYRequestbucket().click();
		  Thread.sleep(4000);
		  homePage.getAdvanceSearchButton().click();
		  Thread.sleep(3000);
		  homePage.getCaseSensitivechkbox().click();
		  homePage.getVariabledropdown().click();
		  shipmntbasicsrch.getSelectCustometName().click();
		  homePage.getOperatordropdown().click();
		  homePage.getOperatorequalsTo().click();
		  Thread.sleep(2000);
		  homePage.getValueinputfield().sendKeys(prop.getProperty("serviceReqwithcasesensitivecustnm"));
		  homePage.getAddrow().click();
		  Thread.sleep(3000);
		  homePage.getApplyfilter().click();
		  LOGGER.info("should not filter");
		  Thread.sleep(3000);
		  testutil.captureScreen(driver, "SerRq casesenstvshould not filter");
	}
	@Test(priority=4) 
	  public void shipmntadvsrchwithCaseSensitivePositvetest() throws InterruptedException, IOException {
		
		wbLib.waitForPageTobeLoad();
		  Thread.sleep(5000);
		  ShipmentsBasicSearchPage shipmntbasicsrch=PageFactory.initElements(TestBase.driver, ShipmentsBasicSearchPage.class);
		  CashierMakerConsumerHomePage homePage=PageFactory.initElements(TestBase.driver, CashierMakerConsumerHomePage.class);
		  ServiceRequestPage serviceRQpg=PageFactory.initElements(TestBase.driver, ServiceRequestPage.class);
		  
		  shipmntbasicsrch.getClickondropdownToNavigateShipment().click();
		  serviceRQpg.getclickonServiceRQ().click();
		  serviceRQpg.getClickMYRequestbucket().click();
		  Thread.sleep(4000);
		  homePage.getAdvanceSearchButton().click();
		  Thread.sleep(3000);
		  homePage.getCaseSensitivechkbox().click();
		  homePage.getVariabledropdown().click();
		  shipmntbasicsrch.getSelectCustometName().click();
		  homePage.getOperatordropdown().click();
		  homePage.getOperatorequalsTo().click();
		  Thread.sleep(2000);
		  homePage.getValueinputfield().sendKeys(prop.getProperty("serviceRQCustNM2"));
		  homePage.getAddrow().click();
		  Thread.sleep(3000);
		  homePage.getApplyfilter().click();
		  LOGGER.info("should filter");
		  Thread.sleep(3000);
		  testutil.captureScreen(driver, "SerRq casesensitvshouldfilter");
	}

	@AfterMethod
	public void tearDown(){;
		driver.quit();
	}
	
}
