package com.aramex.tests;

import java.io.IOException;
import java.util.logging.Logger;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.aramex.base.TestBase;
import com.aramex.pages.CashierMakerConsumerHomePage;
import com.aramex.pages.LoginPage;
import com.aramex.util.TestUtil;
import com.aramex.util.WebDriverLib;

public class ConsumerAdvanceSearchFuncTest extends TestBase{

	WebDriverLib wbLib=new WebDriverLib();
	Logger LOGGER = Logger.getLogger(ConsumerAdvanceSearchFuncTest.class.getName());
	LoginTest logintest = new LoginTest();
	LoginPage devHomePage=PageFactory.initElements(TestBase.driver, LoginPage.class);
	CashierMakerConsumerHomePage homePage=PageFactory.initElements(TestBase.driver, CashierMakerConsumerHomePage.class);
	TestUtil testutil =  new TestUtil();

	
	@BeforeMethod
	public void logintoAramexAppliction() throws InterruptedException {
		logintest.loginToAppAsTester(prop.getProperty("lmsusername"), prop.getProperty("lmspassword"));
		
	}
	
	@Test(priority=1) 
	  public void advanceSearchMobileNumber() throws InterruptedException, IOException {
		
		wbLib.waitForPageTobeLoad();
		  Thread.sleep(5000);
		  CashierMakerConsumerHomePage homePage=PageFactory.initElements(TestBase.driver, CashierMakerConsumerHomePage.class);
		  homePage.getAdvanceSearchButton().click();
		  homePage.getVariabledropdown().click();
		  homePage.getSelectMobileNumber().click();
		  homePage.getOperatordropdown().click();
		  homePage.getOperatorequalsTo().click();
		  Thread.sleep(2000);
		  homePage.getValueinputfield().click();
		  homePage.getValueinputfield().sendKeys(prop.getProperty("SearchbyMobilenumber"));
		  homePage.getAddrow().click();
		  homePage.getApplyfilter().click();
		  Thread.sleep(7000);
		  
		  boolean mobileno = driver.getPageSource().contains(prop.getProperty("SearchbyMobilenumber"));
		  if(mobileno==true)
		  {
			  Assert.assertTrue(true);
			  LOGGER.info("mobile advancedsearch test case passed...");
			  testutil.captureScreen(driver, "mobilenumber advancedsearch");
		  }
		  else {
			  LOGGER.info("mobile advancedsearchs test case failed...");
			  testutil.captureScreen(driver, "mobilenumber advancedsearch failed");
			  Assert.assertTrue(false);
		  }
		
	}
	
	@Test(priority=2) 
	  public void advanceSearchfunctionalityConsumerNM() throws InterruptedException, InvalidFormatException, IOException {
		wbLib.waitForPageTobeLoad();
		  Thread.sleep(5000);
		  CashierMakerConsumerHomePage homePage=PageFactory.initElements(TestBase.driver, CashierMakerConsumerHomePage.class);
		  homePage.getAdvanceSearchButton().click();
		  homePage.getVariabledropdown().click();
		  homePage.getSelectConsumerName().click();
		  homePage.getOperatordropdown().click();
		  homePage.getOperatorequalsTo().click();
		  Thread.sleep(2000);
		  homePage.getValueinputfield().sendKeys(prop.getProperty("ConsumerName"));
		  homePage.getAddrow().click();
		  homePage.getApplyfilter().click();
		  Thread.sleep(7000);
	     // Assert.assertEquals(prop.getProperty("ConsumerName"), driver.findElement(By.xpath("//td[contains(text(),'')][2]")).getText());
		  boolean consumernm = driver.getPageSource().contains(prop.getProperty("ConsumerName"));
		  if(consumernm==true)
		  {
			  Assert.assertTrue(true);
			  LOGGER.info("consumername advance search test case passed...");
			  testutil.captureScreen(driver, "ConsumerName advancesearch");
		  }
		  else {
			  LOGGER.info("consumername advance search test case failed...");
			  testutil.captureScreen(driver, "ConsumerName advancesearch failed");
			  Assert.assertTrue(false);
		  }
		  Thread.sleep(5000);
	}
	
	@Test(priority=3) 
	  public void advanceSearchfunctionalityEmail() throws InterruptedException, InvalidFormatException, IOException {
		wbLib.waitForPageTobeLoad();
		  Thread.sleep(5000);
		  CashierMakerConsumerHomePage homePage=PageFactory.initElements(TestBase.driver, CashierMakerConsumerHomePage.class);
		  homePage.getAdvanceSearchButton().click();
		  homePage.getVariabledropdown().click();
		  homePage.getSelectEmail().click();
		  homePage.getOperatordropdown().click();
		  homePage.getOperatorequalsTo().click();
		  Thread.sleep(2000);
		  homePage.getValueinputfield().sendKeys(prop.getProperty("Email"));
		  homePage.getAddrow().click();
		  homePage.getApplyfilter().click();
		  Thread.sleep(7000);
	     // Assert.assertEquals(prop.getProperty("ConsumerName"), driver.findElement(By.xpath("//td[contains(text(),'')][2]")).getText());
		  boolean email = driver.getPageSource().contains(prop.getProperty("Email"));
		  if(email==true)
		  {
			  Assert.assertTrue(true);
			  LOGGER.info("email advancesearch test case passed...");
			  testutil.captureScreen(driver, "email advancesearch");
		  }
		  else {
			  LOGGER.info("email advancesearch test case failed...");
			  testutil.captureScreen(driver, "email advancesearch failed");
			  Assert.assertTrue(false);
		  }
		  Thread.sleep(5000);
	}	
	
	@Test(priority=4) 
	  public void advanceSearchfunctionalityLatestBatch() throws InterruptedException, InvalidFormatException, IOException {
		wbLib.waitForPageTobeLoad();
		  Thread.sleep(5000);
		  CashierMakerConsumerHomePage homePage=PageFactory.initElements(TestBase.driver, CashierMakerConsumerHomePage.class);
		  homePage.getAdvanceSearchButton().click();
		  homePage.getVariabledropdown().click();
		  homePage.getSelectLatestBatch().click();
		  homePage.getOperatordropdown().click();
		  homePage.getOperatorequalsTo().click();
		  Thread.sleep(2000);
		  homePage.getValueinputfield().sendKeys(prop.getProperty("LatestBatch"));
		  homePage.getAddrow().click();
		  homePage.getApplyfilter().click();
		  Thread.sleep(7000);
	     // Assert.assertEquals(prop.getProperty("ConsumerName"), driver.findElement(By.xpath("//td[contains(text(),'')][2]")).getText());
		  boolean latestbatch = driver.getPageSource().contains(prop.getProperty("LatestBatch"));
		  if(latestbatch==true)
		  {
			  Assert.assertTrue(true);
			  LOGGER.info("LatestBatch advancesearch test case passed...");
			  testutil.captureScreen(driver, "LatestBatch advancesearch");
		  }
		  else {
			  LOGGER.info("LatestBatch advancesearch test case failed...");
			  testutil.captureScreen(driver, "LatestBatch advancesearch failed");
			  Assert.assertTrue(false);
		  }
		  Thread.sleep(5000);
	}	
	
	@Test(priority=5) 
	  public void advanceSearchfunctionalityLimit() throws InterruptedException, InvalidFormatException, IOException {
		wbLib.waitForPageTobeLoad();
		  Thread.sleep(5000);
		  CashierMakerConsumerHomePage homePage=PageFactory.initElements(TestBase.driver, CashierMakerConsumerHomePage.class);
		  homePage.getAdvanceSearchButton().click();
		  homePage.getVariabledropdown().click();
		  homePage.getSelectLimit().click();
		  homePage.getOperatordropdown().click();
		  homePage.getOperatorequalsTo().click();
		  Thread.sleep(2000);
		  homePage.getValueinputfield().sendKeys(prop.getProperty("Limit"));
		  homePage.getAddrow().click();
		  homePage.getApplyfilter().click();
		  Thread.sleep(7000);
	     // Assert.assertEquals(prop.getProperty("ConsumerName"), driver.findElement(By.xpath("//td[contains(text(),'')][2]")).getText());
		  boolean limit = driver.getPageSource().contains(prop.getProperty("Limit"));
		  if(limit==true)
		  {
			  Assert.assertTrue(true);
			  LOGGER.info("Limit advancesearch test case passed...");
			  testutil.captureScreen(driver, "Limit advancesearch");
		  }
		  else {
			  LOGGER.info("Limit advancesearch test case failed...");
			  testutil.captureScreen(driver, "Limit advancesearch failed");
			  Assert.assertTrue(false);
		  }
		  Thread.sleep(5000);
	}	
	
	@Test(priority=6) 
	  public void advanceSearchfunctionalityAvailableLimit() throws InterruptedException, InvalidFormatException, IOException {
		wbLib.waitForPageTobeLoad();
		  Thread.sleep(5000);
		  CashierMakerConsumerHomePage homePage=PageFactory.initElements(TestBase.driver, CashierMakerConsumerHomePage.class);
		  homePage.getAdvanceSearchButton().click();
		  homePage.getVariabledropdown().click();
		  homePage.getSelectAvailableLimit().click();
		  homePage.getOperatordropdown().click();
		  homePage.getOperatorequalsTo().click();
		  Thread.sleep(2000);
		  homePage.getValueinputfield().sendKeys(prop.getProperty("AvailableLimit"));
		  homePage.getAddrow().click();
		  homePage.getApplyfilter().click();
		  Thread.sleep(7000);
	     // Assert.assertEquals(prop.getProperty("ConsumerName"), driver.findElement(By.xpath("//td[contains(text(),'')][2]")).getText());
		  boolean availableLimit = driver.getPageSource().contains(prop.getProperty("AvailableLimit"));
		  if(availableLimit==true)
		  {
			  Assert.assertTrue(true);
			  LOGGER.info("AvailableLimit advancesearch test case passed...");
			  testutil.captureScreen(driver, "AvailableLimit advancesearch");
		  }
		  else {
			  LOGGER.info("AvailableLimit advancesearch test case failed...");
			  testutil.captureScreen(driver, "AvailableLimit advancesearch failed");
			  Assert.assertTrue(false);
		  }
		  Thread.sleep(5000);
	}	
	
	@Test(priority=7) 
	  public void advanceSearchfunctionalityLimitCurrency() throws InterruptedException, InvalidFormatException, IOException {
		wbLib.waitForPageTobeLoad();
		  Thread.sleep(5000);
		  CashierMakerConsumerHomePage homePage=PageFactory.initElements(TestBase.driver, CashierMakerConsumerHomePage.class);
		  homePage.getAdvanceSearchButton().click();
		  homePage.getVariabledropdown().click();
		  homePage.getSelectAvailableLimit().click();
		  homePage.getOperatordropdown().click();
		  homePage.getOperatorequalsTo().click();
		  Thread.sleep(2000);
		  homePage.getValueinputfield().sendKeys(prop.getProperty("LimitCurrency"));
		  homePage.getAddrow().click();
		  homePage.getApplyfilter().click();
		  Thread.sleep(7000);
	     // Assert.assertEquals(prop.getProperty("ConsumerName"), driver.findElement(By.xpath("//td[contains(text(),'')][2]")).getText());
		  boolean limitCurrency = driver.getPageSource().contains(prop.getProperty("LimitCurrency"));
		  if(limitCurrency==true)
		  {
			  Assert.assertTrue(true);
			  LOGGER.info("LimitCurrency advancesearch test case passed...");
			  testutil.captureScreen(driver, "LimitCurrency advancesearch");
		  }
		  else {
			  LOGGER.info("LimitCurrency advancesearch test case failed...");
			  testutil.captureScreen(driver, "LimitCurrency advancesearch failed");
			  Assert.assertTrue(false);
		  }
		  Thread.sleep(5000);
	}
	
	@Test(priority=8) 
	  public void advanceSearchfunctionalityConsumerStatus() throws InterruptedException, InvalidFormatException, IOException {
		wbLib.waitForPageTobeLoad();
		  Thread.sleep(5000);
		  CashierMakerConsumerHomePage homePage=PageFactory.initElements(TestBase.driver, CashierMakerConsumerHomePage.class);
		  homePage.getAdvanceSearchButton().click();
		  homePage.getVariabledropdown().click();
		  homePage.getSelectConsumerStatus().click();
		  homePage.getOperatordropdown().click();
		  homePage.getOperatorequalsTo().click();
		  Thread.sleep(2000);
		  homePage.getValueinputfield().sendKeys(prop.getProperty("ConsumerStatus"));
		  homePage.getAddrow().click();
		  homePage.getApplyfilter().click();
		  Thread.sleep(7000);
	     // Assert.assertEquals(prop.getProperty("ConsumerName"), driver.findElement(By.xpath("//td[contains(text(),'')][2]")).getText());
		  boolean consumerStatus = driver.getPageSource().contains(prop.getProperty("ConsumerStatus"));
		  if(consumerStatus==true)
		  {
			  Assert.assertTrue(true);
			  LOGGER.info("ConsumerStatus advancesearch test case passed...");
			  testutil.captureScreen(driver, "ConsumerStatus advancesearch");
		  }
		  else {
			  LOGGER.info("ConsumerStatus advancesearch test case failed...");
			  testutil.captureScreen(driver, "ConsumerStatus advancesearch failed");
			  Assert.assertTrue(false);
		  }
		  Thread.sleep(5000);
	}
	
	@Test(priority=9) 
	  public void advanceSearchfunctionalityCreditScore() throws InterruptedException, InvalidFormatException, IOException {
		wbLib.waitForPageTobeLoad();
		  Thread.sleep(5000);
		  CashierMakerConsumerHomePage homePage=PageFactory.initElements(TestBase.driver, CashierMakerConsumerHomePage.class);
		  homePage.getAdvanceSearchButton().click();
		  homePage.getVariabledropdown().click();
		  homePage.getSelectCreditScore().click();
		  homePage.getOperatordropdown().click();
		  homePage.getOperatorequalsTo().click();
		  Thread.sleep(2000);
		  homePage.getValueinputfield().sendKeys(prop.getProperty("CreditScore"));
		  homePage.getAddrow().click();
		  homePage.getApplyfilter().click();
		  Thread.sleep(7000);
	     // Assert.assertEquals(prop.getProperty("ConsumerName"), driver.findElement(By.xpath("//td[contains(text(),'')][2]")).getText());
		  boolean creditScore = driver.getPageSource().contains(prop.getProperty("CreditScore"));
		  if(creditScore==true)
		  {
			  Assert.assertTrue(true);
			  LOGGER.info("CreditScore advancesearch test case passed...");
			  testutil.captureScreen(driver, "CreditScore advancesearch");
		  }
		  else {
			  LOGGER.info("CreditScore advancesearch test case failed...");
			  testutil.captureScreen(driver, "CreditScore advancesearch failed");
			  Assert.assertTrue(false);
		  }
		  Thread.sleep(5000);
	}
	
	@Test(priority=10) 
	  public void advanceSearchfunctionalityPropernsityScore() throws InterruptedException, InvalidFormatException, IOException {
		wbLib.waitForPageTobeLoad();
		  Thread.sleep(5000);
		  CashierMakerConsumerHomePage homePage=PageFactory.initElements(TestBase.driver, CashierMakerConsumerHomePage.class);
		  homePage.getAdvanceSearchButton().click();
		  Thread.sleep(3000);
		  homePage.getVariabledropdown().click();
		  homePage.getSelectPropensityScore().click();
		  homePage.getOperatordropdown().click();
		  homePage.getOperatorequalsTo().click();
		  Thread.sleep(2000);
		  homePage.getValueinputfield().sendKeys(prop.getProperty("PropernsityScore"));
		  homePage.getAddrow().click();
		  homePage.getApplyfilter().click();
		  Thread.sleep(7000);
	     // Assert.assertEquals(prop.getProperty("ConsumerName"), driver.findElement(By.xpath("//td[contains(text(),'')][2]")).getText());
		  boolean propernsityScore = driver.getPageSource().contains(prop.getProperty("PropernsityScore"));
		  if(propernsityScore==true)
		  {
			  Assert.assertTrue(true);
			  LOGGER.info("PropernsityScore advancesearch test case passed...");
			  testutil.captureScreen(driver, "PropernsityScore advancesearch");
		  }
		  else {
			  LOGGER.info("PropernsityScore advancesearch test case failed...");
			  testutil.captureScreen(driver, "PropernsityScore advancesearch failed");
			  Assert.assertTrue(false);
		  }
		  Thread.sleep(5000);
	}
	
	@AfterMethod
	public void tearDown(){;
		driver.quit();
	}

}
