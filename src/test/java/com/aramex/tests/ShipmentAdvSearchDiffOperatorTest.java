package com.aramex.tests;

import java.io.IOException;
import java.util.logging.Logger;

import org.openqa.selenium.Keys;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.aramex.base.TestBase;
import com.aramex.pages.AdvanceSearchOperatorPage;
import com.aramex.pages.CashierMakerConsumerHomePage;
import com.aramex.pages.LoginPage;
import com.aramex.pages.ShipmentsBasicSearchPage;
import com.aramex.util.TestUtil;
import com.aramex.util.WebDriverLib;

public class ShipmentAdvSearchDiffOperatorTest extends TestBase{

	WebDriverLib wbLib=new WebDriverLib();
	Logger LOGGER = Logger.getLogger(ShipmentAdvSearchDiffOperatorTest.class.getName());
	LoginTest logintest = new LoginTest();
	LoginPage devHomePage=PageFactory.initElements(TestBase.driver, LoginPage.class);
	TestUtil testutil =  new TestUtil();

	
	@BeforeMethod
	public void logintoAramexAppliction() throws InterruptedException {
		logintest.loginToAppAsTester(prop.getProperty("lmsusername"), prop.getProperty("lmspassword"));
		
	}
	
	@Test(priority=3) 
	  public void ShipmentnotequalstoOperator() throws InterruptedException, IOException {
		wbLib.waitForPageTobeLoad();
		  Thread.sleep(5000);
		  CashierMakerConsumerHomePage homePage=PageFactory.initElements(TestBase.driver, CashierMakerConsumerHomePage.class);
		  AdvanceSearchOperatorPage operatorpg=PageFactory.initElements(TestBase.driver, AdvanceSearchOperatorPage.class);
		  ShipmentsBasicSearchPage shipmntbasicsrch=PageFactory.initElements(TestBase.driver, ShipmentsBasicSearchPage.class);
		 
		  shipmntbasicsrch.getClickondropdownToNavigateShipment().click();
		  shipmntbasicsrch.getClickonshipmentmodule().click();
		  Thread.sleep(5000);
		  homePage.getAdvanceSearchButton().click();
		  homePage.getVariabledropdown().click();
		  homePage.getSelectMobileNumber().click();
		  homePage.getOperatordropdown().click();
		  operatorpg.getOperatornotEqualsTo().click();
		  Thread.sleep(2000);
		  homePage.getValueinputfield().sendKeys(prop.getProperty("ShipmentMobileNo"));
		  homePage.getAddrow().click();
		  homePage.getApplyfilter().click();
		  Thread.sleep(5000);
		  testutil.captureScreen(driver, "ShipmentNotequalsToOperator outcome");
		  
	}
	
	@Test(priority=4) 
	  public void shipmentINOperator() throws InterruptedException, IOException {
		wbLib.waitForPageTobeLoad();
		  Thread.sleep(5000);
		  CashierMakerConsumerHomePage homePage=PageFactory.initElements(TestBase.driver, CashierMakerConsumerHomePage.class);
		  AdvanceSearchOperatorPage operatorpg=PageFactory.initElements(TestBase.driver, AdvanceSearchOperatorPage.class);
		  ShipmentsBasicSearchPage shipmntbasicsrch=PageFactory.initElements(TestBase.driver, ShipmentsBasicSearchPage.class);
		  shipmntbasicsrch.getClickondropdownToNavigateShipment().click();
		  shipmntbasicsrch.getClickonshipmentmodule().click();
		  Thread.sleep(5000);
		  homePage.getAdvanceSearchButton().click();
		  wbLib.waitForTextPresent("Variable");
		  Thread.sleep(2000);
		  homePage.getVariabledropdown().click();
		  shipmntbasicsrch.getSelectShipmentNumber().click();
		  homePage.getOperatordropdown().click();
		  operatorpg.getOperatorin().click();
		  Thread.sleep(2000);
		  homePage.getValueinputfield().sendKeys(prop.getProperty("ShipmentNo"));
		  operatorpg.getAddsymbol().click();
		  homePage.getValueinputfield().sendKeys(prop.getProperty("ShipmentNo2"));
		  operatorpg.getAddsymbol().click();
		  homePage.getAddrow().click();
		  homePage.getApplyfilter().click();
		  Thread.sleep(5000);
		  testutil.captureScreen(driver, "ShipmentINOperator outcome");

		  
	}
	@Test(priority=5) 
	  public void shipmentgreaterThanEqualsToOperator() throws InterruptedException, IOException {
		wbLib.waitForPageTobeLoad();
		  Thread.sleep(5000);
		  CashierMakerConsumerHomePage homePage=PageFactory.initElements(TestBase.driver, CashierMakerConsumerHomePage.class);
		  AdvanceSearchOperatorPage operatorpg=PageFactory.initElements(TestBase.driver, AdvanceSearchOperatorPage.class);
		  ShipmentsBasicSearchPage shipmntbasicsrch=PageFactory.initElements(TestBase.driver, ShipmentsBasicSearchPage.class);
		  shipmntbasicsrch.getClickondropdownToNavigateShipment().click();
		  shipmntbasicsrch.getClickonshipmentmodule().click();
		  Thread.sleep(5000);
		  homePage.getAdvanceSearchButton().click();
		  wbLib.waitForTextPresent("Variable");
		  Thread.sleep(2000);
		  homePage.getVariabledropdown().click();
		  shipmntbasicsrch.getSelectRemainingAmount().click();
		  homePage.getOperatordropdown().click();
		  operatorpg.getOperatorgreaterthanEqualsTo().click();
		  Thread.sleep(2000);
		  homePage.getValueinputfield().sendKeys(prop.getProperty("remainingAmount1"));
		  homePage.getAddrow().click();
		  homePage.getApplyfilter().click();
		  Thread.sleep(5000);
		  testutil.captureScreen(driver, "ShipmentgreaterThanEqualsToOperator outcome");
		  
	}
	
	@Test(priority=6) 
	  public void shipmentlessThanEqualsToOperator() throws InterruptedException, IOException {
		wbLib.waitForPageTobeLoad();
		  Thread.sleep(2000);
		  CashierMakerConsumerHomePage homePage=PageFactory.initElements(TestBase.driver, CashierMakerConsumerHomePage.class);
		  AdvanceSearchOperatorPage operatorpg=PageFactory.initElements(TestBase.driver, AdvanceSearchOperatorPage.class);
		  ShipmentsBasicSearchPage shipmntbasicsrch=PageFactory.initElements(TestBase.driver, ShipmentsBasicSearchPage.class);
		  shipmntbasicsrch.getClickondropdownToNavigateShipment().click();
		  shipmntbasicsrch.getClickonshipmentmodule().click();
		  Thread.sleep(5000);
		  homePage.getAdvanceSearchButton().click();
		  wbLib.waitForTextPresent("Variable");
		  Thread.sleep(2000);
		  homePage.getVariabledropdown().click();
		  shipmntbasicsrch.getSelectRemainingAmount().click();
		  homePage.getOperatordropdown().click();
		  operatorpg.getOperatornotlessthanEqualsTo().click();
		  Thread.sleep(2000);
		  homePage.getValueinputfield().sendKeys(prop.getProperty("remainingAmount2"));
		  homePage.getAddrow().click();
		  homePage.getApplyfilter().click();
		  Thread.sleep(5000);
		  testutil.captureScreen(driver, "shipmentlessThanEqualsToOperator outcome");
		  
	}
	
	@Test(priority=7) 
	  public void shipmentlessThanOperator() throws InterruptedException, IOException {
		wbLib.waitForPageTobeLoad();
		  Thread.sleep(2000);
		  CashierMakerConsumerHomePage homePage=PageFactory.initElements(TestBase.driver, CashierMakerConsumerHomePage.class);
		  AdvanceSearchOperatorPage operatorpg=PageFactory.initElements(TestBase.driver, AdvanceSearchOperatorPage.class);
		  ShipmentsBasicSearchPage shipmntbasicsrch=PageFactory.initElements(TestBase.driver, ShipmentsBasicSearchPage.class);
		  shipmntbasicsrch.getClickondropdownToNavigateShipment().click();
		  shipmntbasicsrch.getClickonshipmentmodule().click();
		  Thread.sleep(5000);
		  homePage.getAdvanceSearchButton().click();
		  wbLib.waitForTextPresent("Variable");
		  Thread.sleep(2000);
		  homePage.getVariabledropdown().click();
		  shipmntbasicsrch.getSelectRemainingAmount().click();
		  homePage.getOperatordropdown().click();
		  operatorpg.getOperatorlessthan().click();
		  Thread.sleep(2000);
		  homePage.getValueinputfield().sendKeys(prop.getProperty("remainingAmount2"));
		  homePage.getAddrow().click();
		  homePage.getApplyfilter().click();
		  Thread.sleep(5000);
		  testutil.captureScreen(driver, "shipmentlessThanOperator outcome");
		  
	}
	
	@Test(priority=8) 
	  public void shipmentgreaterThanOperator() throws InterruptedException, IOException {
		wbLib.waitForPageTobeLoad();
		  Thread.sleep(2000);
		  CashierMakerConsumerHomePage homePage=PageFactory.initElements(TestBase.driver, CashierMakerConsumerHomePage.class);
		  AdvanceSearchOperatorPage operatorpg=PageFactory.initElements(TestBase.driver, AdvanceSearchOperatorPage.class);
		  ShipmentsBasicSearchPage shipmntbasicsrch=PageFactory.initElements(TestBase.driver, ShipmentsBasicSearchPage.class);
		  shipmntbasicsrch.getClickondropdownToNavigateShipment().click();
		  shipmntbasicsrch.getClickonshipmentmodule().click();
		  Thread.sleep(5000);
		  homePage.getAdvanceSearchButton().click();
		  wbLib.waitForTextPresent("Variable");
		  Thread.sleep(2000);
		  homePage.getVariabledropdown().click();
		  shipmntbasicsrch.getSelectRemainingAmount().click();
		  homePage.getOperatordropdown().click();
		  operatorpg.getOperatorgreaterthan().click();
		  Thread.sleep(2000);
		  homePage.getValueinputfield().sendKeys(prop.getProperty("remainingAmount1"));
		  homePage.getAddrow().click();
		  homePage.getApplyfilter().click();
		  Thread.sleep(5000);
		  testutil.captureScreen(driver, "shipmentgreaterToOperator outcome");
		  
	}
	
	@Test(priority=1) 
	  public void shipmentrangeOperator() throws InterruptedException, IOException {
		wbLib.waitForPageTobeLoad();
		  Thread.sleep(2000);
		  CashierMakerConsumerHomePage homePage=PageFactory.initElements(TestBase.driver, CashierMakerConsumerHomePage.class);
		  AdvanceSearchOperatorPage operatorpg=PageFactory.initElements(TestBase.driver, AdvanceSearchOperatorPage.class);
		  ShipmentsBasicSearchPage shipmntbasicsrch=PageFactory.initElements(TestBase.driver, ShipmentsBasicSearchPage.class);
		  shipmntbasicsrch.getClickondropdownToNavigateShipment().click();
		  shipmntbasicsrch.getClickonshipmentmodule().click();
		  Thread.sleep(5000);
		  homePage.getAdvanceSearchButton().click();
		  wbLib.waitForTextPresent("Variable");
		  Thread.sleep(2000);
		  homePage.getVariabledropdown().click();
		  shipmntbasicsrch.getSelectexptdtofdelvry().click();
		  homePage.getOperatordropdown().click();
		  operatorpg.getOperatorrange().click();
		  Thread.sleep(5000);
		 operatorpg.getDatevaluefrom().click();
		 Thread.sleep(3000);
		  operatorpg.getMonthSelectr().click();
		  Thread.sleep(3000);
		  operatorpg.getFromMonth().click();
		  Thread.sleep(3000);
		  operatorpg.getFromDate().click();
		  Thread.sleep(3000);
		  operatorpg.getDatevalueTo().click();
		  Thread.sleep(3000);
		  operatorpg.getMonthSelectr().click();
		  Thread.sleep(3000);
		  operatorpg.getToMonth().click();
		  Thread.sleep(3000);
		  operatorpg.getToDate().click();
		  Thread.sleep(3000);
		  homePage.getAddrow().click();
		  Thread.sleep(3000);
		  homePage.getApplyfilter().click();
		  wbLib.waitForPageTobeLoad();
		  Thread.sleep(5000);
		  testutil.captureScreen(driver, "ShipmentrangeOperator outcome");
		  
	}
	
	@Test(priority=2) 
	  public void shipmentLikeOperator() throws InterruptedException, IOException {
		wbLib.waitForPageTobeLoad();
		  Thread.sleep(2000);
		  CashierMakerConsumerHomePage homePage=PageFactory.initElements(TestBase.driver, CashierMakerConsumerHomePage.class);
		  AdvanceSearchOperatorPage operatorpg=PageFactory.initElements(TestBase.driver, AdvanceSearchOperatorPage.class);
		  ShipmentsBasicSearchPage shipmntbasicsrch=PageFactory.initElements(TestBase.driver, ShipmentsBasicSearchPage.class);
		  shipmntbasicsrch.getClickondropdownToNavigateShipment().click();
		  shipmntbasicsrch.getClickonshipmentmodule().click();
		  Thread.sleep(5000);
		  homePage.getAdvanceSearchButton().click();
		  wbLib.waitForTextPresent("Variable");
		  Thread.sleep(2000);
		  homePage.getVariabledropdown().click();
		  shipmntbasicsrch.getSelectShipmentNumber().click();
		  homePage.getOperatordropdown().click();
		  operatorpg.getOperatorLike().click();
		  Thread.sleep(2000);
		  homePage.getValueinputfield().sendKeys(prop.getProperty("ShipmentNo"));
		  homePage.getAddrow().click();
		  homePage.getApplyfilter().click();
		  Thread.sleep(5000);
		  testutil.captureScreen(driver, "shipmentLikeOperator outcome");
		  
	}
	
	@AfterMethod
	public void tearDown(){;
		driver.quit();
	}
	
}
