package com.aramex.tests;

import java.io.IOException;
import java.util.logging.Logger;

import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.aramex.base.TestBase;
import com.aramex.pages.CashierMakerConsumerHomePage;
import com.aramex.pages.LoginPage;
import com.aramex.util.TestUtil;
import com.aramex.util.WebDriverLib;

public class ConsumerAdvanceSearchOtherFunTest extends TestBase{

	WebDriverLib wbLib=new WebDriverLib();
	Logger LOGGER = Logger.getLogger(ConsumerAdvanceSearchOtherFunTest.class.getName());
	LoginTest logintest = new LoginTest();
	LoginPage devHomePage=PageFactory.initElements(TestBase.driver, LoginPage.class);
	CashierMakerConsumerHomePage homePage=PageFactory.initElements(TestBase.driver, CashierMakerConsumerHomePage.class);
	TestUtil testutil =  new TestUtil();

	
	@BeforeMethod
	public void logintoAramexAppliction() throws InterruptedException {
		logintest.loginToAppAsTester(prop.getProperty("lmsusername"), prop.getProperty("lmspassword"));
		
	}
	@Test(priority=4) 
	  public void advanceSearchaddMulipleRow() throws InterruptedException, IOException {
		
		wbLib.waitForPageTobeLoad();
		  Thread.sleep(5000);
		  CashierMakerConsumerHomePage homePage=PageFactory.initElements(TestBase.driver, CashierMakerConsumerHomePage.class);
		  homePage.getAdvanceSearchButton().click();
		  homePage.getVariabledropdown().click();
		  homePage.getSelectMobileNumber().click();
		  homePage.getOperatordropdown().click();
		  homePage.getOperatorequalsTo().click();
		  Thread.sleep(2000);
		  homePage.getValueinputfield().sendKeys(prop.getProperty("SearchbyMobilenumber"));
		  homePage.getAddrow().click();
		  testutil.captureScreen(driver, "add 1st row");
		  
		  //add2nd row
		  homePage.getVariabledropdown().click();
		  homePage.getSelectConsumerName().click();
		  homePage.getOperatordropdown().click();
		  homePage.getOperatorequalsTo().click();
		  Thread.sleep(2000);
		  homePage.getValueinputfield().sendKeys(prop.getProperty("consumerName2"));
		  homePage.getAddrow().click();
		  testutil.captureScreen(driver, "add 2nd row");
		  
		//add3rd row
		  homePage.getVariabledropdown().click();
		  homePage.getSelectConsumerName().click();
		  homePage.getOperatordropdown().click();
		  homePage.getOperatorequalsTo().click();
		  Thread.sleep(2000);
		  homePage.getValueinputfield().sendKeys(prop.getProperty("consumerName3"));
		  homePage.getAddrow().click();
		  testutil.captureScreen(driver, "add 3rd row");
		  
		  homePage.getApplyfilter().click();
		  Thread.sleep(7000);
		  testutil.captureScreen(driver, "after addrow finaloutcome screen");
		
	}
	
	@Test(priority=3) 
	  public void deleteRow() throws InterruptedException, IOException {
		
		wbLib.waitForPageTobeLoad();
		  Thread.sleep(5000);
		  CashierMakerConsumerHomePage homePage=PageFactory.initElements(TestBase.driver, CashierMakerConsumerHomePage.class);
		  homePage.getAdvanceSearchButton().click();
		  homePage.getVariabledropdown().click();
		  homePage.getSelectMobileNumber().click();
		  homePage.getOperatordropdown().click();
		  homePage.getOperatorequalsTo().click();
		  Thread.sleep(2000);
		  homePage.getValueinputfield().sendKeys(prop.getProperty("SearchbyMobilenumber"));
		  homePage.getAddrow().click();
		  
		  //add2nd row
		  homePage.getVariabledropdown().click();
		  homePage.getSelectConsumerName().click();
		  homePage.getOperatordropdown().click();
		  homePage.getOperatorequalsTo().click();
		  Thread.sleep(2000);
		  homePage.getValueinputfield().sendKeys(prop.getProperty("consumerName2"));
		  homePage.getAddrow().click();
		  homePage.getDeleteRow().click();
		  Thread.sleep(3000);
		  testutil.captureScreen(driver, "delete Row");
		  homePage.getApplyfilter().click();
	}
	
	@Test(priority=1) 
	  public void withCaseSensitiveNegativetestData() throws InterruptedException, IOException {
		
		wbLib.waitForPageTobeLoad();
		  Thread.sleep(5000);
		  CashierMakerConsumerHomePage homePage=PageFactory.initElements(TestBase.driver, CashierMakerConsumerHomePage.class);
		  homePage.getAdvanceSearchButton().click();
		  Thread.sleep(3000);
		  homePage.getCaseSensitivechkbox().click();
		  homePage.getVariabledropdown().click();
		  homePage.getSelectConsumerName().click();
		  homePage.getOperatordropdown().click();
		  homePage.getOperatorequalsTo().click();
		  Thread.sleep(2000);
		  homePage.getValueinputfield().sendKeys(prop.getProperty("withcasesensitiveconnm"));
		  homePage.getAddrow().click();
		  Thread.sleep(3000);
		  homePage.getApplyfilter().click();
		  LOGGER.info("should not filter");
		  Thread.sleep(3000);
		  testutil.captureScreen(driver, "should not filter");
	}
	
	@Test(priority=2) 
	  public void withCaseSensitivePositivetestData() throws InterruptedException, IOException {
		
		wbLib.waitForPageTobeLoad();
		  Thread.sleep(5000);
		  CashierMakerConsumerHomePage homePage=PageFactory.initElements(TestBase.driver, CashierMakerConsumerHomePage.class);
		  homePage.getAdvanceSearchButton().click();
		  Thread.sleep(3000);
		  homePage.getCaseSensitivechkbox().click();
		  homePage.getVariabledropdown().click();
		  homePage.getSelectConsumerName().click();
		  homePage.getOperatordropdown().click();
		  homePage.getOperatorequalsTo().click();
		  Thread.sleep(2000);
		  homePage.getValueinputfield().sendKeys(prop.getProperty("ConsumerName"));
		  homePage.getAddrow().click();
		  Thread.sleep(3000);
		  homePage.getApplyfilter().click();
		  LOGGER.info("should filter");
		  Thread.sleep(3000);
		  testutil.captureScreen(driver, "should filter");
	}
	  
	@AfterMethod
	public void tearDown(){;
		driver.quit();
	}
	
}
