package com.aramex.tests;

import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.aramex.base.TestBase;
import com.aramex.pages.LoginPage;
import com.aramex.util.WebDriverLib;

public class LoginTest extends TestBase{

	LoginPage devHomePage=PageFactory.initElements(TestBase.driver, LoginPage.class);
	WebDriverLib wbLib=new WebDriverLib();
	
	public void loginToAppAsTester(String lmsun1, String lmspwd1) throws InterruptedException {
	initialization();
	   Thread.sleep(9000);
	  // wbLib.waitforvisibilityOfElementLocated("/img[@id='logo']");
	   LoginPage devHomePage=PageFactory.initElements(TestBase.driver, LoginPage.class);
	   devHomePage.getUsernameby().click();
	   devHomePage.getUsernameby().sendKeys(lmsun1);
	   devHomePage.getPasswordby().sendKeys(lmspwd1);
	   devHomePage.getLogin().click();
	   devHomePage.getNavigateTocashierMakr().click();
	   
	   wbLib.waitforvisibilityOfElementLocated("//button[contains(@class,'ant-btn ant-input-search-button ant-btn-primary')]");
	}
	
	public void loginToAppAsCashierchecker(String lmsun1, String lmspwd1) throws InterruptedException {
		initialization();
		   Thread.sleep(9000);
		  // wbLib.waitforvisibilityOfElementLocated("/img[@id='logo']");
		   LoginPage devHomePage=PageFactory.initElements(TestBase.driver, LoginPage.class);
		   devHomePage.getUsernameby().click();
		   devHomePage.getUsernameby().sendKeys(lmsun1);
		   devHomePage.getPasswordby().sendKeys(lmspwd1);
		   devHomePage.getLogin().click();
		   devHomePage.getNavigateToCahsierChkr().click();
		   
		   wbLib.waitforvisibilityOfElementLocated("//button[contains(@class,'ant-btn ant-input-search-button ant-btn-primary')]");
		}
	public void loginToAppAsFinancechecker(String lmsun1, String lmspwd1) throws InterruptedException {
		initialization();
		   Thread.sleep(9000);
		  // wbLib.waitforvisibilityOfElementLocated("/img[@id='logo']");
		   LoginPage devHomePage=PageFactory.initElements(TestBase.driver, LoginPage.class);
		   devHomePage.getUsernameby().click();
		   devHomePage.getUsernameby().sendKeys(lmsun1);
		   devHomePage.getPasswordby().sendKeys(lmspwd1);
		   devHomePage.getLogin().click();
		   devHomePage.getNavigateToFinanceChkr().click();
		   
		   wbLib.waitforvisibilityOfElementLocated("//button[contains(@class,'ant-btn ant-input-search-button ant-btn-primary')]");
		}
	
}
