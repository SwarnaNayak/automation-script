package com.aramex.tests;

import java.io.IOException;
import java.util.logging.Logger;

import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.aramex.base.TestBase;
import com.aramex.pages.AdvanceSearchOperatorPage;
import com.aramex.pages.CashierMakerConsumerHomePage;
import com.aramex.pages.FinanceCheckerPage;
import com.aramex.pages.LoginPage;
import com.aramex.util.TestUtil;
import com.aramex.util.WebDriverLib;

public class FinanceChkrAdvSrchDiffOperatorTest extends TestBase{

	WebDriverLib wbLib=new WebDriverLib();
	Logger LOGGER = Logger.getLogger(ShipmentAdvSearchDiffOperatorTest.class.getName());
	LoginTest logintest = new LoginTest();
	LoginPage devHomePage=PageFactory.initElements(TestBase.driver, LoginPage.class);
	TestUtil testutil =  new TestUtil();

	
	@BeforeMethod
	public void logintoAramexAppliction() throws InterruptedException {
		logintest.loginToAppAsFinancechecker(prop.getProperty("lmsusername"), prop.getProperty("lmspassword"));
		
	}
	
	/*
	 * @Test(priority=6) public void financechkradvSrchnotequalstoOperator() throws
	 * InterruptedException, IOException {
	 * 
	 * wbLib.waitForPageTobeLoad(); //Thread.sleep(5000); FinanceCheckerPage
	 * financechkrpg=PageFactory.initElements(TestBase.driver,
	 * FinanceCheckerPage.class); CashierMakerConsumerHomePage
	 * homePage=PageFactory.initElements(TestBase.driver,
	 * CashierMakerConsumerHomePage.class); AdvanceSearchOperatorPage
	 * operatorpg=PageFactory.initElements(TestBase.driver,
	 * AdvanceSearchOperatorPage.class);
	 * 
	 * Thread.sleep(3000); homePage.getAdvanceSearchButton().click();
	 * homePage.getVariabledropdown().click();
	 * financechkrpg.getSelectDocumentno().click();
	 * homePage.getOperatordropdown().click();
	 * operatorpg.getOperatornotEqualsTo().click(); Thread.sleep(2000);
	 * homePage.getValueinputfield().sendKeys(prop.getProperty("DocumentNo"));
	 * homePage.getAddrow().click(); homePage.getApplyfilter().click();
	 * Thread.sleep(5000); testutil.captureScreen(driver,
	 * "financechkradvSrchnotequalstoOperator");
	 * 
	 * }
	 */
	@Test(priority=2) 
	  public void financechkradvSrcgreaterThanEqualsToOperator() throws InterruptedException, IOException {
		wbLib.waitForPageTobeLoad();
		FinanceCheckerPage financechkrpg=PageFactory.initElements(TestBase.driver, FinanceCheckerPage.class);
		  CashierMakerConsumerHomePage homePage=PageFactory.initElements(TestBase.driver, CashierMakerConsumerHomePage.class);
		  AdvanceSearchOperatorPage operatorpg=PageFactory.initElements(TestBase.driver, AdvanceSearchOperatorPage.class);
		  
		  Thread.sleep(3000);
		  
		  homePage.getAdvanceSearchButton().click();
		  homePage.getVariabledropdown().click();
		  financechkrpg.getSelectTransAmnt().click();
		  homePage.getOperatordropdown().click();
		  operatorpg.getOperatorgreaterthanEqualsTo().click();
		  Thread.sleep(2000);
		  homePage.getValueinputfield().sendKeys(prop.getProperty("TransAmnt1"));
		  homePage.getAddrow().click();
		  homePage.getApplyfilter().click();
		  Thread.sleep(5000);
		  testutil.captureScreen(driver, "financialckrgreaterThanEqualsToOperator outcome");
		  
	}
	
	@Test(priority=3) 
	  public void financechkrlessThanEqualsToOperator() throws InterruptedException, IOException {
		wbLib.waitForPageTobeLoad();
		FinanceCheckerPage financechkrpg=PageFactory.initElements(TestBase.driver, FinanceCheckerPage.class);
		  CashierMakerConsumerHomePage homePage=PageFactory.initElements(TestBase.driver, CashierMakerConsumerHomePage.class);
		  AdvanceSearchOperatorPage operatorpg=PageFactory.initElements(TestBase.driver, AdvanceSearchOperatorPage.class);
		  
		  Thread.sleep(3000);
		  
		  homePage.getAdvanceSearchButton().click();
		  homePage.getVariabledropdown().click();
		  financechkrpg.getSelectTransAmnt().click();
		  homePage.getOperatordropdown().click();
		  operatorpg.getOperatornotlessthanEqualsTo().click();
		  Thread.sleep(2000);
		  homePage.getValueinputfield().sendKeys(prop.getProperty("TransAmnt1"));
		  homePage.getAddrow().click();
		  homePage.getApplyfilter().click();
		  Thread.sleep(5000);
		  testutil.captureScreen(driver, "financialckrLessThanEqualsToOperator outcome");
		  
	}
	
	@Test(priority=4) 
	  public void financechkrlessThanOperator() throws InterruptedException, IOException {
		wbLib.waitForPageTobeLoad();
		FinanceCheckerPage financechkrpg=PageFactory.initElements(TestBase.driver, FinanceCheckerPage.class);
		  CashierMakerConsumerHomePage homePage=PageFactory.initElements(TestBase.driver, CashierMakerConsumerHomePage.class);
		  AdvanceSearchOperatorPage operatorpg=PageFactory.initElements(TestBase.driver, AdvanceSearchOperatorPage.class);
		  
		  Thread.sleep(3000);
		  
		  homePage.getAdvanceSearchButton().click();
		  homePage.getVariabledropdown().click();
		  financechkrpg.getSelectTransAmnt().click();
		  homePage.getOperatordropdown().click();
		  operatorpg.getOperatorlessthan().click();
		  Thread.sleep(2000);
		  homePage.getValueinputfield().sendKeys(prop.getProperty("TransAmnt1"));
		  homePage.getAddrow().click();
		  homePage.getApplyfilter().click();
		  Thread.sleep(5000);
		  testutil.captureScreen(driver, "financialckrLessThanOperator outcome");
		  
	}
	
	/*
	 * @Test(priority=5) public void financechkrgreaterThanOperator() throws
	 * InterruptedException, IOException { wbLib.waitForPageTobeLoad();
	 * FinanceCheckerPage financechkrpg=PageFactory.initElements(TestBase.driver,
	 * FinanceCheckerPage.class); CashierMakerConsumerHomePage
	 * homePage=PageFactory.initElements(TestBase.driver,
	 * CashierMakerConsumerHomePage.class); AdvanceSearchOperatorPage
	 * operatorpg=PageFactory.initElements(TestBase.driver,
	 * AdvanceSearchOperatorPage.class);
	 * 
	 * Thread.sleep(3000);
	 * 
	 * homePage.getAdvanceSearchButton().click();
	 * homePage.getVariabledropdown().click();
	 * financechkrpg.getSelectTransAmnt().click();
	 * homePage.getOperatordropdown().click();
	 * operatorpg.getOperatorgreaterthan().click(); Thread.sleep(2000);
	 * homePage.getValueinputfield().sendKeys(prop.getProperty("TransAmnt1"));
	 * homePage.getAddrow().click(); homePage.getApplyfilter().click();
	 * Thread.sleep(5000); testutil.captureScreen(driver,
	 * "financialckrgreaterThanOperator outcome");
	 * 
	 * }
	 */
	
	@Test(priority=1) 
	  public void financechkrrangeOperator() throws InterruptedException, IOException {
		wbLib.waitForPageTobeLoad();
		  Thread.sleep(5000);
		  FinanceCheckerPage financechkrpg=PageFactory.initElements(TestBase.driver, FinanceCheckerPage.class);
		  CashierMakerConsumerHomePage homePage=PageFactory.initElements(TestBase.driver, CashierMakerConsumerHomePage.class);
		  AdvanceSearchOperatorPage operatorpg=PageFactory.initElements(TestBase.driver, AdvanceSearchOperatorPage.class);
		  
		  homePage.getAdvanceSearchButton().click();
		  wbLib.waitForTextPresent("Variable");
		  Thread.sleep(2000);
		  homePage.getVariabledropdown().click();
		  financechkrpg.getSelectTransDt().click();
		  homePage.getOperatordropdown().click();
		  operatorpg.getOperatorrange().click();
		  Thread.sleep(5000);
		 operatorpg.getDatevaluefrom().click();
		 Thread.sleep(3000);
			/*
			 * operatorpg.getMonthSelectr().click(); Thread.sleep(3000);
			 * financechkrpg.getTransDtfromMonth().click(); Thread.sleep(3000);
			 */
		  financechkrpg.getTransDtfromDate().click();
		  Thread.sleep(3000);
		  operatorpg.getDatevalueTo().click();
		  Thread.sleep(3000);
			/*
			 * operatorpg.getMonthSelectr().click(); Thread.sleep(3000);
			 * financechkrpg.getTransDttoMonth().click(); Thread.sleep(3000);
			 */
		  financechkrpg.getTransDttoDate().click();
		  Thread.sleep(3000);
		  homePage.getAddrow().click();
		  Thread.sleep(3000);
		  homePage.getApplyfilter().click();
		  wbLib.waitForPageTobeLoad();
		  Thread.sleep(5000);
		  testutil.captureScreen(driver, "FinanceChkrrangeOperator outcome");
		  
	}
	
	/*
	 * @Test(priority=7) public void financechkrlikeOperator() throws
	 * InterruptedException, IOException { wbLib.waitForPageTobeLoad();
	 * FinanceCheckerPage financechkrpg=PageFactory.initElements(TestBase.driver,
	 * FinanceCheckerPage.class); CashierMakerConsumerHomePage
	 * homePage=PageFactory.initElements(TestBase.driver,
	 * CashierMakerConsumerHomePage.class); AdvanceSearchOperatorPage
	 * operatorpg=PageFactory.initElements(TestBase.driver,
	 * AdvanceSearchOperatorPage.class);
	 * 
	 * Thread.sleep(3000);
	 * 
	 * homePage.getAdvanceSearchButton().click();
	 * homePage.getVariabledropdown().click();
	 * financechkrpg.getSelectEtailerID().click();
	 * homePage.getOperatordropdown().click(); operatorpg.getOperatorLike().click();
	 * Thread.sleep(2000);
	 * homePage.getValueinputfield().sendKeys(prop.getProperty("EtailerID"));
	 * homePage.getAddrow().click(); homePage.getApplyfilter().click();
	 * Thread.sleep(5000); testutil.captureScreen(driver,
	 * "financialckrLikeOperator outcome");
	 * 
	 * }
	 */
	@AfterMethod
	public void tearDown(){;
		driver.quit();
	}
	
}
