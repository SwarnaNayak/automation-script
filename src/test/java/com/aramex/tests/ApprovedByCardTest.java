package com.aramex.tests;

import java.io.IOException;
import java.util.logging.Logger;

import org.openqa.selenium.By;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.aramex.base.TestBase;
import com.aramex.pages.ServiceRequestPage;
import com.aramex.pages.ShipmentDetailViewPage;
import com.aramex.pages.ShipmentsBasicSearchPage;
import com.aramex.util.TestUtil;
import com.aramex.util.WebDriverLib;

public class ApprovedByCardTest extends TestBase{

	ShipmentDetailViewPage shipmndetailvwpg = new ShipmentDetailViewPage();
	WebDriverLib wbLib=new WebDriverLib();
	Logger LOGGER = Logger.getLogger(ShipmentBasicSearchTest.class.getName());
	LoginTest logintest = new LoginTest();
	TestUtil testutil =  new TestUtil();
	
	@BeforeMethod
	public void logintoAramexAppliction() throws InterruptedException {
		logintest.loginToAppAsTester(prop.getProperty("lmsusername"), prop.getProperty("lmspassword"));
		
	}
	
	@Test
	public void approveByCard() throws InterruptedException, IOException {
		shipmndetailvwpg.clickon_search_Shipment(prop.getProperty("shipmentNoToApproveByCard"));
		ShipmentDetailViewPage shipdtlvwpg=PageFactory.initElements(TestBase.driver, ShipmentDetailViewPage.class);
		ServiceRequestPage serviceRQpg=PageFactory.initElements(TestBase.driver, ServiceRequestPage.class);
		ShipmentsBasicSearchPage shipmntbasicsrch=PageFactory.initElements(TestBase.driver, ShipmentsBasicSearchPage.class);
		Actions act =  new Actions(driver);

		//act.moveToElement(shipdtlvwpg.getShipmntselctarrow()).click().perform();
		shipdtlvwpg.getShipmntselctarrow().click();
		shipdtlvwpg.getSelectPayByCash().click();
		Thread.sleep(3000);
		//Actions act =  new Actions(driver);
		act.moveToElement(driver.findElement(By.xpath("//span[text()='Raise']"))).click().perform();
		Thread.sleep(5000);
		//shipdtlvwpg.getClkRaise().click();
		shipdtlvwpg.getTransactioninput().sendKeys("1");
		shipdtlvwpg.getCardLast4Digit().sendKeys("2345");
		shipdtlvwpg.getAuthCode().sendKeys("2");
		shipdtlvwpg.getComments().sendKeys("Test");
		Thread.sleep(3000);
		act.moveToElement(driver.findElement(By.xpath("//span[text()='Pay']"))).click().perform();

		Thread.sleep(4000);
		wbLib.waitForTextPresent("OK");
		
		String expectedMessage ="Successfully Saved";
		String actualmessage =driver.findElement(By.xpath("//span[text()='OK']/ancestor::div[@class='ant-modal-footer']/preceding-sibling::div[@class='ant-modal-body']")).getText();
		System.out.println(actualmessage);
		testutil.captureScreen(driver, "payByCardverifymessageIs "+actualmessage );
		Assert.assertEquals(actualmessage, expectedMessage);


		Thread.sleep(3000);
		act.moveToElement(shipdtlvwpg.getClkonOK()).click().perform();
		Thread.sleep(8000);
	
		
		//navigate to service Request bucket to verify pay by card case
		serviceRQpg.getClickondropdownToNavigateServiceRq().click();
		serviceRQpg.getclickonServiceRQ().click();
		serviceRQpg.getClickApprovedCardbuck().click();
		Thread.sleep(3000);
		
		if(driver.getPageSource().contains(prop.getProperty("shipmentNoToApproveByCard")))
		{
			System.out.println("shipmentNoToApproveByCard is present");
			driver.findElement(By.xpath("//td[text()='"+prop.getProperty("shipmentNoToApproveByCard")+"']")).click();
			testutil.captureScreen(driver, " ' "+prop.getProperty("shipmentNoToApproveByCard")+" ' PayByCarddetailview");
			Thread.sleep(3000);
			
			}else
			{
			System.out.println("shipmentNoToApproveByCard is absent");
			}
	
		serviceRQpg.getClickAllTransactions().click();
		Thread.sleep(3000);
		testutil.captureScreen(driver, "ApproveByCardtransactionTab");
		Thread.sleep(2000);
		/*driver.findElement(By.xpath("//td[text()='"+prop.getProperty("shipmentNoToApproveByCard")+"']")).click();
		Thread.sleep(9000);*/
		
	}
	@AfterMethod
	public void tearDown(){;
		driver.quit();
	}

	
}
